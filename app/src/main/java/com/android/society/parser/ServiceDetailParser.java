package com.android.society.parser;

import com.android.society.Constants.Constants;
import com.android.society.Model.ServiceDetail;
import com.android.society.Model.StaffDetails;
import com.android.society.utils.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by 201101101 on 11/8/2017.
 */

public class ServiceDetailParser {

    public static  ArrayList<StaffDetails> getAllServiceDetail(String response){
        ArrayList<StaffDetails> staffDetailsArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null) {
                JSONArray dataJsonArray = jsonObject.optJSONArray(Keys.DATA);
                if(dataJsonArray != null && dataJsonArray.length()>0){
                    for (int i = 0; i < dataJsonArray.length(); i++) {
                        JSONObject serviceObject = dataJsonArray.optJSONObject(i);
                        if(serviceObject != null){
                            StaffDetails staffDetails = new StaffDetails();
                            staffDetails.setStaffName(serviceObject.optString(Keys.NAME));
                            staffDetails.setStaffId(serviceObject.optString(Keys.ID));
                            staffDetails.setStaffNumber(serviceObject.optString(Keys.PHONE));
                            staffDetails.setSelected(serviceObject.optBoolean(Keys.IS_SELECTED));
                            staffDetails.setStaffType(String.valueOf(serviceObject.optString(Keys.TYPE)));
                            staffDetails.setImage(serviceObject.optString(Keys.IMAGE));
                            staffDetails.setIdProof(serviceObject.optString(Keys.PROOF));

                            staffDetailsArrayList.add(staffDetails);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return staffDetailsArrayList;
    }
}
