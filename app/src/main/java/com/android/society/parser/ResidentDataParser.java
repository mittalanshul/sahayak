package com.android.society.parser;

import android.content.Context;
import android.text.TextUtils;

import com.android.society.Model.Resident;
import com.android.society.Model.Token;
import com.android.society.utils.Dao;
import com.android.society.utils.Keys;
import com.android.society.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 201101101 on 10/31/2017.
 */

public class ResidentDataParser {

    public static Resident residentParser(String response, Context mContext){
        Resident resident = new Resident();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject dataObject = jsonObject.optJSONObject(Keys.DATA);
            if(dataObject != null){
                String token = dataObject.optString(Keys.TOKEN);
                if(!TextUtils.isEmpty(token)){
                    Dao.getInstance().setToken(new Token(token));
                    PreferenceManager.getInstance(mContext).
                            writeToPrefs(PreferenceManager.TOKEN, token);
                }

                JSONObject residentObject = dataObject.optJSONObject(Keys.RESIDENT);
                if(residentObject != null){
                    resident = AllSocietyDataParser.parseResidentData(residentObject);
                    resident.setLogo(dataObject.optString(Keys.LOGO));

                    JSONObject authorityObject = residentObject.optJSONObject(Keys.AUTHORITY);
                    if(authorityObject != null){
                        resident.setAuthAddress1(authorityObject.optString(Keys.ADDRESS1));
                        resident.setAuthAddress2(authorityObject.optString(Keys.ADDRESS2));
                        resident.setLattitude(authorityObject.optString(Keys.LATITUDE));
                        resident.setLongitude(authorityObject.optString(Keys.LONGITUDE));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resident;
    }
}
