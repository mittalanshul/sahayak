package com.android.society.parser;

import com.android.society.Model.MaintainenceDues;
import com.android.society.utils.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anshul on 08/03/18.
 */

public class AllResidentMaintainenceParserByID {

    public static MaintainenceDues getMaintainenceDuesById(String response){

        MaintainenceDues maintainenceDues = new MaintainenceDues();
        try{
            JSONObject jsonObject = new JSONObject(response);
            jsonObject = jsonObject.optJSONObject(Keys.DATA);
            if(jsonObject != null){

                maintainenceDues.setTotal(jsonObject.optString(Keys.AMOUNT));
                maintainenceDues.setPenalty(jsonObject.optString(Keys.PENALTYAMOUNT));
                maintainenceDues.setPenaltyInterval(jsonObject.optString(Keys.PENALTYINTERVAL));
                maintainenceDues.setPenaltyType(jsonObject.optString(Keys.PENALTYTYPE));
                maintainenceDues.setStatus(jsonObject.optString(Keys.STATUS));
                maintainenceDues.setLastDate(jsonObject.optString(Keys.LASTDATE));
                maintainenceDues.setId(jsonObject.optString(Keys.ID));
            }
        }catch (JSONException e){

        }
        return maintainenceDues;
    }
}
