package com.android.society.parser;

import com.android.society.Model.Token;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anshul on 20/01/18.
 */

public class ResidentExpectedSuccessParser {

    public static String parseExpectedVisitorId(String response){
        String expectedVisitorId = "";
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                JSONObject dataObject = jsonObject.optJSONObject(Keys.DATA);
                if(dataObject != null){
                    expectedVisitorId = dataObject.optString(Keys.ID);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return expectedVisitorId;
    }

}
