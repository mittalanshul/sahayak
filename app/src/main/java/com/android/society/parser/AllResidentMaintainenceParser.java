package com.android.society.parser;

import com.android.society.Model.MaintainenceDues;
import com.android.society.utils.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anshul on 08/03/18.
 */

public class AllResidentMaintainenceParser  {

    public static ArrayList<MaintainenceDues> getMaintainenceDues(String response){

        ArrayList<MaintainenceDues> maintainenceDuesArrayList = new ArrayList<>();
        try{
            JSONObject jsonObject = new JSONObject(response);
            jsonObject = jsonObject.optJSONObject(Keys.DATA);
            if(jsonObject != null){
                JSONArray jsonArray = jsonObject.optJSONArray(Keys.HISTORY);
                if(jsonArray != null && jsonArray.length() >0){
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject mainObject = jsonArray.optJSONObject(i);
                        if(mainObject != null){
                            MaintainenceDues maintainenceDues = new MaintainenceDues();
                            maintainenceDues.setTotal(mainObject.optString(Keys.AMOUNT));
                            maintainenceDues.setPenalty(mainObject.optString(Keys.PENALTYAMOUNT));
                            maintainenceDues.setPenaltyInterval(mainObject.optString(Keys.PENALTYINTERVAL));
                            maintainenceDues.setPenaltyType(mainObject.optString(Keys.PENALTYTYPE));
                            maintainenceDues.setStatus(mainObject.optString(Keys.STATUS));
                            maintainenceDues.setLastDate(mainObject.optString(Keys.LASTDATE));
                            maintainenceDues.setId(mainObject.optString(Keys.ID));
                            maintainenceDuesArrayList.add(maintainenceDues);
                        }
                    }
                }
            }
        }catch (JSONException e){

        }
        return maintainenceDuesArrayList;

    }
}
