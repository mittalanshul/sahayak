package com.android.society.parser;

import com.android.society.Model.Alert;
import com.android.society.Model.ChildExitAction;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anshul on 04/01/18.
 */

public class ChildExitStatusParser {

    public static ChildExitAction parseExitActionData(String response){
        ChildExitAction childExitAction = new ChildExitAction();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                if(jsonObject != null){
                    childExitAction.setAddress1(jsonObject.optString(Keys.ADDRESS1) + " - " + jsonObject.optString(Keys.ADDRESS2));
                    childExitAction.setAnswer(jsonObject.optString(Keys.ANSWER));
                    childExitAction.setName(jsonObject.optString(Keys.NAME));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return childExitAction;
    }
}
