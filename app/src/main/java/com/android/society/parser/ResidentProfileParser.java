package com.android.society.parser;

import android.content.Context;
import android.text.TextUtils;

import com.android.society.Model.Resident;
import com.android.society.utils.Keys;
import com.android.society.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class ResidentProfileParser {

    public static Resident residentParser(String response, Context mContext){
        Resident resident = new Resident();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject dataObject = jsonObject.optJSONObject(Keys.DATA);
            if(dataObject != null){
                if(dataObject != null){
                    resident = AllSocietyDataParser.parseResidentData(dataObject);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resident;
    }
}
