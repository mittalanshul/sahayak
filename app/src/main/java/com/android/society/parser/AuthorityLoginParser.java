package com.android.society.parser;

import com.android.society.Model.Token;
import com.android.society.Model.User;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 201101101 on 10/25/2017.
 */

public class AuthorityLoginParser {

    public static User parserAuthorityLogin(String response){
          User user = new User();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                JSONObject dataObject = jsonObject.optJSONObject(Keys.DATA);
                if(dataObject != null){
                    JSONObject userObject = dataObject.optJSONObject(Keys.USER);
                    if(userObject != null){
                        user.setName(userObject.optString(Keys.NAME));
                        user.setId(userObject.optString(Keys.ID));
                        user.setIdentification(userObject.optString(Keys.IDENTIFICATION));
                        user.setAddress1(userObject.optString(Keys.ADDRESS1));
                        user.setAddress2(userObject.optString(Keys.ADDRESS2));
                        user.setCreatedAt(userObject.optString(Keys.CREATEDAT));
                        user.setUpdatedAt(userObject.optString(Keys.UPDATEDAT));
                        user.setStatus(userObject.optString(Keys.STATUS));
                        user.setEmail(userObject.optString(Keys.EMAIL));
                    }
                    Token token = new Token();
                    String tokenValue = dataObject.optString(Keys.TOKEN);
                    token.setToken(tokenValue);
                    user.setToken(token);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }
}
