package com.android.society.parser;

import com.android.society.Model.Visitor;
import com.android.society.Model.VisitorDetails;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anshul on 07/01/18.
 */

public class VisitorDetailsParser {

    public static VisitorDetails getVisitorDetails(String response){
        VisitorDetails visitorDetails = new VisitorDetails();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                visitorDetails.setVisitorName(jsonObject.optString(Keys.NAME));
                visitorDetails.setVisitorNumber(jsonObject.optString(Keys.PHONE));
                visitorDetails.setPurpose(jsonObject.optString(Keys.REASON));
                visitorDetails.setVehicleName(jsonObject.optString(Keys.VEHICLE));
                visitorDetails.setFlatVisited(jsonObject.optString(Keys.FLAT));
                visitorDetails.setImageURL(jsonObject.optString(Keys.IMAGE));
                visitorDetails.setExpectedVisitorId(jsonObject.optString(Keys.ID));
                visitorDetails.setToken(jsonObject.optString(Keys.TOKEN));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return visitorDetails;
    }

    public static VisitorDetails getVisitorDetails(Visitor visitor){
        VisitorDetails visitorDetails = new VisitorDetails();
        try {
            if(visitor != null){
                visitorDetails.setVisitorName(visitor.getVisitorName());
                visitorDetails.setVisitorNumber(visitor.getVisitorNumber());
                visitorDetails.setPurpose(visitor.getPurpose());
                visitorDetails.setVehicleName(visitor.getVehicleName());
                visitorDetails.setFlatVisited(visitor.getFlatVisited());
                visitorDetails.setExpectedVisitorId(visitor.getId());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return visitorDetails;
    }
}
