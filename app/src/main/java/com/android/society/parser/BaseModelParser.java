package com.android.society.parser;

import com.android.society.Model.BaseModel;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 201101101 on 11/24/2017.
 */

public class BaseModelParser {

    public static BaseModel parseBaseModel(String response){
        BaseModel baseModel = new BaseModel();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                baseModel.setStatus(jsonObject.optBoolean(Keys.STATUS));
                baseModel.setMessage(jsonObject.optString(Keys.MESSAGE));
                baseModel.setData(jsonObject.optString(Keys.DATA));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return baseModel;
    }
}
