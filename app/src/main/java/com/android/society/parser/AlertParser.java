package com.android.society.parser;

import com.android.society.Model.Alert;
import com.android.society.Model.BaseModel;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anshul on 27/12/17.
 */

public class AlertParser {

    public static Alert parseAlertData(String response){
        Alert alert = new Alert();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                JSONObject residentJsonObject = jsonObject.optJSONObject(Keys.RESIDENT);
                if(residentJsonObject != null){
                    alert.setFlatName(residentJsonObject.optString(Keys.ADDRESS1) + " - " + residentJsonObject.optString(Keys.ADDRESS2));
                    alert.setNumber(residentJsonObject.optString(Keys.PHONE1));
                    alert.setIntercomNumber(residentJsonObject.optString(Keys.PHONE2));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return alert;
    }
}
