package com.android.society.parser;

import com.android.society.Model.Token;
import com.android.society.Model.User;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 201101101 on 10/30/2017.
 */

public class OtpSendParser {

    public static Token parserOTPData(String response){
        Token token = new Token();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                JSONObject dataObject = jsonObject.optJSONObject(Keys.DATA);
                if(dataObject != null){
                    String tokenValue = dataObject.optString(Keys.TOKEN);
                    token.setToken(tokenValue);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return token;
    }
}
