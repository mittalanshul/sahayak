package com.android.society.parser;

import com.android.society.Model.Notification;
import com.android.society.Model.Token;
import com.android.society.utils.Keys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 201101101 on 11/30/2017.
 */

public class NotificationParser {

    public static Notification parserNotification(String response){
        Notification notification = new Notification();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                notification.setType(jsonObject.optString(Keys.TYPE));
                notification.setMessage(jsonObject.optString(Keys.MESSAGE));
                JSONObject dataObject = jsonObject.optJSONObject(Keys.DATA);
                if(dataObject != null){
                 notification.setData(dataObject.toString());
                }else{
                    notification.setData(jsonObject.optString(Keys.DATA));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notification;
    }
}
