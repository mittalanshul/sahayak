package com.android.society.parser;

import com.android.society.Model.Event;
import com.android.society.utils.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by 201101101 on 11/9/2017.
 */

public class EventListParser {

    public static ArrayList<Event> getAllEvents(String response){
        ArrayList<Event> eventArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray dataJSONArray = jsonObject.optJSONArray(Keys.DATA);
            if(dataJSONArray != null && dataJSONArray.length() >0){
                for (int i = 0; i < dataJSONArray.length() ; i++) {
                    JSONObject eventObject = dataJSONArray.optJSONObject(i);
                    if(eventObject != null){
                       Event event = new Event();
                        event.setTitle(eventObject.optString(Keys.TITLE));
                        event.setDescription(eventObject.optString(Keys.DESC));
                        event.setType(eventObject.optString(Keys.TYPE));
                        event.setDate(eventObject.optString(Keys.DATE));
                        event.setTime(eventObject.optString(Keys.TIME));
                        event.setId(eventObject.optString(Keys.ID));
                        eventArrayList.add(event);
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventArrayList;
    }
}
