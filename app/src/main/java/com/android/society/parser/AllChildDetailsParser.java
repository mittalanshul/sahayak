package com.android.society.parser;

import com.android.society.Model.BaseModel;
import com.android.society.Model.Child;
import com.android.society.utils.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anshul on 29/12/17.
 */

public class AllChildDetailsParser {

    public static ArrayList<Child> getAllChildDetails(String response){
        ArrayList<Child> childArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject jsonObjectData = jsonObject.optJSONObject(Keys.DATA);
            if(jsonObjectData != null){
                JSONArray jsonArray = jsonObjectData.optJSONArray(Keys.CHILD);
                if(jsonArray != null && jsonArray.length()>0){
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject childObject = jsonArray.optJSONObject(i);
                        if(childObject != null){
                            Child child = new Child();
                            child.setChildName(childObject.optString(Keys.NAME));
                            child.setChildId(childObject.optString(Keys.ID));
                            childArrayList.add(child);
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return childArrayList;
    }
}
