package com.android.society.parser;

import com.android.society.Model.Complaint;
import com.android.society.utils.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by 201101101 on 11/22/2017.
 */

public class AllComplaintsParser {

    public static ArrayList<Complaint> getAllComplaints(String response) {
        ArrayList<Complaint> complaintArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject != null) {
                JSONArray dataJsonArray = jsonObject.optJSONArray(Keys.DATA);
                if (dataJsonArray != null && dataJsonArray.length() > 0) {
                    for (int i = 0; i < dataJsonArray.length(); i++) {
                        JSONObject jsonObject1 = dataJsonArray.optJSONObject(i);
                        if (jsonObject1 != null) {
                            Complaint complaint = new Complaint();
                            complaint.setResident(jsonObject1.optString(Keys.RESIDENT));
                            complaint.setSubject(jsonObject1.optString(Keys.SUBJECT));
                            complaint.setStatus(jsonObject1.optString(Keys.STATUS));
                            complaint.setType(jsonObject1.optString(Keys.TYPE));
                            complaint.setCreatedDate(jsonObject1.optString(Keys.CREATEDAT));
                            complaint.setDesc(jsonObject1.optString(Keys.DESC));
                            complaint.setId(jsonObject1.optString(Keys.ID));
                            complaint.setComments(jsonObject1.optString(Keys.COMMENT));
                            complaintArrayList.add(complaint);
                        }
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return complaintArrayList;
    }

    public static Complaint getComplaint(String response) {
        Complaint complaint = new Complaint();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject != null) {
                JSONObject jsonObject1 = jsonObject.optJSONObject(Keys.DATA);
                if (jsonObject1 != null) {
                    complaint.setResident(jsonObject1.optString(Keys.RESIDENT));
                    complaint.setSubject(jsonObject1.optString(Keys.SUBJECT));
                    complaint.setStatus(jsonObject1.optString(Keys.STATUS));
                    complaint.setType(jsonObject1.optString(Keys.TYPE));
                    complaint.setCreatedDate(jsonObject1.optString(Keys.CREATEDAT));
                    complaint.setDesc(jsonObject1.optString(Keys.DESC));
                    complaint.setId(jsonObject1.optString(Keys.ID));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return complaint;
    }
}
