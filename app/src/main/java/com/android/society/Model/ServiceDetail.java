package com.android.society.Model;

import java.util.ArrayList;

/**
 * Created by 201101101 on 11/8/2017.
 */

public class ServiceDetail {

    private ArrayList<StaffDetails> maidArrayList;
    private ArrayList<StaffDetails> carCleanerArrayList;
    private StaffDetails maidDetails;
    private StaffDetails cleanerDetails;

    public StaffDetails getMaidDetails() {
        return maidDetails;
    }

    public void setMaidDetails(StaffDetails maidDetails) {
        this.maidDetails = maidDetails;
    }

    public StaffDetails getCleanerDetails() {
        return cleanerDetails;
    }

    public void setCleanerDetails(StaffDetails cleanerDetails) {
        this.cleanerDetails = cleanerDetails;
    }



    public ArrayList<StaffDetails> getMaidArrayList() {
        return maidArrayList;
    }

    public void setMaidArrayList(ArrayList<StaffDetails> maidArrayList) {
        this.maidArrayList = maidArrayList;
    }

    public ArrayList<StaffDetails> getCarCleanerArrayList() {
        return carCleanerArrayList;
    }

    public void setCarCleanerArrayList(ArrayList<StaffDetails> carCleanerArrayList) {
        this.carCleanerArrayList = carCleanerArrayList;
    }


}
