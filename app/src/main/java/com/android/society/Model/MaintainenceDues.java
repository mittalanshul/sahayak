package com.android.society.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anshul on 17/02/18.
 */

public class MaintainenceDues implements Parcelable{

    private String currentMaintainence;
    private String penalty;
    private String previousDues;
    private String total;
    private String lastDate;
    private String status;
    private String id;
    private String penaltyInterval;
    private String penaltyType;

    public MaintainenceDues(){

    }

    protected MaintainenceDues(Parcel in) {
        currentMaintainence = in.readString();
        penalty = in.readString();
        previousDues = in.readString();
        total = in.readString();
        lastDate = in.readString();
        status = in.readString();
        id = in.readString();
        penaltyInterval = in.readString();
        penaltyType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currentMaintainence);
        dest.writeString(penalty);
        dest.writeString(previousDues);
        dest.writeString(total);
        dest.writeString(lastDate);
        dest.writeString(status);
        dest.writeString(id);
        dest.writeString(penaltyInterval);
        dest.writeString(penaltyType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaintainenceDues> CREATOR = new Creator<MaintainenceDues>() {
        @Override
        public MaintainenceDues createFromParcel(Parcel in) {
            return new MaintainenceDues(in);
        }

        @Override
        public MaintainenceDues[] newArray(int size) {
            return new MaintainenceDues[size];
        }
    };

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPenaltyInterval() {
        return penaltyInterval;
    }

    public void setPenaltyInterval(String penaltyInterval) {
        this.penaltyInterval = penaltyInterval;
    }

    public String getPenaltyType() {
        return penaltyType;
    }

    public void setPenaltyType(String penaltyType) {
        this.penaltyType = penaltyType;
    }




    public String getCurrentMaintainence() {
        return currentMaintainence;
    }

    public void setCurrentMaintainence(String currentMaintainence) {
        this.currentMaintainence = currentMaintainence;
    }

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getPreviousDues() {
        return previousDues;
    }

    public void setPreviousDues(String previousDues) {
        this.previousDues = previousDues;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }



}
