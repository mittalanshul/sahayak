package com.android.society.Model;

/**
 * Created by anshul on 27/12/17.
 */

public class Alert {

    private String flatName="";
    private String number="";

    public String getFlatName() {
        return flatName;
    }

    public void setFlatName(String flatName) {
        this.flatName = flatName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIntercomNumber() {
        return intercomNumber;
    }

    public void setIntercomNumber(String intercomNumber) {
        this.intercomNumber = intercomNumber;
    }

    private String intercomNumber="";
}
