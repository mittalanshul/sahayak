package com.android.society.Model;

import android.os.Parcelable;

import io.realm.RealmObject;

/**
 * Created by 201101101 on 10/30/2017.
 */

public class Visitor extends RealmObject {

    private String visitorName;
    private String flatVisited;
    private String purpose;
    private String visitorNumber;
    private String vehicleName;
    private String date;
    private String time;
    private boolean isExited;
    private String dateExited;
    private String visitorPicUrl;
    private String visitorLocalImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getDateExited() {
        return dateExited;
    }

    public void setDateExited(String dateExited) {
        this.dateExited = dateExited;
    }

    public String getTimeExited() {
        return timeExited;
    }

    public void setTimeExited(String timeExited) {
        this.timeExited = timeExited;
    }

    private String timeExited;

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getFlatVisited() {
        return flatVisited;
    }

    public void setFlatVisited(String flatVisited) {
        this.flatVisited = flatVisited;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getVisitorNumber() {
        return visitorNumber;
    }

    public void setVisitorNumber(String visitorNumber) {
        this.visitorNumber = visitorNumber;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isExited() {
        return isExited;
    }

    public void setExited(boolean exited) {
        isExited = exited;
    }


    public String getVisitorPicUrl() {
        return visitorPicUrl;
    }

    public void setVisitorPicUrl(String visitorPicUrl) {
        this.visitorPicUrl = visitorPicUrl;
    }

    public String getVisitorLocalImage() {
        return visitorLocalImage;
    }

    public void setVisitorLocalImage(String visitorLocalImage) {
        this.visitorLocalImage = visitorLocalImage;
    }
}
