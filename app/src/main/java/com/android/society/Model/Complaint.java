package com.android.society.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 201101101 on 10/18/2017.
 */

public class Complaint implements Parcelable{

    private String resident;
    private String subject;
    private String desc;
    private String type;
    private String status;
    private String createdDate;
    private String comments ;

    protected Complaint(Parcel in) {
        resident = in.readString();
        subject = in.readString();
        desc = in.readString();
        type = in.readString();
        status = in.readString();
        createdDate = in.readString();
        comments = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(resident);
        dest.writeString(subject);
        dest.writeString(desc);
        dest.writeString(type);
        dest.writeString(status);
        dest.writeString(createdDate);
        dest.writeString(comments);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Complaint> CREATOR = new Creator<Complaint>() {
        @Override
        public Complaint createFromParcel(Parcel in) {
            return new Complaint(in);
        }

        @Override
        public Complaint[] newArray(int size) {
            return new Complaint[size];
        }
    };

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public Complaint(){

    }

    public String getResident() {
        return resident;
    }

    public void setResident(String resident) {
        this.resident = resident;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


}
