package com.android.society.Model;

import io.realm.RealmObject;

/**
 * Created by 201101101 on 11/6/2017.
 */

public class ResidentIn extends RealmObject {

    private String residentName;
    private String flatVisited;
    private String residentNumber;
    private String vehicleName;
    private String date;
    private String time;
    private String slotName;

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }



    public String getResidentName() {
        return residentName;
    }

    public void setResidentName(String residentName) {
        this.residentName = residentName;
    }

    public String getFlatVisited() {
        return flatVisited;
    }

    public void setFlatVisited(String flatVisited) {
        this.flatVisited = flatVisited;
    }

    public String getResidentNumber() {
        return residentNumber;
    }

    public void setResidentNumber(String residentNumber) {
        this.residentNumber = residentNumber;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}