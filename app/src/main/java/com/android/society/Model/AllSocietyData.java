package com.android.society.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by 201101101 on 10/28/2017.
 */

public class AllSocietyData implements Parcelable{
    private HashMap<String,Resident> residentHashMap;
    private TreeSet<String> towerSet;
    private TreeSet<String> towerFlatSet;
    private ArrayList<StaffDetails> carCleanerList;
    private ArrayList<StaffDetails> maidArrayList;
    private ArrayList<StaffDetails> guardArrayList;
    private ArrayList<StaffDetails> officeArrayList;

    public ArrayList<StaffDetails> getGuardArrayList() {
        return guardArrayList;
    }

    public void setGuardArrayList(ArrayList<StaffDetails> guardArrayList) {
        this.guardArrayList = guardArrayList;
    }

    public ArrayList<StaffDetails> getOfficeArrayList() {
        return officeArrayList;
    }

    public void setOfficeArrayList(ArrayList<StaffDetails> officeArrayList) {
        this.officeArrayList = officeArrayList;
    }

    public ArrayList<StaffDetails> getOthersArrayList() {
        return othersArrayList;
    }

    public void setOthersArrayList(ArrayList<StaffDetails> othersArrayList) {
        this.othersArrayList = othersArrayList;
    }

    private ArrayList<StaffDetails> othersArrayList;

    public ArrayList<StaffDetails> getCarCleanerList() {
        return carCleanerList;
    }

    public void setCarCleanerList(ArrayList<StaffDetails> carCleanerList) {
        this.carCleanerList = carCleanerList;
    }



    public ArrayList<StaffDetails> getMaidArrayList() {
        return maidArrayList;
    }

    public void setMaidArrayList(ArrayList<StaffDetails> maidArrayList) {
        this.maidArrayList = maidArrayList;
    }

    public HashMap<String, Resident> getResidentHashMap() {
        return residentHashMap;
    }

    public void setResidentHashMap(HashMap<String, Resident> residentHashMap) {
        this.residentHashMap = residentHashMap;
    }

    public TreeSet<String> getTowerSet() {
        return towerSet;
    }

    public void setTowerSet(TreeSet<String> towerSet) {
        this.towerSet = towerSet;
    }

    public TreeSet<String> getFlatSet() {
        return towerFlatSet;
    }

    public void setFlatSet(TreeSet<String> towerFlatSet) {
        this.towerFlatSet = towerFlatSet;
    }



    public AllSocietyData(){

    }

    protected AllSocietyData(Parcel in) {
        this.residentHashMap = (HashMap<String,Resident>) in.readSerializable();
        this.towerSet = (TreeSet<String>) in.readSerializable();
        this.towerFlatSet = (TreeSet<String>) in.readSerializable();
        this.carCleanerList = in.readArrayList(StaffDetails.class.getClassLoader());
        this.maidArrayList = in.readArrayList(StaffDetails.class.getClassLoader());
        this.guardArrayList = in.readArrayList(StaffDetails.class.getClassLoader());
        this.officeArrayList = in.readArrayList(StaffDetails.class.getClassLoader());
        this.othersArrayList = in.readArrayList(StaffDetails.class.getClassLoader());
    }

    public static final Creator<AllSocietyData> CREATOR = new Creator<AllSocietyData>() {
        @Override
        public AllSocietyData createFromParcel(Parcel in) {
            return new AllSocietyData(in);
        }

        @Override
        public AllSocietyData[] newArray(int size) {
            return new AllSocietyData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.residentHashMap);
        parcel.writeSerializable(this.towerSet);
        parcel.writeSerializable(this.towerFlatSet);
        parcel.writeList(this.carCleanerList);
        parcel.writeList(this.maidArrayList);
        parcel.writeList(this.guardArrayList);
        parcel.writeList(this.officeArrayList);
        parcel.writeList(this.othersArrayList);
    }
}
