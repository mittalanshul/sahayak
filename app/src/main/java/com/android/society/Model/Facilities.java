package com.android.society.Model;

/**
 * Created by 201101101 on 10/16/2017.
 */

public class Facilities {
    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    private String facilityName;
    private int iconID;

    public int getIconID() {
        return iconID;
    }

    public void setIconID(int iconID) {
        this.iconID = iconID;
    }
}
