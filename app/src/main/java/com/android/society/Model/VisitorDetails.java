package com.android.society.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anshul on 07/01/18.
 */

public class VisitorDetails implements Parcelable{

    private String visitorName;
    private String flatVisited;
    private String purpose;
    private String visitorNumber;
    private String vehicleName;
    private String date;
    private String time;
    private boolean isExited;
    private String dateExited;
    private String expectedVisitorId;
    private String imageURL;
    private String token;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }




    public VisitorDetails(){

    }

    protected VisitorDetails(Parcel in) {
        visitorName = in.readString();
        flatVisited = in.readString();
        purpose = in.readString();
        visitorNumber = in.readString();
        vehicleName = in.readString();
        date = in.readString();
        time = in.readString();
        isExited = in.readByte() != 0;
        dateExited = in.readString();
        expectedVisitorId = in.readString();
        timeExited = in.readString();
        token = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(visitorName);
        dest.writeString(flatVisited);
        dest.writeString(purpose);
        dest.writeString(visitorNumber);
        dest.writeString(vehicleName);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeByte((byte) (isExited ? 1 : 0));
        dest.writeString(dateExited);
        dest.writeString(expectedVisitorId);
        dest.writeString(timeExited);
        dest.writeString(token);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VisitorDetails> CREATOR = new Creator<VisitorDetails>() {
        @Override
        public VisitorDetails createFromParcel(Parcel in) {
            return new VisitorDetails(in);
        }

        @Override
        public VisitorDetails[] newArray(int size) {
            return new VisitorDetails[size];
        }
    };

    public String getDateExited() {
        return dateExited;
    }

    public void setDateExited(String dateExited) {
        this.dateExited = dateExited;
    }

    public String getTimeExited() {
        return timeExited;
    }

    public void setTimeExited(String timeExited) {
        this.timeExited = timeExited;
    }

    private String timeExited;

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getFlatVisited() {
        return flatVisited;
    }

    public void setFlatVisited(String flatVisited) {
        this.flatVisited = flatVisited;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getVisitorNumber() {
        return visitorNumber;
    }

    public void setVisitorNumber(String visitorNumber) {
        this.visitorNumber = visitorNumber;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isExited() {
        return isExited;
    }

    public void setExited(boolean exited) {
        isExited = exited;
    }

    public String getExpectedVisitorId() {
        return expectedVisitorId;
    }

    public void setExpectedVisitorId(String expectedVisitorId) {
        this.expectedVisitorId = expectedVisitorId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
