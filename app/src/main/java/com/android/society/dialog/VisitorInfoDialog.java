/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;

/**
 * Created by 201101101 on 10/31/2017.
 */

public class VisitorInfoDialog extends DialogFragment {

    private TextView textVisitorName;
    private TextView textVisitorNumber;
    private TextView textVisitorVehicleName;
    private TextView textFlatVisited;
    private TextView textPurpose;
    private TextView textEnteredAt;
    private TextView textExitedAt;
    private View rootView;
    private Visitor visitor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.visitor_info_dialog, container, false);
        getDialog().setTitle("Visitor Info");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setData();
    }

    private void initViews(){
        textVisitorName = (TextView)rootView.findViewById(R.id.text_visitor_name);
        textVisitorNumber = (TextView)rootView.findViewById(R.id.text_visitor_number);
        textVisitorVehicleName = (TextView)rootView.findViewById(R.id.text_vehicle_name);
        textFlatVisited = (TextView)rootView.findViewById(R.id.text_flat);
        textPurpose = (TextView)rootView.findViewById(R.id.text_purpose);
        textEnteredAt = (TextView)rootView.findViewById(R.id.text_visitor_created);
        textExitedAt = (TextView)rootView.findViewById(R.id.text_visitor_exited);

    }

    public void setVisitorData(Visitor paramVisitor){
        visitor = paramVisitor;
    }

    private void setData(){
        textVisitorName.setText(visitor.getVisitorName());
        textVisitorNumber.setText(visitor.getVisitorNumber());
        textVisitorVehicleName.setText(visitor.getVehicleName());
        if(Utils.getLoggedInMode(getActivity()).equalsIgnoreCase(Constants.MODE_RESIDENT)){
            textFlatVisited.setText(PreferenceManager.getInstance(getActivity()).getString(PreferenceManager.USER_FLAT));
        } else{
            textFlatVisited.setText(visitor.getFlatVisited());
        }
        textPurpose.setText(visitor.getPurpose());
        textEnteredAt.setText(visitor.getDate() + " " + visitor.getTime());
        if(!TextUtils.isEmpty(visitor.getDateExited())){
            rootView.findViewById(R.id.table_exited).setVisibility(View.VISIBLE);
            textExitedAt.setText(visitor.getDateExited() + " " + visitor.getTimeExited());
        }else{
            rootView.findViewById(R.id.table_exited).setVisibility(View.GONE);
        }


    }
}
