package com.android.society.dialog;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.society.Constants.Constants;
import com.android.society.Model.VisitorDetails;
import com.android.society.R;
import com.android.society.parser.VisitorDetailsParser;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.github.sumimakito.awesomeqr.AwesomeQRCode;

import java.io.ByteArrayOutputStream;

/**
 * Created by anshul on 04/01/18.
 */

public class ExpectedVisitorDialog extends DialogFragment implements View.OnClickListener{

    private LinearLayout mLinearAskPermission;
    private View rootView;
    private String jsonBody;
    private ImageView imageView;
    private Bitmap bitmap;
    private LinearLayout mLinearQR;

    public String getJsonBody() {
        return jsonBody;
    }

    public void setJsonBody(String jsonBody) {
        this.jsonBody = jsonBody;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.qr_code, container, false);
        getDialog().setTitle("Invitation");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView = (ImageView)rootView.findViewById(R.id.img_qr_code);
        mLinearAskPermission = (LinearLayout)rootView.findViewById(R.id.linear_ask_permission) ;
        mLinearQR = (LinearLayout)rootView.findViewById(R.id.linear_qr);
        mLinearAskPermission.setOnClickListener(this);
        generateQRCode();
    }

    private void generateQRCode(){

      new AwesomeQRCode.Renderer()
                .contents(jsonBody.toString())
                .size(300).margin(20)
                .renderAsync(new AwesomeQRCode.Callback() {
                    @Override
                    public void onRendered(AwesomeQRCode.Renderer renderer, final Bitmap bitmap) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(bitmap);
                            }
                        });
                    }

                    @Override
                    public void onError(AwesomeQRCode.Renderer renderer, Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case R.id.linear_ask_permission:
               bitmap = createBitmapFromView(mLinearQR);
               checkforWriteStoragePermission();
               break;
       }
    }

    public Bitmap createBitmapFromView(View v) {
        v.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bitmap);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return bitmap;
    }

    public void onClickApp(Bitmap bitmap,VisitorDetails visitorDetails) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "Invitation", null);
            Uri imageUri = Uri.parse(path);

            @SuppressWarnings("unused")
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            String coordinates = PreferenceManager.getInstance(getContext()).getString(PreferenceManager.SOCIETY_LATTITUDE)
                    +","+PreferenceManager.getInstance(getContext()).getString(PreferenceManager.SOCIETY_LONGITUDE);
            String googleLink = "http://maps.google.com/maps?q="+coordinates;

            String textTobeShared = "Hi " + visitorDetails.getVisitorName() +" , you are invited by "
                    + PreferenceManager.getInstance(getActivity()).getString(PreferenceManager.USER_NAME) + " to visit "
                    + PreferenceManager.getInstance(getActivity()).getString(PreferenceManager.USER_FLAT) + "\n"
                    + "Address is"
                    + PreferenceManager.getInstance(getActivity()).getString(PreferenceManager.SOCIETY_ADDRESS_1)
                    + PreferenceManager.getInstance(getActivity()).getString(PreferenceManager.SOCIETY_ADDRESS_2) +
                    " : " + googleLink;

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("image/*");
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(android.content.Intent.EXTRA_STREAM, imageUri);
            waIntent.putExtra(Intent.EXTRA_TEXT, textTobeShared);
            waIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            getActivity().startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (Exception e) {
            Utils.displayToast(getActivity(),"App not Installed");
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void checkforWriteStoragePermission(){
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .WRITE_EXTERNAL_STORAGE},
                                        Constants.REQUEST_PERMISSIONS);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .WRITE_EXTERNAL_STORAGE},
                        Constants.REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want
            if(!TextUtils.isEmpty(jsonBody)){
                Log.v("anshul","on generating " +jsonBody);
                VisitorDetails visitorDetails = VisitorDetailsParser.getVisitorDetails(jsonBody);
                if(visitorDetails != null){
                    onClickApp(bitmap,visitorDetails);
                }
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(!TextUtils.isEmpty(jsonBody)){
                        VisitorDetails visitorDetails = VisitorDetailsParser.getVisitorDetails(jsonBody);
                        if(visitorDetails != null){
                            onClickApp(bitmap,visitorDetails);
                        }
                    }
                }
                return;
            }
        }
    }

}



