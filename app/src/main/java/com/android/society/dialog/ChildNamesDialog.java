package com.android.society.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Child;
import com.android.society.Model.Staff;
import com.android.society.R;
import com.android.society.activity.BaseActivity;
import com.android.society.adapter.ChildRecyclerAdapter;
import com.android.society.callbacks.ChildSelectedCallback;
import com.android.society.parser.AllComplaintsParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by anshul on 28/12/17.
 */

public class ChildNamesDialog extends DialogFragment implements ChildSelectedCallback{

    private RecyclerView mRecyclerView;
    private LinearLayout mLinearAskPermission;
    private View rootView;
    private Child mChild;

    public String getResidentId() {
        return residentId;
    }

    public void setResidentId(String residentId) {
        this.residentId = residentId;
    }

    private String residentId;

    public ArrayList<Child> getChildArrayList() {
        return childArrayList;
    }

    public void setChildArrayList(ArrayList<Child> childArrayList) {
        this.childArrayList = childArrayList;
    }

    private ArrayList<Child> childArrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.child_names_dialog, container, false);
        getDialog().setTitle("Child Info");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setData();
    }

    private void initViews(){
     mRecyclerView = (RecyclerView)rootView.findViewById(R.id.child_names_recycler_view);
     mLinearAskPermission = (LinearLayout)rootView.findViewById(R.id.linear_ask_permission);
     mLinearAskPermission.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             if(Utils.isInternetAvailable(getActivity())){
                 if(mChild != null){
                   triggerCallToAskPermission(mChild);
                 }else{
                     Utils.displayToast(getActivity(),"Please select child to ask permission");
                 }
             }else{
                 Utils.displayToast(getActivity(),getString(R.string.internet_error));
             }

         }
     });
    }


    private void triggerCallToAskPermission(Child mChild){
        ((BaseActivity)getActivity()).showProgressDialog("Please wait .. Getting all children",false);
        String url = Constants.BASE_URL + Constants.CHILD_ENTRY_REQUEST+"?name="+mChild.getChildName()+"&id="+residentId;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ((BaseActivity)getActivity()).removeProgressDialog();
                        BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                        if(baseModel.isStatus()){
                           Utils.displayToast(getActivity(),"Resident has been requested for permission");
                           dismiss();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((BaseActivity)getActivity()).removeProgressDialog();
                        Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Utils.getHeaders(getActivity().getApplicationContext());
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



        };
        ((BaseActivity)getActivity()).queue.add(postRequest);
    }


    private void setData(){
       if(childArrayList != null && childArrayList.size()>0){
           ChildRecyclerAdapter childRecyclerAdapter = new ChildRecyclerAdapter(childArrayList);
           mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
           mRecyclerView.setAdapter(childRecyclerAdapter);
           childRecyclerAdapter.setChildSelectedCallback(this);
       }
    }

    @Override
    public void onChildSelected(Child child) {
        if(child != null){
            mChild = child;
        }
    }

    @Override
    public void onChildDeselected() {
       mChild = null;
    }
}


