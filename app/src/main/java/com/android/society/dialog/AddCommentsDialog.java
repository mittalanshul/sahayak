package com.android.society.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.society.R;
import com.android.society.callbacks.CommentAddedCallback;
import com.android.society.utils.Utils;

/**
 * Created by anshul on 12/01/18.
 */

public class AddCommentsDialog extends DialogFragment {

    private CommentAddedCallback commentAddedCallback;


    private View rootView;
    private EditText mEdtComent;
    private LinearLayout mLinearAddComment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_commets, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEdtComent = (EditText) rootView.findViewById(R.id.edt_comment);
        mLinearAddComment = (LinearLayout)rootView.findViewById(R.id.linear_continue) ;
        mLinearAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(mEdtComent.getText().toString().trim())){
                    if(commentAddedCallback != null){
                        commentAddedCallback.onCommentAdded(mEdtComent.getText().toString().trim());
                    }
                }else{
                    Utils.displayToast(getActivity(),"Please enter comment");
                }
            }
        });
    }


    public CommentAddedCallback getCommentAddedCallback() {
        return commentAddedCallback;
    }

    public void setCommentAddedCallback(CommentAddedCallback commentAddedCallback) {
        this.commentAddedCallback = commentAddedCallback;
    }
}
