/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Staff;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.activity.AttendenceActivity;
import com.android.society.callbacks.OnServiceAdded;
import com.android.society.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;

/**
 * Created by 201101101 on 11/8/2017.
 */

public class AddServiceDialog extends DialogFragment implements View.OnClickListener{

    private StaffDetails staffDetails;
    private OnServiceAdded onServiceAdded;
    private View rootView;
    private MaterialBetterSpinner materialDesignSpinnerService;
    private Button mButtonCreateAttendence;
    ArrayList<String> namesArrayList = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapterName;

    public ArrayList<StaffDetails> getStaffDetailsArrayList() {
        return staffDetailsArrayList;
    }

    public void setStaffDetailsArrayList(ArrayList<StaffDetails> staffDetailsArrayList) {
        this.staffDetailsArrayList = staffDetailsArrayList;
    }

    private ArrayList<StaffDetails> staffDetailsArrayList;

    public OnServiceAdded getOnServiceAdded() {
        return onServiceAdded;
    }

    public void setOnServiceAdded(OnServiceAdded onServiceAdded) {
        this.onServiceAdded = onServiceAdded;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_service_dialog, container, false);
        getDialog().setTitle("Add Service");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        for(StaffDetails staffDetails : staffDetailsArrayList){
            if(!TextUtils.isEmpty(staffDetails.getStaffName())){
                namesArrayList.add(staffDetails.getStaffName());
            }
        }
        arrayAdapterName = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, namesArrayList);
        materialDesignSpinnerService.setAdapter(arrayAdapterName);
        arrayAdapterName.notifyDataSetChanged();
        materialDesignSpinnerService.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String name = adapterView.getItemAtPosition(i).toString();
                getStaffDetailsSelected(name);
            }
        });
    }

    private void getStaffDetailsSelected(String name){
        for(StaffDetails staffDetails1:staffDetailsArrayList){
            if(staffDetails1.getStaffName().equalsIgnoreCase(name)){
                staffDetails = staffDetails1;
            }
        }
    }

    private void initViews(){
        materialDesignSpinnerService = (MaterialBetterSpinner)rootView. findViewById(R.id.spinner_service);
        mButtonCreateAttendence = (Button)rootView.findViewById(R.id.add_service);
        mButtonCreateAttendence.setOnClickListener(this);
    }

    private void setData(){

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_service:
                if(staffDetails != null){
                  if(onServiceAdded != null){
                      onServiceAdded.onServiceAdded(staffDetails);
                      dismiss();
                  }
                }else{
                    Utils.displayToast(getActivity().getApplicationContext(),"Please select name to add service");
                }
                break;
        }

    }
}
