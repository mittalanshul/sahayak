/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.utils.Utils;

/**
 * Created by 201101101 on 11/6/2017.
 */

public class ResidentInfoDialog extends DialogFragment {

    private TextView textVisitorName;
    private TextView textVisitorNumber;
    private TextView textVisitorVehicleName;
    private TextView textFlatVisited;
    private TextView textSlotInfo;
    private View rootView;
    private ResidentIn residentIn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.resident_info_dailog, container, false);
        getDialog().setTitle("Resident Info");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setData();
    }

    private void initViews(){
        textVisitorName = (TextView)rootView.findViewById(R.id.text_visitor_name);
        textVisitorNumber = (TextView)rootView.findViewById(R.id.text_visitor_number);
        textVisitorVehicleName = (TextView)rootView.findViewById(R.id.text_vehicle_name);
        textFlatVisited = (TextView)rootView.findViewById(R.id.text_flat);
        textSlotInfo = (TextView)rootView.findViewById(R.id.text_park_slot);
    }

    public void setResidentInData(ResidentIn paramResidentIn){
        residentIn = paramResidentIn;
    }

    private void setData(){
        textVisitorName.setText(residentIn.getResidentName());
        try {
            textVisitorNumber.setText(Utils.maskString(residentIn.getResidentNumber(),0,
                    residentIn.getResidentNumber().length()-2,'*'));
            textVisitorVehicleName.setText(residentIn.getVehicleName());
            textFlatVisited.setText(residentIn.getFlatVisited());
            textSlotInfo.setText(residentIn.getSlotName());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

