package com.android.society.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.society.Model.Resident;
import com.android.society.Model.ResidentIn;
import com.android.society.R;

/**
 * Created by anshul on 22/01/18.
 */

public class ResidentDirectoryInfoDialog extends DialogFragment {

    private TextView textVisitorName;
    private TextView textVisitorNumber;
    private TextView textVisitorVehicleName;
    private TextView textFlatVisited;
    private TextView textSlotInfo;
    private View rootView;
    private Resident residentIn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.resident_info_dailog, container, false);
        getDialog().setTitle("Resident Info");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setData();
    }

    private void initViews(){
        textVisitorName = (TextView)rootView.findViewById(R.id.text_visitor_name);
        textVisitorNumber = (TextView)rootView.findViewById(R.id.text_visitor_number);
        textVisitorVehicleName = (TextView)rootView.findViewById(R.id.text_vehicle_name);
        textFlatVisited = (TextView)rootView.findViewById(R.id.text_flat);
        textSlotInfo = (TextView)rootView.findViewById(R.id.text_park_slot);
    }

    public void setResidentInData(Resident paramResidentIn){
        residentIn = paramResidentIn;
    }

    private void setData(){
        textVisitorName.setText(residentIn.getName());
        textFlatVisited.setText(residentIn.getAddress1() + " - " + residentIn.getAddress2());
        textSlotInfo.setText(residentIn.getParkingSlot());
    }
}


