package com.android.society.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.Complaint;
import com.android.society.Model.Event;
import com.android.society.Model.MaintainenceDues;
import com.android.society.Model.Resident;
import com.android.society.Model.StaffDetails;
import com.android.society.Model.Token;
import com.android.society.Model.VisitorDetails;
import com.android.society.activity.AddEmergencyContacts;
import com.android.society.activity.AttendenceActivity;
import com.android.society.activity.ChildExitActionActivity;
import com.android.society.activity.ChildSafetyActivity;
import com.android.society.activity.ComplaintDetailActivity;
import com.android.society.activity.ComplaintDetailsActivity;
import com.android.society.activity.CreateComplaintActivity;
import com.android.society.activity.CreateEventActivity;
import com.android.society.activity.CreateNoticeActivity;
import com.android.society.activity.DialyServicesActivity;
import com.android.society.activity.DomesticServicesUsed;
import com.android.society.activity.EventDetailActivity;
import com.android.society.activity.EventExpandedActivity;
import com.android.society.activity.ExpectedResidentVisitorActivity;
import com.android.society.activity.ExpectedVisitorActivity;
import com.android.society.activity.FacilitiesDetailActivity;
import com.android.society.activity.FacilityTypeActivity;
import com.android.society.activity.KnowYourSocietyLogin;
import com.android.society.activity.MaintainenceDuesActivity;
import com.android.society.activity.NoticeDetailActivity;
import com.android.society.activity.NoticeExpandActivity;
import com.android.society.activity.PanicAlertActivity;
import com.android.society.activity.ParkingEntryActvity;
import com.android.society.activity.RWALoginActivity;
import com.android.society.activity.ResidentEntryActivity;
import com.android.society.activity.ResidentFlatListActivity;
import com.android.society.activity.ResidentMaintainenceHistoryActivity;
import com.android.society.activity.ResidentProfileActivity;
import com.android.society.activity.ResidentVisitorHistory;
import com.android.society.activity.ResidentsActivity;
import com.android.society.activity.SignInActivity;
import com.android.society.activity.SocietyFacilitiesActivity;
import com.android.society.activity.StaffAttendenceActivity;
import com.android.society.activity.StaffEntryRegister;
import com.android.society.activity.VisitorActionActivity;
import com.android.society.activity.VisitorDetailForm;
import com.android.society.activity.VisitorEntryActivity;
import com.android.society.activity.VisitorExitActionActivity;
import com.android.society.activity.VisitorRegisterActivity;

import java.util.ArrayList;

/**
 * Created by 201101101 on 10/16/2017.
 */

public class NavigationUtils {

    public static void navigateToSignUp(Context context){
        Intent intent = new Intent(context, SignInActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToAddEmergencyContacts(Context context , AllSocietyData allSocietyData){
        Intent intent = new Intent(context, AddEmergencyContacts.class);
        intent.putExtra("all_data",allSocietyData);
        context.startActivity(intent);
    }

    public static void navigateToMaintainence(Context context, MaintainenceDues maintainenceDues){
        Intent intent = new Intent(context, MaintainenceDuesActivity.class);
        intent.putExtra("main_id",maintainenceDues);
        context.startActivity(intent);
    }

    public static void navigateToSocietyFacilities(Context context){
        Intent intent = new Intent(context, SocietyFacilitiesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void navigateToLogin(Context context,String mode){
        Intent intent = new Intent(context, RWALoginActivity.class);
        intent.putExtra("mode",mode);
        ((Activity)context).startActivityForResult(intent,1);
    }

    public static void navigateToModeLogin(Context context){
        Intent intent = new Intent(context, KnowYourSocietyLogin.class);
        context.startActivity(intent);
    }

    public static void navigateToEventDetails(Context context){
        Intent intent = new Intent(context, EventDetailActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToNoticeDetails(Context context){
        Intent intent = new Intent(context, NoticeDetailActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToFacilitiesDetails(Context context , AllSocietyData allSocietyData){
        Intent intent = new Intent(context, FacilitiesDetailActivity.class);
        intent.putExtra("all_data",allSocietyData);
        context.startActivity(intent);
    }

    public static void navigateToComplaintDetails(Context context){
        Intent intent = new Intent(context, ComplaintDetailsActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToComplaint(Context context, Complaint complaint){
        Intent intent = new Intent(context, ComplaintDetailActivity.class);
        intent.putExtra("complaint",complaint);
        ((Activity)context).startActivityForResult(intent,1234);
    }

    public static void navigateToEvent(Context context, Event event){
        Intent intent = new Intent(context, EventExpandedActivity.class);
        intent.putExtra("event",event);
        context.startActivity(intent);
    }

    public static void navigateToNotice(Context context, Event event){
        Intent intent = new Intent(context, NoticeExpandActivity.class);
        intent.putExtra("event",event);
        context.startActivity(intent);
    }

    public static void navigateToResidentSection(Context context, AllSocietyData allSocietyData ,
                                                 boolean isFromChildSafety, boolean isFromPanic){
        Intent intent = new Intent(context, ResidentsActivity.class);
        intent.putExtra("all_data",allSocietyData);
        intent.putExtra("child_safety",isFromChildSafety);
        intent.putExtra("panic",isFromPanic);
        ((Activity)context).startActivityForResult(intent,43);
    }

    public static void navigateToMainLogin(Context context){
        Intent intent = new Intent(context, KnowYourSocietyLogin.class);
        context.startActivity(intent);
    }

    public static void navigateToResidentFlatsSection(Context context,
                                                      String towerName,AllSocietyData allSocietyData,
                                                      boolean isFromChildSafety , boolean isFromPanic){
        Intent intent = new Intent(context, ResidentFlatListActivity.class);
        intent.putExtra("all_data",allSocietyData);
        intent.putExtra("tower",towerName);
        intent.putExtra("child_safety",isFromChildSafety);
        intent.putExtra("panic",isFromPanic);
        ((Activity)context).startActivityForResult(intent,42);
    }

    public static void navigateToAddEvent(Context context){
        Intent intent = new Intent(context, CreateEventActivity.class);
        ((Activity)context).startActivityForResult(intent,4);
    }

    public static void navigateToAddNotice(Context context){
        Intent intent = new Intent(context, CreateNoticeActivity.class);
        ((Activity)context).startActivityForResult(intent,10);
    }

    public static void navigateToAddComplaint(Context context){
        Intent intent = new Intent(context, CreateComplaintActivity.class);
        ((Activity)context).startActivityForResult(intent,5);
    }

    public static void navigateToRWAMembers(Context context){
        Intent intent = new Intent(context, SocietyFacilitiesActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToFacilityType(Context context, String type ,
                                              ArrayList<StaffDetails> staffDetailsArrayList){
        Intent intent = new Intent(context, FacilityTypeActivity.class);
        intent.putExtra(Constants.TYPE,type);
        intent.putParcelableArrayListExtra("list",staffDetailsArrayList);
        context.startActivity(intent);
    }

    public static void navigateToVisitorEntry(Context context , AllSocietyData allSocietyData,boolean isFromExpectedVisitor){
        Intent intent = new Intent(context, VisitorEntryActivity.class);
        intent.putExtra("all_data",allSocietyData);
        intent.putExtra("is_from_expected",isFromExpectedVisitor);
        context.startActivity(intent);
    }

    public static void navigateToVisitorEntryFromBarcode(Context context , VisitorDetails visitorDetails, boolean isFromExpectedVisitor){
        Intent intent = new Intent(context, VisitorEntryActivity.class);
        intent.putExtra("is_from_expected",isFromExpectedVisitor);
        intent.putExtra("visitor_details",visitorDetails);
        context.startActivity(intent);
    }

    public static void navigateToExpectedVisitor(Context context){
        if(Utils.getLoggedInMode(context).equalsIgnoreCase(Constants.MODE_RESIDENT)){
            Intent intent = new Intent(context, ExpectedVisitorActivity.class);
            context.startActivity(intent);
        }else if(Utils.getLoggedInMode(context).equalsIgnoreCase(Constants.MODE_GUARD)){
            Intent intent = new Intent(context, ExpectedVisitorActivity.class);
            context.startActivity(intent);
        }

    }

    public static void navigateToDialyServices(Context context){
        Intent intent = new Intent(context, DialyServicesActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToVisitorEntryRegister(Context context){
        Intent intent = new Intent(context, VisitorRegisterActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToStaffEntryRegister(Context context){
        Intent intent = new Intent(context, StaffEntryRegister.class);
        context.startActivity(intent);
    }

    public static void navigateToAttendence(Context context , AllSocietyData allSocietyData){
        Intent intent = new Intent(context, AttendenceActivity.class);
        intent.putExtra("all_data",allSocietyData);
        context.startActivity(intent);
    }

    public static void navigateToResidentVisitorHistory(Context context){
        Intent intent = new Intent(context, ResidentVisitorHistory.class);
        context.startActivity(intent);
    }

    public static void navigateToStaffAttendence(Context context , AllSocietyData allSocietyData){
        Intent intent = new Intent(context, StaffAttendenceActivity.class);
        intent.putExtra("all_data",allSocietyData);
        context.startActivity(intent);
    }

    public static void navigateToResidentEntry(Context context , AllSocietyData allSocietyData){
        Intent intent = new Intent(context, ResidentEntryActivity.class);
        intent.putExtra("all_data",allSocietyData);
        context.startActivity(intent);
    }

    public static void navigateToAddServices(Context context){
        Intent intent = new Intent(context, DomesticServicesUsed.class);
        context.startActivity(intent);
    }

    public static void navigateToResidentProfile(Context context, Resident resident){
        Intent intent = new Intent(context, ResidentProfileActivity.class);
        intent.putExtra("resident",resident);
        context.startActivity(intent);
    }

    public static void navigateToPanicAlert(Context context, String data,String message){
        Intent intent = new Intent(context, PanicAlertActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.NOTIFICATION_DATA,data);
        intent.putExtra("message",message);
        context.startActivity(intent);
    }

    public static void navigateToChildSafety(Context context , String data){
        Intent intent = new Intent(context, ChildSafetyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.NOTIFICATION_DATA , data);
        context.startActivity(intent);
    }

    public static void navigateToChildSafetyAction(Context context , String data){
        Intent intent = new Intent(context, ChildExitActionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.NOTIFICATION_DATA , data);
        context.startActivity(intent);
    }

    public static void navigateToVisitorSafetyAction(Context context , String data){
        Intent intent = new Intent(context, VisitorActionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.NOTIFICATION_DATA , data);
        context.startActivity(intent);
    }

    public static void navigateToMaintainenceDuesById(Context context , String data , String id){
        Intent intent = new Intent(context, MaintainenceDuesActivity.class);
        intent.putExtra(Constants.NOTIFICATION_DATA , data);
        intent.putExtra("ID" , id);
        context.startActivity(intent);
    }


    public static void navigateToMaintainenceDuesHistory(Context context){
        Intent intent = new Intent(context, ResidentMaintainenceHistoryActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToVisitorDetailForm(Context context , String data , String type){
        Intent intent = new Intent(context, VisitorDetailForm.class);
        intent.putExtra(Constants.NOTIFICATION_DATA , data);
        intent.putExtra(Constants.NOTIFICATION_TYPE , type);
        context.startActivity(intent);
    }

    public static void navigateToLogout(Context context){
        Utils.clearDataOnLogout(context);
    }
}
