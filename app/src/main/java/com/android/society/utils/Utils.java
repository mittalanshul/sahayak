package com.android.society.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.Resident;
import com.android.society.Model.Token;
import com.android.society.Model.User;
import com.android.society.R;
import com.android.society.activity.SocietyFacilitiesActivity;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.R.attr.mode;

/**
 * Created by 201101101 on 10/17/2017.
 */

public class Utils {

    public static void displayToast(Context context ,String message){
        if(!TextUtils.isEmpty(message)){
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isUserLoggedIn(Context context){
        return PreferenceManager.getInstance(context).getBool(PreferenceManager.IS_LOGGED_IN);
    }

    public static String getLoggedInMode(Context context){
        return PreferenceManager.getInstance(context).getString(PreferenceManager.IS_LOGGED_IN_MODE, Constants.MODE_RWA);
    }

    public static String getToken(Context context){
        Log.v("anshul","token is " + PreferenceManager.getInstance(context).getString(PreferenceManager.TOKEN, ""));
        return PreferenceManager.getInstance(context).getString(PreferenceManager.TOKEN, "");
    }



    public static void sendNotification(Context context ,String messageBody,String title) {
        Intent intent = new Intent(context, SocietyFacilitiesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.app_icon)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    public static Map<String,String> getHeaders(Context context){
        Map<String,String> headersMap = new HashMap<>();
        headersMap.put(Constants.AUTHORIZATION_HEADER,Utils.getToken(context));
        return headersMap;
    }

    public static void saveUserData(Context context ,User user,String mode){
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.USER_NAME, user.getName());
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.USER_EMAIL, user.getEmail());
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.IS_LOGGED_IN_MODE, mode);
        if(user.getToken() != null && !TextUtils.isEmpty(user.getToken().getToken())){
            PreferenceManager.getInstance(context).
                    writeToPrefs(PreferenceManager.TOKEN, user.getToken().getToken());
            Dao.getInstance().setToken(user.getToken());
        }

        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.IDENTIFICATION, user.getIdentification());
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.IS_LOGGED_IN, true);
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.ID, user.getId());
    }

    public static void saveResidentData(Context context , Resident resident, String mode){
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.USER_NAME, resident.getName());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.USER_FLAT, resident.getAddress1() + " - " + resident.getAddress2());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.IS_LOGGED_IN_MODE, mode);
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.IS_LOGGED_IN, true);
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.IDENTIFICATION, resident.getId());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.ID, resident.getId());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.LOGO, resident.getLogo());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.SOCIETY_ADDRESS_1,resident.getAddress1());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.SOCIETY_ADDRESS_2,resident.getAddress2());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.SOCIETY_LATTITUDE,resident.getLattitude());
        PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.SOCIETY_LONGITUDE,resident.getLongitude());
    }

    public static void saveResidentData(Context context , Resident resident){
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.USER_NAME, resident.getName());
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.USER_FLAT, resident.getAddress1() + " - " + resident.getAddress2());
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.IS_LOGGED_IN, true);
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.IDENTIFICATION, resident.getId());
        PreferenceManager.getInstance(context).
                writeToPrefs(PreferenceManager.ID, resident.getId());
    }

    /**
     * Check Internet connection is available or not.
     *
     * @param context is the {@link Context} of the {@link Activity}.
     * @return <b>true</b> is Internet connection is available.
     */
    public static boolean isInternetAvailable(Context context) {
        boolean isInternetAvailable = false;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager
                    .getActiveNetworkInfo();

            if (networkInfo != null && (networkInfo.isConnected())) {
                isInternetAvailable = true;
            }
        } catch (Exception exception) {
            // Do Nothing
        }

        return isInternetAvailable;
    }

    public static String getCurrentDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return  simpleDateFormat.format(new Date());
    }

    public static String getCurrentTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        return  simpleDateFormat.format(new Date());
    }

    public static String changeTimeFormat(String time){
        Date dt = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        return  sdf.format(dt);
    }

    public static boolean isNumberValid(String mobile,Context mContext) {
        if (TextUtils.isEmpty(mobile)) {
            Utils.displayToast(mContext,"Please enter number");
            return false;
        }else if(mobile.length()<10){
            Utils.displayToast(mContext,"Mobile number should be of 10 digits");
        }else if(mobile.startsWith("+")){
            Utils.displayToast(mContext,"Invalid number");
        }
        return true;
    }

    public static void hideKeyboard(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm != null) {
                imm.hideSoftInputFromWindow(((Activity) context)
                        .getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String maskString(String strText, int start, int end, char maskChar)
            throws Exception{

        if(strText == null || strText.equals(""))
            return "";

        if(start < 0)
            start = 0;

        if( end > strText.length() )
            end = strText.length();

        if(start > end)
            throw new Exception("End index cannot be greater than start index");

        int maskLength = end - start;

        if(maskLength == 0)
            return strText;

        StringBuilder sbMaskString = new StringBuilder(maskLength);

        for(int i = 0; i < maskLength; i++){
            sbMaskString.append(maskChar);
        }

        return strText.substring(0, start)
                + sbMaskString.toString()
                + strText.substring(start + maskLength);
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    /**
     * Turn drawable resource into byte array.
     *
     * @param context parent context
     * @param id      drawable resource id
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static void clearDataOnLogout(Context context){
        PreferenceManager.getInstance(context).remove(PreferenceManager.USER_EMAIL);
        PreferenceManager.getInstance(context).remove(PreferenceManager.USER_NUMBER);
        PreferenceManager.getInstance(context).remove(PreferenceManager.USER_REGISTER_NUMBER);
        PreferenceManager.getInstance(context).remove(PreferenceManager.USER_NAME);
        PreferenceManager.getInstance(context).remove(PreferenceManager.IS_LOGGED_IN_MODE);
        PreferenceManager.getInstance(context).remove(PreferenceManager.TOKEN);
        Dao.getInstance().setToken(null);
        PreferenceManager.getInstance(context).remove(PreferenceManager.IS_LOGGED_IN);
        PreferenceManager.getInstance(context).remove(PreferenceManager.USER_FLAT);
        PreferenceManager.getInstance(context).remove(PreferenceManager.SOCIETY_ADDRESS_1);
        PreferenceManager.getInstance(context).remove(PreferenceManager.SOCIETY_ADDRESS_2);
        PreferenceManager.getInstance(context).remove(PreferenceManager.SOCIETY_LATTITUDE);
        PreferenceManager.getInstance(context).remove(PreferenceManager.SOCIETY_LONGITUDE);
    }
}
