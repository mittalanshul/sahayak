/**
 * Copyright (c) 2018 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.Resident;
import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.activity.BaseActivity;
import com.android.society.activity.SocietyFacilitiesActivity;
import com.android.society.adapter.ResidentEntryListAdapter;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;
import com.android.society.dialog.ResidentInfoDialog;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by 201101101 on 11/6/2017.
 */

public class ResidentInFragment extends Fragment implements OnVisitorInteractionCallbacks {

    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<ResidentIn> residentInRealmResults;
    private ResidentEntryListAdapter visitorEntryListAdapter;
    private String residentNumber;


    public ResidentInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_in, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        residentInRealmResults = Realm.getDefaultInstance()
                .where(ResidentIn.class)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }

    public void loadDefaultData(){
        residentInRealmResults = Realm.getDefaultInstance()
                .where(ResidentIn.class)
                .findAll();
        if(residentInRealmResults != null && residentInRealmResults.size() >0){
            if(visitorEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                visitorEntryListAdapter.setResidentInRealmResults(residentInRealmResults);
                visitorEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        visitorEntryListAdapter = new ResidentEntryListAdapter(residentInRealmResults);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(visitorEntryListAdapter);
        visitorEntryListAdapter.setOnVisitorInteractionCallbacks(this);
    }

    @Override
    public void onVisitorExited(final Visitor visitor) {

    }

    public void refreshVisitorsDataOnQuery(String name){
        RealmResults<ResidentIn> queriedVisitors = Realm.getDefaultInstance()
                .where(ResidentIn.class)
                .contains("residentName",name)
                .or().contains("residentNumber",name)
                .or().contains("flatVisited",name)
                .or().contains("vehicleName",name)
                .or().contains("slotName",name)
                .findAll();
        if(queriedVisitors != null && queriedVisitors.size() >0){
            if(visitorEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                visitorEntryListAdapter.setResidentInRealmResults(queriedVisitors);
                visitorEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onVisitorClicked(Visitor visitor) {

    }

    @Override
    public void onResidentClicked(ResidentIn residentIn) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ResidentInfoDialog residentInfoDialog = new ResidentInfoDialog ();
        residentInfoDialog.setResidentInData(residentIn);
        residentInfoDialog.setCancelable(true);
        residentInfoDialog.show(fm, "Sample Fragment");
    }

    public  boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getActivity(),android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {
               requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            makeCall(residentNumber);
            Log.v("TAG","Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission granted", Toast.LENGTH_SHORT).show();
                    makeCall(residentNumber);
                } else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onResidentCallMake(ResidentIn residentIn) {
        if(residentIn != null && !TextUtils.isEmpty(residentIn.getResidentNumber())){
            residentNumber = residentIn.getResidentNumber();
            if(isPermissionGranted()){
                makeCall(residentNumber);
            }
        }

    }

    private void makeCall(String number){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        startActivity(callIntent);
    }
}

