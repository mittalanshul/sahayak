package com.android.society.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.adapter.VisitorEntryListAdapter;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;
import com.android.society.dialog.VisitorInfoDialog;

import java.util.logging.Logger;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisitorOutFragment extends Fragment implements OnVisitorInteractionCallbacks {
    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<Visitor> visitors;
    private VisitorEntryListAdapter visitorEntryListAdapter;
    public static boolean isCacheDirty;


    public VisitorOutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_out, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        isCacheDirty = false;
        visitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .equalTo("isExited",true)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }

    public void loadDefaultData(){
        visitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .equalTo("isExited",true)
                .findAll();
        if(visitors != null && visitors.size() >0){
            if(visitorEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                visitorEntryListAdapter.setVisitorsList(visitors);
                visitorEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        VisitorOutFragment visitorOutFragment = new VisitorOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    public void refreshVisitorsDataOnQuery(String name){
        RealmResults<Visitor> queriedVisitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .contains("visitorName",name)
                .or().contains("visitorNumber",name)
                .or().contains("flatVisited",name)
                .or().contains("vehicleName",name)
                .equalTo("isExited",true)
                .findAll();
        if(queriedVisitors != null && queriedVisitors.size() >0){
            if(visitorEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                visitorEntryListAdapter.setVisitorsList(queriedVisitors);
                visitorEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        VisitorOutFragment visitorOutFragment = new VisitorOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        visitorEntryListAdapter = new VisitorEntryListAdapter(visitors,getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(visitorEntryListAdapter);
        visitorEntryListAdapter.setOnVisitorInteractionCallbacks(this);
    }

    public void makeCacheDirty(boolean val){
        isCacheDirty = val;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser && isCacheDirty){
            refreshVisitorData();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void refreshVisitorData(){
        visitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .equalTo("isExited",true)
                .findAll();
        if(visitorEntryListAdapter != null){
            visitorEntryListAdapter.setVisitorsList(visitors);
            visitorEntryListAdapter.notifyDataSetChanged();
            makeCacheDirty(false);
        }

    }

    @Override
    public void onVisitorExited(Visitor visitor) {

    }

    @Override
    public void onVisitorClicked(Visitor visitor) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        VisitorInfoDialog visitorInfoDialog = new VisitorInfoDialog ();
        visitorInfoDialog.setVisitorData(visitor);
        visitorInfoDialog.setCancelable(true);
        visitorInfoDialog.show(fm, "Sample Fragment");
    }

    @Override
    public void onResidentClicked(ResidentIn residentIn) {

    }

    @Override
    public void onResidentCallMake(ResidentIn residentIn) {

    }
}
