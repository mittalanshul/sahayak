package com.android.society.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.callbacks.OnServiceAdded;
import com.android.society.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;


public class AddNewDomesticFragment extends Fragment implements View.OnClickListener{

    private StaffDetails staffDetails;
    private OnServiceAdded onServiceAdded;
    private View rootView;
    private MaterialBetterSpinner materialDesignSpinnerService;
    private Button mButtonCreateAttendence;
    ArrayList<String> namesArrayList = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapterName;

    public ArrayList<StaffDetails> getStaffDetailsArrayList() {
        return staffDetailsArrayList;
    }

    public void setStaffDetailsArrayList(ArrayList<StaffDetails> staffDetailsArrayList) {
        this.staffDetailsArrayList = staffDetailsArrayList;
    }

    private ArrayList<StaffDetails> staffDetailsArrayList;

    public OnServiceAdded getOnServiceAdded() {
        return onServiceAdded;
    }

    public void setOnServiceAdded(OnServiceAdded onServiceAdded) {
        this.onServiceAdded = onServiceAdded;
    }


    public AddNewDomesticFragment() {
        // Required empty public constructor
    }


    public static AddNewDomesticFragment newInstance(String param1, String param2) {
        AddNewDomesticFragment fragment = new AddNewDomesticFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_service_dialog, container, false);
        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        if(staffDetailsArrayList != null && staffDetailsArrayList.size() >0){
            for(StaffDetails staffDetails : staffDetailsArrayList){
                if(!TextUtils.isEmpty(staffDetails.getStaffName())){
                    namesArrayList.add(staffDetails.getStaffName());
                }
            }
            arrayAdapterName = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, namesArrayList);
            materialDesignSpinnerService.setAdapter(arrayAdapterName);
            arrayAdapterName.notifyDataSetChanged();
            materialDesignSpinnerService.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String name = adapterView.getItemAtPosition(i).toString();
                    getStaffDetailsSelected(name);
                }
            });
        }else{
            materialDesignSpinnerService.setVisibility(View.GONE);
            mButtonCreateAttendence.setVisibility(View.GONE);
            if(getActivity() != null){
                Utils.displayToast(getActivity(),"No service found to addßßß");
                getActivity().finish();
            }

        }

    }

    private void getStaffDetailsSelected(String name){
        for(StaffDetails staffDetails1:staffDetailsArrayList){
            if(staffDetails1.getStaffName().equalsIgnoreCase(name)){
                staffDetails = staffDetails1;
            }
        }
    }

    private void initViews(){
        materialDesignSpinnerService = (MaterialBetterSpinner)rootView. findViewById(R.id.spinner_service);
        mButtonCreateAttendence = (Button)rootView.findViewById(R.id.add_service);
        mButtonCreateAttendence.setOnClickListener(this);
    }

    private void setData(){

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_service:
                if(staffDetails != null){
                    if(onServiceAdded != null){
                        onServiceAdded.onServiceAdded(staffDetails);
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                }else{
                    Utils.displayToast(getActivity().getApplicationContext(),"Please select name to add service");
                }
                break;
        }

    }
}
