package com.android.society.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.society.R;
import com.android.society.callbacks.CommentAddedCallback;
import com.android.society.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EnterCommentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EnterCommentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EnterCommentFragment extends Fragment implements View.OnClickListener{

    private View view;

    public CommentAddedCallback getCommentAddedCallback() {
        return commentAddedCallback;
    }

    public void setCommentAddedCallback(CommentAddedCallback commentAddedCallback) {
        this.commentAddedCallback = commentAddedCallback;
    }

    private CommentAddedCallback commentAddedCallback;


    private View rootView;
    private EditText mEdtComent;
    private LinearLayout mLinearAddComment;
    private ImageView imgCross;

    public EnterCommentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EnterCommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EnterCommentFragment newInstance(String param1, String param2) {
        EnterCommentFragment fragment = new EnterCommentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEdtComent = (EditText) view.findViewById(R.id.edt_comment);
        imgCross = (ImageView) view.findViewById(R.id.img_cross);
        mLinearAddComment = (LinearLayout)view.findViewById(R.id.linear_continue) ;
        mLinearAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(mEdtComent.getText().toString().trim())){
                    if(commentAddedCallback != null){
                        commentAddedCallback.onCommentAdded(mEdtComent.getText().toString().trim());
                        onDetach();
                    }
                }else{
                    Utils.displayToast(getActivity(),"Please enter your comment");
                }
            }
        });

        initOnClickListener();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void initOnClickListener() {
        imgCross.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_enter_comment, container, false);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        commentAddedCallback = null;
    }

    @Override
    public void onClick(View v) {
        switch (view.getId()){
            case R.id.img_cross:
                mEdtComent.setText("");
                Utils.hideKeyboard(getActivity());
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
