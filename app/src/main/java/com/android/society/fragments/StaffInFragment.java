/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.society.Constants.Constants;
import com.android.society.Model.Staff;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.activity.BaseActivity;
import com.android.society.adapter.StaffEntryListAdapter;
import com.android.society.callbacks.OnStaffInteractionCallback;
import com.android.society.dialog.StaffInfoDialog;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class StaffInFragment extends Fragment implements OnStaffInteractionCallback {

    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<Staff> staffRealmResults;
    private StaffEntryListAdapter staffEntryListAdapter;
    private RealmChangeListener realmListener;


    public StaffInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_in, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        staffRealmResults = Realm.getDefaultInstance()
                .where(Staff.class)
                .equalTo("isExited",false)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        staffEntryListAdapter = new StaffEntryListAdapter(staffRealmResults);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(staffEntryListAdapter);
        staffEntryListAdapter.setOnStaffInteractionCallback(this);
    }



    private void refreshStaffEntryData(){
        staffRealmResults = Realm.getDefaultInstance()
                .where(Staff.class)
                .equalTo("isExited",false)
                .findAll();
        if(staffRealmResults != null && staffRealmResults.size() >0){
            setmRecyclerViewData();
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        StaffOutFragment staffOutFragment = new StaffOutFragment();
        staffOutFragment.makeCacheDirty(true);
    }

    @Override
    public void onStaffExited(Staff staff) {
        if(staff != null){
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Staff staffToEdit = realm.where(Staff.class)
                    .equalTo("staffId", staff.getStaffId())
                    .equalTo("isExited",false)
                    .findFirst();
            staffToEdit.setExited(true);
            realm.commitTransaction();
            refreshStaffEntryData();
            makeStaffEntryOnServer(staff);

        }


    }

    private void makeStaffEntryOnServer(Staff staff){
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id",staff.getStaffId());
            jsonBody.put("exit", Utils.getCurrentDate() + " " + Utils.getCurrentTime());
            jsonBody.put("entry",staff.getDate() + " " + staff.getTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();
        if(Utils.isInternetAvailable(getActivity())){
            String url = Constants.BASE_URL + Constants.CREATE_VISITOR_ENTRY_URL ;
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("anshul","entry created on server");
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getActivity().getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }
            };
            ((BaseActivity)getActivity()).queue.add(postRequest);
        }
    }

    @Override
    public void onStaffClicked(Staff staff) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        StaffInfoDialog staffInfoDialog = new StaffInfoDialog ();
        staffInfoDialog.setStaffData(staff);
        staffInfoDialog.setCancelable(true);
        staffInfoDialog.show(fm, "Sample Fragment");
    }
}

