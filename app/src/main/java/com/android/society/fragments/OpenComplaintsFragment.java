/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Complaint;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.activity.BaseActivity;
import com.android.society.activity.ComplaintDetailsActivity;
import com.android.society.adapter.RecyclerComplaintsAdapter;
import com.android.society.callbacks.OnComplaintAdded;
import com.android.society.parser.AllComplaintsParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class OpenComplaintsFragment extends Fragment implements OnComplaintAdded {

    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<Visitor> visitors;
    private RecyclerComplaintsAdapter mRecyclerViewComplaintsAdapter;
    private ArrayList<Complaint> complaintArrayList;



    public OpenComplaintsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_complaint, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        showEmptyView();
        triggerCallToGetAllComplaints();
        super.onViewCreated(view, savedInstanceState);
    }

    private void triggerCallToGetAllComplaints(){
        if(Utils.isInternetAvailable(getActivity().getApplicationContext())){
            ((BaseActivity)getActivity()).showProgressDialog("Please wait .. Getting all complaints",false);
            String url = "";
            if(PreferenceManager.getInstance(getActivity()).getString(PreferenceManager.IS_LOGGED_IN_MODE)
                    .equalsIgnoreCase(Constants.MODE_RWA)){
                url = Constants.BASE_URL + Constants.GET_ALL_COMPLAINTS+"?status=pending";
            }else if(PreferenceManager.getInstance(getActivity()).getString(PreferenceManager.IS_LOGGED_IN_MODE)
                    .equalsIgnoreCase(Constants.MODE_RESIDENT)){
                url = Constants.BASE_URL + Constants.GET_ALL_RESIDENT_COMPLAINTS+"?status=pending";
            }
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("anshul" ,"triggerCallToGetAllComplaints -- >> " + response);
                            if(getActivity() != null){
                                ((BaseActivity)getActivity()).removeProgressDialog();
                            }
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                complaintArrayList = AllComplaintsParser.getAllComplaints(response);
                                if(complaintArrayList != null && complaintArrayList.size()>0){
                                    Log.v("anshul","onResponse" + complaintArrayList.size());
                                    showEmptyView();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((BaseActivity)getActivity()).removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getActivity().getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }



            };
            ((BaseActivity)getActivity()).queue.add(postRequest);
        }else{
            Utils.displayToast(getActivity().getApplicationContext(),"Please check your internet connection");
        }
    }

    private void initViews(){
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view_complaint);
    }

    private void showEmptyView() {
        if (complaintArrayList != null && complaintArrayList.size() > 0) {
            mView.findViewById(R.id.empty_view).setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            setRecyclerViewData();
        } else {
            mView.findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    private void setRecyclerViewData() {
        mRecyclerViewComplaintsAdapter = new RecyclerComplaintsAdapter(complaintArrayList,getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewComplaintsAdapter.setOnComplaintAddedCall(this);
        mRecyclerView.setAdapter(mRecyclerViewComplaintsAdapter);
    }

    @Override
    public void omComplaintAdded(Complaint complaint) {
        Log.v("anshul","omComplaintAdded fragment called");
      if(complaintArrayList != null && complaintArrayList.size()>0){
          complaintArrayList.add(0,complaint);
      }else{
          complaintArrayList = new ArrayList<>();
          complaintArrayList.add(complaint);
      }
        if(mRecyclerViewComplaintsAdapter != null){
            mRecyclerView.setVisibility(View.VISIBLE);
            mRecyclerViewComplaintsAdapter.setEventArrayList(complaintArrayList);
            mRecyclerViewComplaintsAdapter.notifyDataSetChanged();
            mView.findViewById(R.id.empty_view).setVisibility(View.GONE);
        }
    }

    @Override
    public void omComplaintDelete(Complaint complaint) {
        ((ComplaintDetailsActivity)getActivity()).triggerCallToDeleteComplaint(complaint);
    }

    @Override
    public void omComplaintClicked(Complaint complaint) {
        NavigationUtils.navigateToComplaint(getActivity(),complaint);
    }

    public void refreshVisitorsDataOnQuery(String name){

        ArrayList<Complaint> complaintArrayListLocal = new ArrayList<>();
        for(Complaint complaint : complaintArrayList){
            if(complaint.getResident().contains(name) || complaint.getSubject().contains(name)
                    || complaint.getDesc().contains(name) || complaint.getId().contains(name)
                    || complaint.getType().contains(name)){
                complaintArrayListLocal.add(complaint);
            }
        }
        if(complaintArrayListLocal != null && complaintArrayListLocal.size() >0){
            if(mRecyclerViewComplaintsAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                mRecyclerViewComplaintsAdapter.setEventArrayList(complaintArrayListLocal);
                mRecyclerViewComplaintsAdapter.notifyDataSetChanged();
            }
        } else{
           showEmptyView();
        }
    }

    public void refreshComplaints(Complaint complaint){

        if(complaintArrayList != null && complaintArrayList.size() >0){
            complaintArrayList.remove(complaint);
            if(complaintArrayList.size() >0){
                if(mRecyclerViewComplaintsAdapter != null){
                    mRecyclerViewComplaintsAdapter.setEventArrayList(complaintArrayList);
                    mRecyclerViewComplaintsAdapter.notifyDataSetChanged();
                }
            }   else{
                showEmptyView();
            }
        } else{
            showEmptyView();
        }
    }

    public void refreshStatus(){
        triggerCallToGetAllComplaints();
    }

    public void loadDefaultData(){
        if(complaintArrayList != null && complaintArrayList.size() >0){
            if(mRecyclerViewComplaintsAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                mRecyclerViewComplaintsAdapter.setEventArrayList(complaintArrayList);
                mRecyclerViewComplaintsAdapter.notifyDataSetChanged();
            }
        } else{
            showEmptyView();
        }
       /* VisitorOutFragment visitorOutFragment = new VisitorOutFragment();
        visitorOutFragment.makeCacheDirty(true);*/
    }
}
