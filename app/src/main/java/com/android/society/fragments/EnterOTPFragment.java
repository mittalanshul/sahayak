/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.fragments;


import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Resident;
import com.android.society.Model.Token;
import com.android.society.Model.User;
import com.android.society.R;
import com.android.society.activity.BaseActivity;
import com.android.society.callbacks.SmsListener;
import com.android.society.parser.AuthorityLoginParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.parser.OtpSendParser;
import com.android.society.parser.ResidentDataParser;
import com.android.society.receiver.SMSReceiver;
import com.android.society.utils.Dao;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EnterOTPFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EnterOTPFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView mTextOTPLabel;
    private TextView mTextResendOTP;
    private TextView mTextTimerOTP;
    private LinearLayout mTextSubmit;
    private int mOtpTimerLimit;
    private View mView;
    private EditText mFloatLabelEditTextOTP;
    private String mobile;
    private ImageView imgCross;


    public EnterOTPFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EnterOTPFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EnterOTPFragment newInstance(String param1, String param2) {
        EnterOTPFragment fragment = new EnterOTPFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mobile = getArguments().getString("mobile");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_otp, container, false);
        return mView;
    }

    private void triggerCallForOTP(){
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("phone",mobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();
        if(Utils.isInternetAvailable(getActivity())){
            ((BaseActivity)getActivity()).showProgressDialog("Please wait.. sending OTP",false);
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL + Constants.LOGIN_RESIDENT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            ((BaseActivity)getActivity()).removeProgressDialog();
                            Log.v("anshul","onResponse" + response);
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Token token = OtpSendParser.parserOTPData(response);
                                PreferenceManager.getInstance(getActivity().getApplicationContext()).
                                        writeToPrefs(PreferenceManager.TOKEN, token.getToken());
                                Dao.getInstance().setToken(token);
                                SMSReceiver.bindListener(new SmsListener() {
                                    @Override
                                    public void messageReceived(String messageText) {
                                        readOTP(messageText);
                                    }
                                });
                                disableResendOTP();
                                mOtpTimerLimit = 10;
                                launchOTPResendTImer();
                            }else{
                                getActivity().finish();
                            }
                            Utils.displayToast(getActivity(),baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((BaseActivity)getActivity()).removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

            };
            ((BaseActivity)getActivity()).queue.add(postRequest);
        }else{
            Utils.displayToast(getActivity().getApplicationContext(),"Please check your internet connection");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
       initOnClickListener();
        triggerCallForOTP();

    }

    private void initOnClickListener() {
        mTextSubmit.setOnClickListener(this);
        imgCross.setOnClickListener(this);
    }

    private void initViews(View paramView) {
        mTextSubmit = (LinearLayout) paramView.findViewById(R.id.linear_continue);
        mTextResendOTP = (TextView) paramView.findViewById(R.id.text_resend_otp);
        mTextTimerOTP = (TextView) paramView.findViewById(R.id.text_timer_otp);
        mFloatLabelEditTextOTP = (EditText) paramView.findViewById(R.id.otp);
        imgCross = (ImageView) paramView.findViewById(R.id.img_cross);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.linear_continue:
               if(isOTPValid()){
                    triggerCallToConfirmOTP();
               }
                break;

            case R.id.img_cross:
                mFloatLabelEditTextOTP.setText("");
                Utils.hideKeyboard(getActivity());
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
                break;
        }
    }

    private void triggerCallToConfirmOTP(){
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("otp",mFloatLabelEditTextOTP.getText().toString().trim());
            jsonBody.put("app_id",PreferenceManager.getInstance(getActivity().getApplicationContext())
                    .getString(PreferenceManager.DEVICE_USER_TOKEN));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();
        if(Utils.isInternetAvailable(getActivity())){
            ((BaseActivity)getActivity()).showProgressDialog("Logging in",false);
            String url = Constants.BASE_URL + Constants.VERIFY_RESIDENT ;
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            mOtpTimerLimit =0;
                            ((BaseActivity)getActivity()).removeProgressDialog();
                            Log.v("anshul","resident on response data is " + response);
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Resident resident = ResidentDataParser.residentParser(response,getActivity().getApplicationContext());
                                Utils.saveResidentData(getActivity().getApplicationContext(),resident,Constants.MODE_RESIDENT);
                                NavigationUtils.navigateToSocietyFacilities(getActivity());
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                            Utils.displayToast(getActivity(),baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((BaseActivity)getActivity()).removeProgressDialog();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getActivity().getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

            };
            ((BaseActivity)getActivity()).queue.add(postRequest);
        }else{
            Utils.displayToast(getActivity().getApplicationContext(),"Please check your internet connection");
        }
    }

    private void updateTime(String secondsLeft) {
        if (mTextTimerOTP != null) {
            if(getActivity() != null){
                SpannableStringBuilder brandName = new SpannableStringBuilder(getActivity().getString(R.string.dont_recieve_otp));
                brandName.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.black)), 0, brandName.length(), 0);

                SpannableString itemName = new SpannableString(secondsLeft);
                itemName.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.colorPrimaryDark)), 0, itemName.length(), 0);

                brandName.append(" ").append(itemName);
                mTextTimerOTP.setText(brandName);
            }
        }
    }

    private void launchOTPResendTImer() {
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateTime(String.valueOf(--mOtpTimerLimit + " seconds"));
                                    if (mOtpTimerLimit == 0) {
                                        enableResendOTP();
                                        interrupt();
                                    }
                                }
                            });
                            Thread.sleep(1000);
                        }
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();
    }

    private void enableResendOTP() {
        mView.findViewById(R.id.linear_resend_block).setVisibility(View.VISIBLE);
        mView.findViewById(R.id.linear_timer_block).setVisibility(View.GONE);
    }

    private void checkForAutoReadPermission() {

            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale
                        (getActivity(), Manifest.permission.READ_SMS)) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content),
                            "Please Grant Permissions",
                            Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    requestPermissions(
                                            new String[]{Manifest.permission
                                                    .READ_SMS},
                                            Constants.REQUEST_PERMISSIONS);
                                }
                            }).show();
                } else {
                    requestPermissions(
                            new String[]{Manifest.permission
                                    .READ_SMS},
                            Constants.REQUEST_PERMISSIONS);
                }
            } else {
                //Call whatever you want
                SMSReceiver.bindListener(new SmsListener() {
                    @Override
                    public void messageReceived(String messageText) {
                        readOTP(messageText);
                    }
                });
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SMSReceiver.bindListener(new SmsListener() {
                        @Override
                        public void messageReceived(String messageText) {
                            readOTP(messageText);
                        }
                    });
                }
                return;
            }
        }
    }

    private void readOTP(String message) {
        Pattern pattern = Pattern.compile("[0-9]{1,6}");
        Matcher matcher = pattern.matcher(message);
        String otp = "";
        while (matcher.find()) {
            otp = matcher.group();
        }
        if (!TextUtils.isEmpty(otp)) {
            mFloatLabelEditTextOTP.setText(otp);
            triggerCallToConfirmOTP();
        }
    }

    /**
     * This method is used to check if OTP eneterd is valid or not
     *
     * @return
     */
    private boolean isOTPValid() {
        boolean isOTPValid = true;
        if (TextUtils.isEmpty(mFloatLabelEditTextOTP.getText().toString().trim())) {
            Utils.displayToast(getActivity(), "Please enter OTP");
            isOTPValid = false;
        }
        return isOTPValid;
    }

    private void disableResendOTP() {
        mView.findViewById(R.id.linear_resend_block).setVisibility(View.GONE);
        mView.findViewById(R.id.linear_timer_block).setVisibility(View.VISIBLE);
    }
}
