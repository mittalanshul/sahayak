package com.android.society.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.society.Constants.Constants;
import com.android.society.Model.Resident;
import com.android.society.R;
import com.android.society.activity.ResidentProfileActivity;
import com.android.society.callbacks.ResidentProfileCallbacks;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class ResidentProfileFragment extends Fragment implements  View.OnClickListener , CheckBox.OnCheckedChangeListener{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;


    private EditText residentName;
    private EditText residentNumber;
    private boolean isJointOwner;
    private boolean isRentedOut;
    private EditText joinOwnerName;
    private EditText jointOwnerNumber;
    private EditText tenantName;
    private EditText tenantNumber;
    private CheckBox mCheckJoint;
    private CheckBox mCheckTenant;
    private Button mCreateResident;
    private LinearLayout mLinearJoint;
    private LinearLayout mLinearTenant;

    private Resident residentData;

    private View mView;

    public ResidentProfileCallbacks getmResidentProfileCallbacks() {
        return mResidentProfileCallbacks;
    }

    public void setmResidentProfileCallbacks(ResidentProfileCallbacks mResidentProfileCallbacks) {
        this.mResidentProfileCallbacks = mResidentProfileCallbacks;
    }

    private ResidentProfileCallbacks mResidentProfileCallbacks;


    public ResidentProfileFragment() {
        // Required empty public constructor
    }

    public static ResidentProfileFragment newInstance(String param1, String param2) {
        ResidentProfileFragment fragment = new ResidentProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            residentData = (Resident) getArguments().getSerializable("resident");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_resident_profile, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(mView);
        setOnClickListener();
        if (PreferenceManager.getInstance(getActivity()).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RWA)) {
          makeFieldsNonEditable();
        }else{
            residentName.setEnabled(false);
            residentNumber.setEnabled(false);
        }
        if(residentData != null){
            setResidentData(residentData);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void initViews(View paramView) {
        residentName = (EditText) paramView.findViewById(R.id.text_resident_name);
        residentNumber = (EditText) paramView.findViewById(R.id.text_resident_number);
        joinOwnerName = (EditText) paramView.findViewById(R.id.joint_owner_name);
        jointOwnerNumber = (EditText) paramView.findViewById(R.id.joint_owner_number);
        mCheckJoint = (CheckBox) paramView.findViewById(R.id.check_joint_owner);
        mCreateResident = (Button) paramView.findViewById(R.id.create_profile);
        mLinearJoint = (LinearLayout) paramView.findViewById(R.id.linear_joint_owner);
    }

    private void setOnClickListener() {
        mCreateResident.setOnClickListener(this);
        mCheckJoint.setOnCheckedChangeListener(this);
    }

     private void makeFieldsNonEditable() {
        residentName.setEnabled(false);
        residentNumber.setEnabled(false);
        joinOwnerName.setEnabled(false);
        jointOwnerNumber.setEnabled(false);
        mCheckJoint.setEnabled(false);
        mCheckTenant.setEnabled(false);
        mCreateResident.setVisibility(View.GONE);
        mLinearJoint.setEnabled(false);

    }

    private void setResidentData(Resident residentData){
        residentName.setText(residentData.getName());
        residentNumber.setText(residentData.getPhone1());
        if(residentData.isJointOwner()){
            mCheckJoint.setChecked(true);
            joinOwnerName.setText(residentData.getJointName());
            jointOwnerNumber.setText(residentData.getPhone2());
        }else{
            mCheckJoint.setChecked(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_profile:
                if (isDataValid()) {
                    if(mResidentProfileCallbacks != null){
                        try {
                            JSONObject jsonObject = new JSONObject();
                            if(isJointOwner){
                                jsonObject.put("is_joint_owner",true);
                                jsonObject.put("joint_name",joinOwnerName.getText().toString().trim());
                                jsonObject.put("phone2",jointOwnerNumber.getText().toString().trim());
                            }
                            mResidentProfileCallbacks.uploadResidentProfile(jsonObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
        switch (compoundButton.getId()) {
            case R.id.check_joint_owner:
                if (checked) {
                    mLinearJoint.setVisibility(View.VISIBLE);
                } else {
                    mLinearJoint.setVisibility(View.GONE);
                }
                break;
        }
    }


    public boolean isDataValid() {
        if(!mCheckJoint.isChecked()){
            Utils.displayToast(getActivity(), "No Informatio found to upload");
        }else if (mCheckJoint.isChecked() && TextUtils.isEmpty(joinOwnerName.getText().toString().trim())) {
            Utils.displayToast(getActivity(), "Please enter Joint owner name");
            return false;
        }
        return true;
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mResidentProfileCallbacks = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
