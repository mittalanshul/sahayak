/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.society.Constants.Constants;
import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.activity.BaseActivity;
import com.android.society.adapter.VisitorEntryListAdapter;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;
import com.android.society.dialog.VisitorInfoDialog;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisitorInFragment extends Fragment implements OnVisitorInteractionCallbacks{

    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<Visitor> visitors;
    private RealmResults<Visitor> tempVisitors;
    private VisitorEntryListAdapter visitorEntryListAdapter;
    private RealmChangeListener realmListener;


    public VisitorInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_in, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        visitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .equalTo("isExited",false)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }



    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        visitorEntryListAdapter = new VisitorEntryListAdapter(visitors,getActivity());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(visitorEntryListAdapter);
        visitorEntryListAdapter.setOnVisitorInteractionCallbacks(this);
    }

    @Override
    public void onVisitorExited(final Visitor visitor) {
        if(visitor != null){
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Visitor visitorToEdit = realm.where(Visitor.class)
                    .equalTo("visitorNumber", visitor.getVisitorNumber())
                    .equalTo("isExited",false)
                    .findFirst();
            visitorToEdit.setExited(true);
            visitorToEdit.setDateExited(Utils.getCurrentDate());
            visitorToEdit.setTimeExited(Utils.getCurrentTime());
            realm.commitTransaction();
            refreshVisitorsEntryData();
           makeVisitorExitEntryOnServer(visitorToEdit);

        }
    }
    private void makeVisitorExitEntryOnServer(Visitor visitor){
        if(Utils.isInternetAvailable(getActivity())){
            String url = Constants.BASE_URL + Constants.EXIT_VISITOR +"name="+visitor.getVisitorName()+"&phone="+visitor.getVisitorNumber()
                    +"&exit="+Utils.getCurrentDate()+ Utils.getCurrentTime();
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("anshul","exited created on server " +response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getActivity().getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

            };
            ((BaseActivity)getActivity()).queue.add(postRequest);
        }
    }

    public void loadDefaultData(){
        visitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .equalTo("isExited",false)
                .findAll();
        if(visitors != null && visitors.size() >0){
            if(visitorEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                visitorEntryListAdapter.setVisitorsList(visitors);
                visitorEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        VisitorOutFragment visitorOutFragment = new VisitorOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    public void refreshVisitorsDataOnQuery(String name){
        RealmResults<Visitor> queriedVisitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .contains("visitorName",name)
                .or().contains("visitorNumber",name)
                .or().contains("flatVisited",name)
                .or().contains("vehicleName",name)
                .equalTo("isExited",false)
                .findAllSorted("date", Sort.ASCENDING);
        if(queriedVisitors != null && queriedVisitors.size() >0){
            if(visitorEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                visitorEntryListAdapter.setVisitorsList(queriedVisitors);
                visitorEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        VisitorOutFragment visitorOutFragment = new VisitorOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    private void refreshVisitorsEntryData(){
        visitors = Realm.getDefaultInstance()
                .where(Visitor.class)
                .equalTo("isExited",false)
                .findAll();
        if(visitors != null && visitors.size() >0){
            setmRecyclerViewData();
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        VisitorOutFragment visitorOutFragment = new VisitorOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    @Override
    public void onVisitorClicked(Visitor visitor) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        VisitorInfoDialog visitorInfoDialog = new VisitorInfoDialog ();
        visitorInfoDialog.setVisitorData(visitor);
        visitorInfoDialog.setCancelable(true);
        visitorInfoDialog.show(fm, "Sample Fragment");
    }

    @Override
    public void onResidentClicked(ResidentIn residentIn) {

    }

    @Override
    public void onResidentCallMake(ResidentIn residentIn) {

    }
}
