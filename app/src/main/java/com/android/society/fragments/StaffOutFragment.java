/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.society.Model.Staff;
import com.android.society.R;
import com.android.society.adapter.StaffEntryListAdapter;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class StaffOutFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<Staff> staffs;
    private StaffEntryListAdapter staffEntryListAdapter;
    public static boolean isCacheDirty;


    public StaffOutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_out, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        isCacheDirty = false;
        staffs = Realm.getDefaultInstance()
                .where(Staff.class)
                .equalTo("isExited",true)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        staffEntryListAdapter = new StaffEntryListAdapter(staffs);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(staffEntryListAdapter);
    }

    public void makeCacheDirty(boolean val){
        isCacheDirty = val;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser && isCacheDirty){
            refreshStaffData();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void refreshStaffData(){
        staffs = Realm.getDefaultInstance()
                .where(Staff.class)
                .equalTo("isExited",true)
                .findAll();
        if(staffEntryListAdapter != null){
            staffEntryListAdapter.setStaffRealmResults(staffs);
            staffEntryListAdapter.notifyDataSetChanged();
            makeCacheDirty(false);
        }

    }

}

