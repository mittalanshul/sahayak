package com.android.society.service;

import android.util.Log;

import com.android.society.utils.PreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.logging.Logger;


/**
 * Created by anshul on 18/02/18.
 */

public class EdgeOverGCMInstanceIdService extends FirebaseInstanceIdService {
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.v("anshul","the gcm id is " + refreshedToken);
        PreferenceManager.getInstance(getApplicationContext()).writeToPrefs("gcm_token", refreshedToken);
    }
}
