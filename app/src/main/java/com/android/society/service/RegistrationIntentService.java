package com.android.society.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.society.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by anshul on 24/02/18.
 */

public class RegistrationIntentService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RegistrationIntentService(String name) {
        super(name);
    }

    public  RegistrationIntentService(){
        super(null);

    }

    // ...

    @Override
    public void onHandleIntent(Intent intent) {
        Log.v("anshul","gcm token onHandleIntent");
        // ...
        InstanceID instanceID = InstanceID.getInstance(this);
        try {
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.v("anshul","gcm token " + token);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
