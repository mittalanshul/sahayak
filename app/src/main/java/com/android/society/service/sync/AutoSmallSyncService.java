package com.android.society.service.sync;

import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.TaskParams;

/**
 * Created by anshul on 18/02/18.
 */

public class AutoSmallSyncService  extends GcmTaskService {
    public static final String sSmallSyncServiceTag = AutoSmallSyncService.class.getSimpleName();



    public static PeriodicTask getSmallSyncPeriodicTask() {
        return new PeriodicTask.Builder().setService(AutoSmallSyncService.class).setTag(AutoSmallSyncService.class.getName()).setRequiresCharging(false).setRequiredNetwork(0).setPeriod(60).setFlex(10).build();
    }

    public int onRunTask(TaskParams taskParams) {

        return 1;
    }
}
