package com.android.society.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.android.society.service.TestService;

/**
 * Created by anshul on 24/02/18.
 */

public class ConnectivityReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (isChangeToWifi(intent)) {
            Intent service = new Intent(context, TestService.class);
            context.startService(service);
        }
    }

    private boolean isChangeToWifi(Intent intent) {
        if (intent.getExtras() != null) {
            if (intent.getExtras().containsKey(ConnectivityManager.EXTRA_NETWORK_TYPE)) {
                int type = intent.getExtras().getInt(ConnectivityManager.EXTRA_NETWORK_TYPE);
                if (type == ConnectivityManager.TYPE_WIFI) {
                    return true;
                }
            }
        }
        return false;
    }
}

