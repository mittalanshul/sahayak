package com.android.society.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by anshul on 24/02/18.
 */

public class ScreenReceiver extends BroadcastReceiver {

    // THANKS JASON
    public static boolean wasScreenOn = true;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            // DO WHATEVER YOU NEED TO DO HERE
            wasScreenOn = false;
            //Jabong.CURRENT_TIME_ON_STOP_EVENT = System.currentTimeMillis();
            //Utils.setCurrentTimeOnStopEvent(context, Jabong.CURRENT_TIME_ON_STOP_EVENT);

        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            // AND DO WHATEVER YOU NEED TO DO HERE
            //   wasScreenOn = true;
        }
    }

}
