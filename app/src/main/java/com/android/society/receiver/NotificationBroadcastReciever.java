package com.android.society.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.Utils;

import java.util.logging.Logger;

/**
 * Created by anshul on 24/12/17.
 */

public class NotificationBroadcastReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("anshul","NotificationBroadcastReciever called");
        if (intent.getAction().equals(Constants.INTENT_ACTION_BROADCAST)) {
            String type = intent.getStringExtra(Constants.NOTIFICATION_TYPE);
                if(type.equalsIgnoreCase(Constants.AD_ALERT)){
                    Log.v("anshul","call panic onReceive");
                    NavigationUtils.navigateToPanicAlert(context,
                            intent.getStringExtra(Constants.NOTIFICATION_DATA) ,
                            intent.getStringExtra(Constants.NOTIFICATION_MESSAGE));
                }else if(type.equalsIgnoreCase(Constants.AD_CHILD_SAFETY)){
                    NavigationUtils.navigateToChildSafety(context , intent.getStringExtra(Constants.NOTIFICATION_DATA));
                }else if(type.equalsIgnoreCase(Constants.AD_CHILD_ACTION)){
                    NavigationUtils.navigateToChildSafetyAction(context , intent.getStringExtra(Constants.NOTIFICATION_DATA));
                }else if(type.equalsIgnoreCase(Constants.AD_GUARD_VISITOR_ACTION)){
                    showAlertDialog(context , intent.getStringExtra(Constants.NOTIFICATION_DATA));
                } else if(type.equalsIgnoreCase(Constants.AD_VISITOR) || type.equalsIgnoreCase(Constants.AD_VISITOR_ACTION)){
                    NavigationUtils.navigateToVisitorDetailForm(context , intent.getStringExtra(Constants.NOTIFICATION_DATA),type);
                }
        }
    }

    private void showAlertDialog(final Context context , String data){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("AlertDialog Title");
        alertDialogBuilder
                .setMessage("Some Alert Dialog message.")
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(context, "OK button click ", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(context, "CANCEL button click ", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
