package com.android.society.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.society.service.TestService;

/**
 * Created by anshul on 18/02/18.
 */

public class RestartServiceReceiver extends BroadcastReceiver
{

    private static final String TAG = "anshul";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive");
        context.startService(new Intent(context, TestService.class));

    }

}
