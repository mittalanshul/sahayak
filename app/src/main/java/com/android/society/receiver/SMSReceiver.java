package com.android.society.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.android.society.callbacks.SmsListener;

/**
 * Created by 201101101 on 10/22/2017.
 */

public class SMSReceiver extends BroadcastReceiver {

    private static SmsListener mListener;
    public SMSReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();
        if(data != null){
            Object[] pdus = (Object[]) data.get("pdus");
            if(pdus != null && pdus.length>0){
                for (int i = 0; i < pdus.length; i++) {
                    SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

                    String sender = smsMessage.getDisplayOriginatingAddress();
                    //You must check here if the sender is your provider and not another one with same text.

                    String messageBody = smsMessage.getMessageBody();

                    //Pass on the text to our listener.
                    if(mListener != null)
                        mListener.messageReceived(messageBody);
                }
            }
        }


    }
    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }

}