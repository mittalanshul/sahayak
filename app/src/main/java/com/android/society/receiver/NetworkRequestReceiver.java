package com.android.society.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.society.service.NetworkService;

/**
 * Created by anshul on 18/02/18.
 */

public class NetworkRequestReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Bundle extra = intent.getExtras();
        if (extra != null && !extra.isEmpty()) {
            Intent fetchGateKeeperDataService = new Intent(context, NetworkService.class);
            fetchGateKeeperDataService.putExtras(extra);
            context.startService(fetchGateKeeperDataService);
        }
    }
}
