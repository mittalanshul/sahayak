package com.android.society.receiver;

/**
 * Created by anshul on 22/12/17.
 */

public interface SensorInteractionListener {
    void updateUIOnActivity();

    void updateLastUpdate(long lastUpdate);
}
