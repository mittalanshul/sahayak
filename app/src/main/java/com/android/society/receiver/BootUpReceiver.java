package com.android.society.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.android.society.activity.SplashActivity;
import com.android.society.service.TestService;
import com.android.society.service.sync.AutoSmallSyncService;
import com.android.society.utils.Utils;
import com.google.android.gms.gcm.GcmNetworkManager;

/**
 * Created by anshul on 18/02/18.
 */

public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("anshul","hello boot");
        TestService mSensorService = new TestService(context);
        Intent mServiceIntent = new Intent(context, mSensorService.getClass());
            context.startService(mServiceIntent);
    }
}
