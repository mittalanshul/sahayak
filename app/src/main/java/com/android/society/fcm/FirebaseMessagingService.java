/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.Notification;
import com.android.society.R;
import com.android.society.activity.ChildExitActionActivity;
import com.android.society.activity.ChildSafetyActivity;
import com.android.society.activity.ComplaintDetailsActivity;
import com.android.society.activity.MaintainenceDuesActivity;
import com.android.society.activity.PanicAlertActivity;
import com.android.society.activity.ResidentVisitorHistory;
import com.android.society.activity.SocietyFacilitiesActivity;
import com.android.society.activity.VisitorDetailForm;
import com.android.society.parser.NotificationParser;
import com.android.society.utils.NavigationUtils;
import com.google.firebase.messaging.RemoteMessage;

import static android.content.ContentValues.TAG;

/**
 * Created by 201101101 on 10/17/2017.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private String message;
    private Notification notification;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.v("anshul","onMessageReceived : called" + remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getData());
        for(String key : remoteMessage.getData().keySet()){
            String value = remoteMessage.getData().get(key);
            Log.d(TAG, "parameters are  " + key + "-----" + value);
            Log.v("anshul","parameters are  " + key + "-----" + value);
        }

        Log.v("anshul","parameters are  " +remoteMessage.getMessageType());
        Log.v("anshul","parameters are  " + remoteMessage.getMessageId());

        //Calling method to generate notification
       sendNotification(remoteMessage);
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(RemoteMessage remoteMessage) {
        Intent intent = getIntent(remoteMessage);
        if(intent != null){
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if(notification.getType().equalsIgnoreCase(Constants.AD_ALERT)){
                defaultSoundUri = Uri.parse("android.resource://com.android.society/" + R.raw.warning);
            }else if(notification.getType().equalsIgnoreCase(Constants.AD_VISITOR)){
                defaultSoundUri = Uri.parse("android.resource://com.android.society/" + R.raw.doorbell);
            }
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.app_icon)
                    .setContentTitle(remoteMessage.getData().get("title"))
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                    .setLights(Color.parseColor("#17aa86"),3000,3000)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
        }

    }

    private Intent getIntent(RemoteMessage remoteMessage){
        Intent intent = null;
        notification = NotificationParser.parserNotification(remoteMessage.getData().get("body"));
        Log.v("anshul","type of notification is " + notification.getType());
        if(notification != null){
            message = notification.getMessage();
            if(notification.getType().equalsIgnoreCase(Constants.AD_COMPLAINT)){
                intent = new Intent(this, ComplaintDetailsActivity.class);
                startActivity(intent);
            }else if(notification.getType().equalsIgnoreCase(Constants.AD_ALERT)){
                Log.v("anshul","call panic getIntent");
                intent = new Intent(this, PanicAlertActivity.class);
                intent.putExtra(Constants.NOTIFICATION_MESSAGE, notification.getMessage());
                intent.putExtra(Constants.NOTIFICATION_TYPE, notification.getType());
                intent.putExtra(Constants.NOTIFICATION_DATA, notification.getData());
                startActivity(intent);
            }else if(notification.getType().equalsIgnoreCase(Constants.AD_CHILD_SAFETY)){
                intent = new Intent(this, ChildSafetyActivity.class);
                intent.putExtra(Constants.NOTIFICATION_MESSAGE, notification.getMessage());
                intent.putExtra(Constants.NOTIFICATION_TYPE, notification.getType());
                intent.putExtra(Constants.NOTIFICATION_DATA, notification.getData());
                startActivity(intent);
            } else if(notification.getType().equalsIgnoreCase(Constants.AD_VISITOR) || notification.getType().equalsIgnoreCase(Constants.AD_VISITOR_ACTION)){
                intent = new Intent(this, VisitorDetailForm.class);
                intent.putExtra(Constants.NOTIFICATION_MESSAGE, notification.getMessage());
                intent.putExtra(Constants.NOTIFICATION_TYPE, notification.getType());
                intent.putExtra(Constants.NOTIFICATION_DATA, notification.getData());
                startActivity(intent);
            }else if(notification.getType().equalsIgnoreCase(Constants.AD_MAINTAINENCE)){
                intent = new Intent(this, MaintainenceDuesActivity.class);
                startActivity(intent);
            } else if(notification.getType().equalsIgnoreCase(Constants.AD_CHILD_ACTION)){
                NavigationUtils.navigateToChildSafetyAction(this, intent.getStringExtra(Constants.NOTIFICATION_DATA));
            }else if(notification.getType().equalsIgnoreCase(Constants.AD_CHILD_ACTION)){
                sendBroadCast(notification);
            }else if(notification.getType().equalsIgnoreCase(Constants.AD_GUARD_VISITOR_ACTION)){
                showAlertDialog(this,"");
            }
        }
        return intent;
    }

    private void sendBroadCast(Notification notification){
        Log.v("anshul ","sendBroadCast " + notification.getData());
        Intent broadcast = new Intent(Constants.INTENT_ACTION_BROADCAST);
        broadcast.putExtra(Constants.NOTIFICATION_MESSAGE, notification.getMessage());
        broadcast.putExtra(Constants.NOTIFICATION_TYPE, notification.getType());
        broadcast.putExtra(Constants.NOTIFICATION_DATA, notification.getData());
        sendBroadcast(broadcast);
    }

    private void showAlertDialog(final Context context , String data){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("AlertDialog Title");
        alertDialogBuilder
                .setMessage("Some Alert Dialog message.")
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(context, "OK button click ", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(context, "CANCEL button click ", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
