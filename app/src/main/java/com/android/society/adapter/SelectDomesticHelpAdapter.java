package com.android.society.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.utils.PreferenceManager;
import com.android.society.widget.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by anshul on 14/02/18.
 */

public class SelectDomesticHelpAdapter extends RecyclerView.Adapter<SelectDomesticHelpAdapter.MyViewHolder> {


    private ArrayList<StaffDetails> staffDetailsArrayList;
    private Context mContext;
    private StaffDetails selectedStaff;

    public ArrayList<StaffDetails> getStaffDetailsArrayList() {
        return staffDetailsArrayList;
    }

    public void setStaffDetailsArrayList(ArrayList<StaffDetails> staffDetailsArrayList) {
        this.staffDetailsArrayList = staffDetailsArrayList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView label;
        public TextView name;
        public TextView number;
        public CircularImageView circularImageView;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.rel_domestic);
            label = (TextView) view.findViewById(R.id.label_domestic);
            name = (TextView) view.findViewById(R.id.text_staff_name);
            number = (TextView) view.findViewById(R.id.text_staff_number);
            circularImageView = (CircularImageView) view.findViewById(R.id.domestic_pic);
            cardView = view.findViewById(R.id.card_details);
        }
    }

    public SelectDomesticHelpAdapter(Context context) {
        this.mContext = context;
    }

    public SelectDomesticHelpAdapter(ArrayList<StaffDetails> staffDetailsArrayList, Context paramContext) {
        this(paramContext);
        this.staffDetailsArrayList = staffDetailsArrayList;
    }

    @Override
    public SelectDomesticHelpAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_domestic_service, parent, false);

        return new SelectDomesticHelpAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final SelectDomesticHelpAdapter.MyViewHolder holder, int position) {
        final StaffDetails staffDetails = staffDetailsArrayList.get(position);
        holder.label.setText(staffDetails.getStaffType());
        holder.name.setText(staffDetails.getStaffName());
        holder.number.setText(staffDetails.getStaffNumber());
        holder.relativeLayout.setTag(staffDetails);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (staffDetails.isSelected()) {
                    staffDetails.setSelected(false);
                    selectedStaff = null;
                    holder.cardView.setForeground(null);
                } else {
                    if (selectedStaff != null) {
                        selectedStaff.setSelected(false);
                    }
                    staffDetails.setSelected(true);
                    selectedStaff = staffDetails;
                    holder.cardView.setForeground(mContext.getResources().getDrawable(R.color.colorPrimaryLite));
                }
            }
        });

        if (staffDetails.isSelected()) {
            holder.cardView.setForeground(mContext.getResources().getDrawable(R.color.colorPrimaryLite));
        } else {
            holder.cardView.setForeground(null);
        }

        String url = Constants.MEDIA_BASE_URL + staffDetails.getImage();
        holder.circularImageView.setImageDrawable(null);
        Picasso.with(mContext).load(url).placeholder(R.drawable.user_pic_def).error(R.drawable.user_pic_def)
                .into(holder.circularImageView);
    }

    public StaffDetails getSelectedStaff() {
        return selectedStaff;
    }

    public void setSelectedStaff(StaffDetails selectedStaff) {
        this.selectedStaff = selectedStaff;
    }

    @Override
    public int getItemCount() {
        return staffDetailsArrayList == null ? 0 : staffDetailsArrayList.size();
    }
}
