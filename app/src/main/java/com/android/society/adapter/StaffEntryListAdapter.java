/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Model.Staff;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.callbacks.OnStaffInteractionCallback;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;

import io.realm.RealmResults;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class StaffEntryListAdapter extends RecyclerView.Adapter<StaffEntryListAdapter.MyViewHolder> {

    private RealmResults<Staff> staffRealmResults;
    private OnStaffInteractionCallback onStaffInteractionCallback;

    public RealmResults<Staff> getStaffRealmResults() {
        return staffRealmResults;
    }

    public void setStaffRealmResults(RealmResults<Staff> staffRealmResults) {
        this.staffRealmResults = staffRealmResults;
    }

    public OnStaffInteractionCallback getOnStaffInteractionCallback() {
        return onStaffInteractionCallback;
    }

    public void setOnStaffInteractionCallback(OnStaffInteractionCallback onStaffInteractionCallback) {
        this.onStaffInteractionCallback = onStaffInteractionCallback;
    }

    public StaffEntryListAdapter(RealmResults<Staff> paramStaffs){
        staffRealmResults = paramStaffs;
    }

    @Override
    public StaffEntryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.staff_list_item, parent, false);
        return new StaffEntryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StaffEntryListAdapter.MyViewHolder holder, int position) {
        final Staff staff = staffRealmResults.get(position);
        holder.staffName.setText(staff.getStaffName());
        holder.staffNumber.setText(staff.getStaffNumber());
        holder.staffType.setText(staff.getStaffType());
        holder.relativeLayout.setTag(staff);
        holder.staffExit.setTag(staff);
        if(staff.isExited()){
            holder.staffExit.setVisibility(View.GONE);
        }else{
            holder.staffExit.setVisibility(View.VISIBLE);
        }
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Staff staffClicked = (Staff) view.getTag();
                if(onStaffInteractionCallback != null){
                    onStaffInteractionCallback.onStaffClicked(staffClicked);
                }
            }
        });
        holder.staffExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Staff staffClicked = (Staff) view.getTag();
                if(onStaffInteractionCallback != null){
                    onStaffInteractionCallback.onStaffExited(staffClicked);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return staffRealmResults.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView staffName;
        public TextView staffNumber;
        public TextView staffType;
        public TextView staffExit;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_visitor);
            staffName = (TextView) view.findViewById(R.id.text_staff_name);
            staffNumber = (TextView) view.findViewById(R.id.text_staff_number);
            staffType = (TextView) view.findViewById(R.id.text_type);
            staffExit = (TextView) view.findViewById(R.id.text_exit_staff);
        }
    }

}

