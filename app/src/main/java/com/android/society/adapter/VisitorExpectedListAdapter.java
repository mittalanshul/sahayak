package com.android.society.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;
import com.android.society.utils.PreferenceManager;

import java.util.ArrayList;

import io.realm.RealmResults;

/**
 * Created by anshul on 04/01/18.
 */

public class VisitorExpectedListAdapter extends RecyclerView.Adapter<VisitorExpectedListAdapter.MyViewHolder> {
    public ArrayList<Visitor> getVisitorsList() {
        return visitorsList;
    }

    public void setVisitorsList(ArrayList<Visitor> visitorsList) {
        this.visitorsList = visitorsList;
    }

    private ArrayList<Visitor> visitorsList;
    private OnVisitorInteractionCallbacks onVisitorInteractionCallbacks;
    private Context mContext;


    public OnVisitorInteractionCallbacks getOnVisitorInteractionCallbacks() {
        return onVisitorInteractionCallbacks;
    }

    public void setOnVisitorInteractionCallbacks(OnVisitorInteractionCallbacks onVisitorInteractionCallbacks) {
        this.onVisitorInteractionCallbacks = onVisitorInteractionCallbacks;
    }



    public VisitorExpectedListAdapter(ArrayList<Visitor> paramVisitors ,Context paramContext){
        visitorsList = paramVisitors;
        mContext = paramContext ;
    }

    @Override
    public VisitorExpectedListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.visitor_list_item, parent, false);
        return new VisitorExpectedListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VisitorExpectedListAdapter.MyViewHolder holder, int position) {
        final Visitor visitor = visitorsList.get(position);
        holder.visitorName.setText(visitor.getVisitorName());
        holder.visitorNumber.setText(visitor.getVisitorNumber());
        holder.visitorVehicle.setText(visitor.getVehicleName());
        holder.visitorFlat.setText(visitor.getFlatVisited());
        holder.visitorPurpose.setText(visitor.getPurpose());
        holder.relativeLayout.setTag(visitor);
        holder.visitorExit.setTag(visitor);
        holder.imgDelete.setTag(visitor);

        holder.visitorExit.setVisibility(View.GONE);
        holder.visitorVehicle.setVisibility(View.GONE);
        holder.visitorPurpose.setVisibility(View.GONE);
        holder.visitorVehicleLabel.setVisibility(View.GONE);
        holder.visitorPurposeLabel.setVisibility(View.GONE);
        holder.visitorFlatLabel.setVisibility(View.GONE);
        holder.visitorFlat.setVisibility(View.GONE);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Visitor visitor = (Visitor) view.getTag();
                if(onVisitorInteractionCallbacks != null){
                    onVisitorInteractionCallbacks.onVisitorClicked(visitor);
                }
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Visitor visitor = (Visitor) view.getTag();
                if(onVisitorInteractionCallbacks != null){
                    onVisitorInteractionCallbacks.onVisitorExited(visitor);
                }
            }
        });

        if(PreferenceManager.getInstance(mContext).getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_GUARD)){
            holder.imgDelete.setVisibility(View.GONE);
        }else{
            holder.imgDelete.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return visitorsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout relativeLayout;
        public TextView visitorName;
        public TextView visitorNumber;
        public TextView visitorVehicle;
        public TextView visitorFlat;
        public TextView visitorPurpose;
        public TextView visitorExit;
        public TextView visitorVehicleLabel;
        public TextView visitorPurposeLabel;
        public TextView visitorFlatLabel;
        public ImageView imgDelete;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_visitor);
            visitorName = (TextView) view.findViewById(R.id.text_visitor_name);
            visitorNumber = (TextView) view.findViewById(R.id.text_visitor_number);
            visitorVehicle = (TextView) view.findViewById(R.id.text_vehicle_name);
            visitorFlat = (TextView) view.findViewById(R.id.text_flat);
            visitorPurpose = (TextView) view.findViewById(R.id.text_purpose);
            visitorExit = (TextView) view.findViewById(R.id.text_exit_visitor);
            visitorVehicleLabel = (TextView) view.findViewById(R.id.text_vehicle_label);
            visitorPurposeLabel = (TextView) view.findViewById(R.id.text_purpose_label);
            visitorFlatLabel = (TextView) view.findViewById(R.id.text_flat_label);
            imgDelete = (ImageView)view.findViewById(R.id.img_deleter);
        }
    }

}

