/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Complaint;
import com.android.society.Model.Event;
import com.android.society.R;
import com.android.society.activity.ComplaintDetailActivity;
import com.android.society.callbacks.OnComplaintAdded;
import com.android.society.callbacks.onEventClicked;
import com.android.society.utils.PreferenceManager;

import java.util.ArrayList;

/**
 * Created by 201101101 on 10/18/2017.
 */

public class RecyclerComplaintsAdapter extends RecyclerView.Adapter<RecyclerComplaintsAdapter.MyViewHolder> {

    public ArrayList<Complaint> getEventArrayList() {
        return eventArrayList;
    }

    public void setEventArrayList(ArrayList<Complaint> eventArrayList) {
        this.eventArrayList = eventArrayList;
    }

    private ArrayList<Complaint> eventArrayList;
    private Context mContext;

    public OnComplaintAdded getOnComplaintAddedCall() {
        return onComplaintAddedCall;
    }

    public void setOnComplaintAddedCall(OnComplaintAdded onComplaintAddedCall) {
        this.onComplaintAddedCall = onComplaintAddedCall;
    }

    private OnComplaintAdded onComplaintAddedCall;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView title;
        public TextView flat;
        public TextView description;
        public ImageView imgDelete;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_event);
            title = (TextView) view.findViewById(R.id.text_title);
            flat = (TextView) view.findViewById(R.id.text_flat);
            description = (TextView) view.findViewById(R.id.text_desc);
            imgDelete = (ImageView)view.findViewById(R.id.delete_complaint);
        }
    }


    public RecyclerComplaintsAdapter(ArrayList<Complaint> eventArrayList, Context paramContext) {
        this.eventArrayList = eventArrayList;
        mContext = paramContext;
    }

    @Override
    public RecyclerComplaintsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_show_complaint, parent, false);

        return new RecyclerComplaintsAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(RecyclerComplaintsAdapter.MyViewHolder holder, int position) {
        final Complaint event = eventArrayList.get(position);
        holder.title.setText(event.getResident());
        holder.flat.setText(event.getSubject());
        holder.description.setText(event.getType());
        holder.relativeLayout.setTag(event);
        holder.imgDelete.setTag(event);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Complaint complaint = (Complaint) view.getTag();
                if(onComplaintAddedCall != null){
                    onComplaintAddedCall.omComplaintClicked(complaint);
                }
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Complaint event = (Complaint) view.getTag();
                if(onComplaintAddedCall != null){
                    onComplaintAddedCall.omComplaintDelete(event);
                }
            }
        });

        if(PreferenceManager.getInstance(mContext).getString(PreferenceManager.IS_LOGGED_IN_MODE)
                .equalsIgnoreCase(Constants.MODE_RWA)){
          holder.imgDelete.setVisibility(View.GONE);
        }else{
            holder.imgDelete.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return eventArrayList.size();
    }
}

