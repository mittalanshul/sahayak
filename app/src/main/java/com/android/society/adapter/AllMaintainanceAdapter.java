package com.android.society.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Model.MaintainenceDues;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.callbacks.MaintainanceCallbacks;
import com.android.society.widget.CircularImageView;
import java.util.ArrayList;

/**
 * Created by anshul on 08/03/18.
 */

public class AllMaintainanceAdapter extends RecyclerView.Adapter<AllMaintainanceAdapter.MyViewHolder> {


    public ArrayList<MaintainenceDues> getMaintainenceDuesArrayList() {
        return maintainenceDuesArrayList;
    }

    public void setMaintainenceDuesArrayList(ArrayList<MaintainenceDues> maintainenceDuesArrayList) {
        this.maintainenceDuesArrayList = maintainenceDuesArrayList;
    }

    private ArrayList<MaintainenceDues> maintainenceDuesArrayList;
    private Context mContext;

    public MaintainanceCallbacks getMaintainanceCallbacks() {
        return maintainanceCallbacks;
    }

    public void setMaintainanceCallbacks(MaintainanceCallbacks maintainanceCallbacks) {
        this.maintainanceCallbacks = maintainanceCallbacks;
    }

    public MaintainanceCallbacks maintainanceCallbacks;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView label;
        public TextView name;
        public TextView number;
        public TextView mainDetails;
        public CircularImageView circularImageView;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_main);
            label = (TextView) view.findViewById(R.id.label_main);
            name = (TextView) view.findViewById(R.id.text_total_label);
            mainDetails = (TextView) view.findViewById(R.id.main_details);
        }
    }


    public AllMaintainanceAdapter(ArrayList<MaintainenceDues> maintainenceDuesArrayList, Context paramContext) {
        this.maintainenceDuesArrayList = maintainenceDuesArrayList;
        mContext = paramContext;
    }

    @Override
    public AllMaintainanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_maintainance, parent, false);

        return new AllMaintainanceAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(AllMaintainanceAdapter.MyViewHolder holder, int position) {
        final MaintainenceDues maintainenceDues = maintainenceDuesArrayList.get(position);
        holder.label.setText(maintainenceDues.getStatus());
        holder.name.setText("Total Rs. " + maintainenceDues.getTotal());
        holder.relativeLayout.setTag(maintainenceDues.getId());
        /*holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = (String)v.getTag();
                if(maintainanceCallbacks != null){
                    maintainanceCallbacks.onMaintainanceClicked(id);
                }
            }
        });*/
        holder.mainDetails.setTag(maintainenceDues);
        holder.mainDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaintainenceDues maintainenceDues1 = (MaintainenceDues) v.getTag();
                if(maintainanceCallbacks != null){
                    maintainanceCallbacks.onMaintainanceClicked(maintainenceDues1);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return maintainenceDuesArrayList.size();
    }
}
