package com.android.society.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Contact;
import com.android.society.Model.Event;
import com.android.society.R;
import com.android.society.callbacks.onEventClicked;
import com.android.society.utils.PreferenceManager;

import java.util.ArrayList;

/**
 * Created by anshul on 24/12/17.
 */

public class EmergencyContactsRecyclerView extends RecyclerView.Adapter<EmergencyContactsRecyclerView.MyViewHolder> {

    private ArrayList<Contact> contactArrayList;
    private Context mContext;

    public onEventClicked getOnEventClickedCall() {
        return onEventClickedCall;
    }

    public void setOnEventClickedCall(onEventClicked onEventClickedCall) {
        this.onEventClickedCall = onEventClickedCall;
    }

    private onEventClicked onEventClickedCall;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView title;
        public TextView description;
        public ImageView imgDelete;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_event);
            title = (TextView) view.findViewById(R.id.text_title);
            description = (TextView) view.findViewById(R.id.text_desc);
            imgDelete = (ImageView)view.findViewById(R.id.delete_contact);
        }
    }


    public EmergencyContactsRecyclerView(ArrayList<Contact> contactArrayList, Context paramContext) {
        this.contactArrayList = contactArrayList;
        mContext = paramContext;
    }

    @Override
    public EmergencyContactsRecyclerView.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.emergency_contacts, parent, false);

        return new EmergencyContactsRecyclerView.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(EmergencyContactsRecyclerView.MyViewHolder holder, int position) {
        final Contact contact = contactArrayList.get(position);
        holder.title.setText(contact.getName());
        holder.description.setText(contact.getNumber());
        holder.relativeLayout.setTag(contact);
        holder.imgDelete.setTag(contact);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Contact contact1 = (Contact) view.getTag();

                String emergencyNumbers = PreferenceManager.getInstance(mContext).getString(PreferenceManager.EMERGENCY_NUMBERS
                        ,"");
                String emergencyNames = PreferenceManager.getInstance(mContext).getString(PreferenceManager.EMERGENCY_NAMES
                        ,"");
                if(!TextUtils.isEmpty(emergencyNumbers) && !TextUtils.isEmpty(emergencyNames)){
                    if(emergencyNumbers.contains(",") && emergencyNames.contains(",")){
                        emergencyNumbers = emergencyNumbers.replace(contact1.getNumber(),"");
                        emergencyNumbers = emergencyNumbers.replace(",","");
                        emergencyNumbers = emergencyNumbers.trim();
                        PreferenceManager.getInstance(mContext).writeToPrefs(PreferenceManager.EMERGENCY_NUMBERS
                                ,emergencyNumbers);

                        emergencyNames = emergencyNames.replace(contact1.getName(),"");
                        emergencyNames = emergencyNames.replace(",","");
                        emergencyNames = emergencyNames.trim();
                        PreferenceManager.getInstance(mContext).writeToPrefs(PreferenceManager.EMERGENCY_NAMES
                                ,emergencyNames);
                    } else{
                        PreferenceManager.getInstance(mContext).writeToPrefs(PreferenceManager.EMERGENCY_NUMBERS
                                ,"");
                        PreferenceManager.getInstance(mContext).writeToPrefs(PreferenceManager.EMERGENCY_NAMES
                                ,"");
                    }
                }
                contactArrayList.remove(contact1);
                notifyDataSetChanged();
            }
        });
        holder.imgDelete.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return contactArrayList.size();
    }
}
