/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.android.society.fragments.StaffInFragment;
import com.android.society.fragments.StaffOutFragment;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class StaffEntryPagerAdapter extends FragmentStatePagerAdapter {
    private StaffInFragment staffInFragment;
    private StaffOutFragment staffOutFragment;
    public StaffEntryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                staffInFragment = new StaffInFragment();
                fragment = staffInFragment;
                fragment.setRetainInstance(true);
                break;
            case 1:
                staffOutFragment = new StaffOutFragment();
                fragment = staffOutFragment;
                fragment.setRetainInstance(true);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
