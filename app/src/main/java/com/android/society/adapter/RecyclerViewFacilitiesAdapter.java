/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Model.Facilities;
import com.android.society.R;
import com.android.society.callbacks.OnFacilityEventClicked;

import java.util.ArrayList;

/**
 * Created by 201101101 on 10/16/2017.
 */

public class RecyclerViewFacilitiesAdapter extends RecyclerView.Adapter<RecyclerViewFacilitiesAdapter.MyViewHolder> {

        private ArrayList<Facilities> facilitiesList;

    public OnFacilityEventClicked getOnFacilityEventClicked() {
        return onFacilityEventClicked;
    }

    public void setOnFacilityEventClicked(OnFacilityEventClicked onFacilityEventClicked) {
        this.onFacilityEventClicked = onFacilityEventClicked;
    }

    private OnFacilityEventClicked onFacilityEventClicked;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private RelativeLayout relativeLayout;
            public ImageView imageView;
            public TextView title;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.textView);
                imageView = (ImageView) view.findViewById(R.id.imageView);
                relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout);
            }
        }


    public RecyclerViewFacilitiesAdapter(ArrayList<Facilities> moviesList) {
            this.facilitiesList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.grid_item_layout, parent, false);

            return new MyViewHolder(itemView);
        }



    @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final Facilities facilities = facilitiesList.get(position);
            holder.title.setText(facilities.getFacilityName());
            holder.relativeLayout.setTag(facilities);
            holder.imageView.setBackgroundResource(facilities.getIconID());
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Facilities facilities1 = (Facilities)view.getTag();
                    if(onFacilityEventClicked != null){
                        onFacilityEventClicked.onFacilityClicked(facilities1.getFacilityName());
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return facilitiesList.size();
        }
    }

