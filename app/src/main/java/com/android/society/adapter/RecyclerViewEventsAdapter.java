/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Event;
import com.android.society.Model.Facilities;
import com.android.society.R;
import com.android.society.callbacks.OnFacilityEventClicked;

import java.util.ArrayList;
import com.android.society.callbacks.onEventClicked;
import com.android.society.utils.PreferenceManager;

/**
 * Created by 201101101 on 10/17/2017.
 */

public class RecyclerViewEventsAdapter  extends RecyclerView.Adapter<RecyclerViewEventsAdapter.MyViewHolder> {

    private ArrayList<Event> eventArrayList;
    private Context mContext;

    public onEventClicked getOnEventClickedCall() {
        return onEventClickedCall;
    }

    public void setOnEventClickedCall(onEventClicked onEventClickedCall) {
        this.onEventClickedCall = onEventClickedCall;
    }

    private onEventClicked onEventClickedCall;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView title;
        public TextView description;
        public ImageView imgDelete;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_event);
            title = (TextView) view.findViewById(R.id.text_title);
            description = (TextView) view.findViewById(R.id.text_desc);
            imgDelete = (ImageView)view.findViewById(R.id.delete_event);
        }
    }


    public RecyclerViewEventsAdapter(ArrayList<Event> eventArrayList, Context paramContext) {
        this.eventArrayList = eventArrayList;
        mContext = paramContext;
    }

    @Override
    public RecyclerViewEventsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_show_event, parent, false);

        return new RecyclerViewEventsAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(RecyclerViewEventsAdapter.MyViewHolder holder, int position) {
        final Event event = eventArrayList.get(position);
        holder.title.setText(event.getTitle());
        holder.description.setText(event.getDescription());
        holder.relativeLayout.setTag(event);
        holder.imgDelete.setTag(event);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Event event = (Event) view.getTag();
                if(onEventClickedCall != null){
                    onEventClickedCall.onEventAdded(event);
                }
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Event event = (Event) view.getTag();
                if(onEventClickedCall != null){
                    onEventClickedCall.onEventDelete(event);
                }
            }
        });

        if(PreferenceManager.getInstance(mContext).getString(PreferenceManager.IS_LOGGED_IN_MODE)
                .equalsIgnoreCase(Constants.MODE_RWA)){
            holder.imgDelete.setVisibility(View.VISIBLE);
        }else{
            holder.imgDelete.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return eventArrayList.size();
    }
}


