/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.support.test.espresso.core.deps.guava.collect.Iterables;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Model.Facilities;
import com.android.society.R;
import com.android.society.callbacks.OnFacilityEventClicked;
import com.android.society.callbacks.OnResidentTowerSelected;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by 201101101 on 10/28/2017.
 */

public class ResidentTowersAdapter extends RecyclerView.Adapter<ResidentTowersAdapter.MyViewHolder> {

    private TreeSet<String> residentTowerList;
    private OnResidentTowerSelected onResidentTowerSelected;

    public OnResidentTowerSelected getOnResidentTowerSelected() {
        return onResidentTowerSelected;
    }

    public void setOnResidentTowerSelected(OnResidentTowerSelected onResidentTowerSelected) {
        this.onResidentTowerSelected = onResidentTowerSelected;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_fac);
            title = (TextView) view.findViewById(R.id.text_facility);
        }
    }


    public ResidentTowersAdapter(TreeSet<String> towerList) {
        this.residentTowerList = towerList;
    }

    @Override
    public ResidentTowersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_facility, parent, false);

        return new ResidentTowersAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(ResidentTowersAdapter.MyViewHolder holder, int position) {
        final String towerName = Iterables.get(residentTowerList, position);
        holder.title.setText(towerName);
        holder.relativeLayout.setTag(towerName);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String towerName = (String)view.getTag();
                if(onResidentTowerSelected != null){
                    onResidentTowerSelected.onResidentTowerSelected(towerName);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return residentTowerList.size();
    }
}


