/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Model.ResidentIn;
import com.android.society.R;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;
import com.android.society.utils.Utils;

import io.realm.RealmResults;

/**
 * Created by 201101101 on 11/6/2017.
 */

public class ResidentEntryListAdapter extends RecyclerView.Adapter<ResidentEntryListAdapter.MyViewHolder> {


    public RealmResults<ResidentIn> getResidentInRealmResults() {
        return residentInRealmResults;
    }

    public void setResidentInRealmResults(RealmResults<ResidentIn> residentInRealmResults) {
        this.residentInRealmResults = residentInRealmResults;
    }

    private RealmResults<ResidentIn> residentInRealmResults;
    private OnVisitorInteractionCallbacks onVisitorInteractionCallbacks;


    public OnVisitorInteractionCallbacks getOnVisitorInteractionCallbacks() {
        return onVisitorInteractionCallbacks;
    }

    public void setOnVisitorInteractionCallbacks(OnVisitorInteractionCallbacks onVisitorInteractionCallbacks) {
        this.onVisitorInteractionCallbacks = onVisitorInteractionCallbacks;
    }



    public ResidentEntryListAdapter(RealmResults<ResidentIn> residentIns){
        residentInRealmResults = residentIns;
    }

    @Override
    public ResidentEntryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.visitor_list_item, parent, false);
        return new ResidentEntryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ResidentEntryListAdapter.MyViewHolder holder, int position) {
        Log.v("anshul","o bind ");

        final ResidentIn residentIn = residentInRealmResults.get(position);
        holder.visitorName.setText(residentIn.getResidentName());

        try {
            holder.visitorNumber.setText(Utils.maskString(residentIn.getResidentNumber(),0,
                    residentIn.getResidentNumber().length()-2,'*'));
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.visitorVehicle.setText(residentIn.getVehicleName());
        holder.visitorFlat.setText(residentIn.getFlatVisited());
        holder.relativeLayout.setTag(residentIn);

        holder.visitorVehicle.setVisibility(View.GONE);
        holder.visitorVehicleLabel.setVisibility(View.GONE);
        holder.visitorPurpose.setVisibility(View.GONE);
        holder.visitorPurposeLabel.setVisibility(View.GONE);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResidentIn residentIn = (ResidentIn) view.getTag();
                if(onVisitorInteractionCallbacks != null){
                    onVisitorInteractionCallbacks.onResidentClicked(residentIn);
                }
            }
        });
        holder.visitorExit.setText("CALL");
        holder.visitorExit.setTag(residentIn);
        holder.visitorExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResidentIn residentIn1 = (ResidentIn) view.getTag();
                if(onVisitorInteractionCallbacks != null){
                    onVisitorInteractionCallbacks.onResidentCallMake(residentIn1);
                }
            }
        });
        holder.visitorNumber.setVisibility(View.GONE);
        holder.visitorNumberLabel.setVisibility(View.GONE);
        holder.imgDelete.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return residentInRealmResults.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView visitorName;
        public TextView visitorNumber;
        public TextView visitorVehicle;
        public TextView visitorFlat;
        public TextView visitorPurpose;
        public TextView visitorExit;
        public TextView visitorVehicleLabel;
        public TextView visitorPurposeLabel;
        public TextView visitorNumberLabel;
        public ImageView imgDelete;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_visitor);
            visitorName = (TextView) view.findViewById(R.id.text_visitor_name);
            visitorNumber = (TextView) view.findViewById(R.id.text_visitor_number);
            visitorVehicle = (TextView) view.findViewById(R.id.text_vehicle_name);
            visitorFlat = (TextView) view.findViewById(R.id.text_flat);
            visitorPurpose = (TextView) view.findViewById(R.id.text_purpose);
            visitorExit = (TextView) view.findViewById(R.id.text_exit_visitor);
            visitorVehicleLabel = (TextView) view.findViewById(R.id.text_vehicle_label);
            visitorPurposeLabel = (TextView) view.findViewById(R.id.text_purpose_label);
            visitorNumberLabel = (TextView)view.findViewById(R.id.text_number_label);
            imgDelete = (ImageView)view.findViewById(R.id.img_deleter);
        }
    }

}

