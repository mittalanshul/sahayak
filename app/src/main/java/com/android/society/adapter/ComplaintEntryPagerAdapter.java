/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.android.society.fragments.OpenComplaintsFragment;
import com.android.society.fragments.ResolvedComplaintsFragment;
import com.android.society.fragments.VisitorInFragment;
import com.android.society.fragments.VisitorOutFragment;

import java.util.HashMap;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class ComplaintEntryPagerAdapter extends FragmentStatePagerAdapter {
    private OpenComplaintsFragment openComplaintsFragment;
    private ResolvedComplaintsFragment resolvedComplaintsFragment;
    private HashMap<Integer,Fragment> mPageReferenceMap = new HashMap<>();
    public ComplaintEntryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }

    public Fragment getFragment(int key) {
        return mPageReferenceMap.get(key);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                openComplaintsFragment = new OpenComplaintsFragment();
                fragment = openComplaintsFragment;
                fragment.setRetainInstance(true);
                break;
            case 1:
                resolvedComplaintsFragment = new ResolvedComplaintsFragment();
                fragment = resolvedComplaintsFragment;
                fragment.setRetainInstance(true);
                break;
        }
        mPageReferenceMap.put(position, fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}

