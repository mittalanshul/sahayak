/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Model.Event;
import com.android.society.Model.FacilityType;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.callbacks.onEventClicked;

import java.util.ArrayList;

/**
 * Created by 201101101 on 10/17/2017.
 */

public class RecyclerViewFacilityTypeAdapter extends RecyclerView.Adapter<RecyclerViewFacilityTypeAdapter.MyViewHolder> {

    private ArrayList<StaffDetails> staffDetailsArrayList;
    private onEventClicked onEventClickedCall;

    public ArrayList<StaffDetails> getStaffDetailsArrayList() {
        return staffDetailsArrayList;
    }

    public void setStaffDetailsArrayList(ArrayList<StaffDetails> staffDetailsArrayList) {
        this.staffDetailsArrayList = staffDetailsArrayList;
    }


    public onEventClicked getOnEventClickedCall() {
        return onEventClickedCall;
    }

    public void setOnEventClickedCall(onEventClicked onEventClickedCall) {
        this.onEventClickedCall = onEventClickedCall;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView title;
        public TextView description;
        public ImageView imgDelete;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_event);
            title = (TextView) view.findViewById(R.id.text_title);
            description = (TextView) view.findViewById(R.id.text_desc);
            imgDelete = (ImageView)view.findViewById(R.id.delete_facility);
        }
    }


    public RecyclerViewFacilityTypeAdapter(ArrayList<StaffDetails> staffDetailsArrayList) {
        this.staffDetailsArrayList = staffDetailsArrayList;
    }

    @Override
    public RecyclerViewFacilityTypeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_facility_type, parent, false);

        return new RecyclerViewFacilityTypeAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(RecyclerViewFacilityTypeAdapter.MyViewHolder holder, int position) {
        final StaffDetails staffDetails = staffDetailsArrayList.get(position);
        holder.title.setText(staffDetails.getStaffName());
        holder.description.setText(staffDetails.getStaffNumber());
        holder.relativeLayout.setTag(staffDetails);
        holder.imgDelete.setTag(staffDetails);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaffDetails staffDetails1 = (StaffDetails) view.getTag();
                /*if(onEventClickedCall != null){
                    onEventClickedCall.onEventAdded(event);
                }*/
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaffDetails staffDetails1 = (StaffDetails) view.getTag();

            }
        });
    }

    @Override
    public int getItemCount() {
        return staffDetailsArrayList.size();
    }
}
