package com.android.society.adapter;

import android.support.test.espresso.core.deps.guava.collect.Iterables;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Model.Child;
import com.android.society.R;
import com.android.society.callbacks.ChildSelectedCallback;
import com.android.society.callbacks.OnResidentTowerSelected;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.logging.Logger;

/**
 * Created by anshul on 29/12/17.
 */

public class ChildRecyclerAdapter extends RecyclerView.Adapter<ChildRecyclerAdapter.MyViewHolder> {

   private ArrayList<Child> childArrayList;

    public ArrayList<Child> getChildArrayList() {
        return childArrayList;
    }

    public void setChildArrayList(ArrayList<Child> childArrayList) {
        this.childArrayList = childArrayList;
    }

    public ChildSelectedCallback getChildSelectedCallback() {
        return childSelectedCallback;
    }

    public void setChildSelectedCallback(ChildSelectedCallback childSelectedCallback) {
        this.childSelectedCallback = childSelectedCallback;
    }

    private ChildSelectedCallback childSelectedCallback;

    public ChildRecyclerAdapter(ArrayList<Child> paramChildArrayList){
        childArrayList = paramChildArrayList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView childName;
        public CheckBox checkBox;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_child);
            childName = (TextView) view.findViewById(R.id.text_child_name);
            checkBox = (CheckBox)view.findViewById(R.id.check_child);
        }
    }



    @Override
    public ChildRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_child_name, parent, false);

        return new ChildRecyclerAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(final ChildRecyclerAdapter.MyViewHolder holder, int position) {
        final Child child = childArrayList.get(position);
        Log.v("anshul","onBindViewHolder " + child.getChildName());
        holder.childName.setText(child.getChildName());
        holder.relativeLayout.setTag(child);
        holder.checkBox.setTag(child);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Child childSelected = (Child) view.getTag();
                holder.checkBox.setChecked(true);
                if(childSelectedCallback != null){
                    childSelectedCallback.onChildSelected(childSelected);
                }
            }
        });
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Child childSelected = (Child)buttonView.getTag();
                if(isChecked){
                    if(childSelectedCallback != null){
                        childSelectedCallback.onChildSelected(childSelected);
                    }
                }else{
                    if(childSelectedCallback != null){
                        childSelectedCallback.onChildDeselected();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return childArrayList.size();
    }
}

