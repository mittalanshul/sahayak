package com.android.society.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Complaint;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.callbacks.MakePhoneCallListener;
import com.android.society.callbacks.OnComplaintAdded;
import com.android.society.utils.PreferenceManager;
import com.android.society.widget.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by anshul on 14/02/18.
 */

public class DomesticHelpAdapter  extends RecyclerView.Adapter<DomesticHelpAdapter.MyViewHolder> {


    private ArrayList<StaffDetails> staffDetailsArrayList;
    private Context mContext;

    public MakePhoneCallListener getMakePhoneCallListener() {
        return makePhoneCallListener;
    }

    public void setMakePhoneCallListener(MakePhoneCallListener makePhoneCallListener) {
        this.makePhoneCallListener = makePhoneCallListener;
    }

    private MakePhoneCallListener makePhoneCallListener;
    public ArrayList<StaffDetails> getStaffDetailsArrayList() {
        return staffDetailsArrayList;
    }

    public void setStaffDetailsArrayList(ArrayList<StaffDetails> staffDetailsArrayList) {
        this.staffDetailsArrayList = staffDetailsArrayList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView label;
        public TextView name;
        public TextView number;
        public ImageView imgDelete;
        public TextView callDomestic;
        public CircularImageView circularImageView;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_domestic);
            label = (TextView) view.findViewById(R.id.label_domestic);
            name = (TextView) view.findViewById(R.id.text_staff_name);
            number = (TextView) view.findViewById(R.id.text_staff_number);
            circularImageView = (CircularImageView)view.findViewById(R.id.domestic_pic);
            callDomestic = (TextView)view.findViewById(R.id.call_domestic);
        }
    }


    public DomesticHelpAdapter(ArrayList<StaffDetails> staffDetailsArrayList, Context paramContext) {
        this.staffDetailsArrayList = staffDetailsArrayList;
        mContext = paramContext;
    }

    @Override
    public DomesticHelpAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_domestic_service, parent, false);

        return new DomesticHelpAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(DomesticHelpAdapter.MyViewHolder holder, int position) {
        final StaffDetails staffDetails = staffDetailsArrayList.get(position);
        holder.label.setText(staffDetails.getStaffType());
        holder.name.setText(staffDetails.getStaffName());
        holder.number.setText(staffDetails.getStaffNumber());
        holder.relativeLayout.setTag(staffDetails);
        holder.callDomestic.setTag(staffDetails.getStaffNumber());
        if(!TextUtils.isEmpty(staffDetails.getStaffNumber())){
            holder.callDomestic.setVisibility(View.VISIBLE);
        }else{
            holder.callDomestic.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(staffDetails.getImage())){
            String url = Constants.MEDIA_BASE_URL + staffDetails.getImage();
            Log.v("anshul","service image url is" + url);
            Picasso.with(mContext).load(url).placeholder(R.drawable.user_pic_def).error(R.drawable.user_pic_def)
                    .into(holder.circularImageView);
        }

        holder.callDomestic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(makePhoneCallListener != null){
                    String number = (String)v.getTag();
                    if(!TextUtils.isEmpty(number)){
                        makePhoneCallListener.onPhoneCallMake(number);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return staffDetailsArrayList.size();
    }
}
