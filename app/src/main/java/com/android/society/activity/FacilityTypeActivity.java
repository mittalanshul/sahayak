/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.support.constraint.solver.SolverVariable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.society.Constants.Constants;
import com.android.society.Model.FacilityType;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.adapter.RecyclerViewFacilitiesAdapter;
import com.android.society.adapter.RecyclerViewFacilityTypeAdapter;

import java.util.ArrayList;

public class FacilityTypeActivity extends BaseActivity {

    ArrayList<FacilityType> facilityTypeArrayList;
    private RecyclerViewFacilityTypeAdapter mRecyclerViewFacilitiesAdapter;
    private RecyclerView mRecyclerView;
    private String type;
    ArrayList<StaffDetails> staffDetailsArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facility_type);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_fac_type);
        if(getIntent() != null && getIntent().getExtras() != null)
        getSupportActionBar().setTitle(getIntent().getStringExtra(Constants.TYPE));
        if(getIntent() != null && getIntent().getExtras() != null){
            type = getIntent().getStringExtra(Constants.TYPE).toUpperCase();
            staffDetailsArrayList = getIntent().getParcelableArrayListExtra("list");
            Log.v("anshul","staffDetailsArrayList data " + staffDetailsArrayList);
            setRecyclerViewData();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



    private void setRecyclerViewData(){
        mRecyclerViewFacilitiesAdapter = new RecyclerViewFacilityTypeAdapter(staffDetailsArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mRecyclerViewFacilitiesAdapter);
    }
}
