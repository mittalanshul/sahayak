/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.android.society.Constants.Constants;
import com.android.society.Model.Facilities;
import com.android.society.R;
import com.android.society.adapter.RecyclerViewFacilitiesAdapter;
import com.android.society.callbacks.OnFacilityEventClicked;
import com.android.society.utils.NavigationUtils;

import java.util.ArrayList;

public class DialyServicesActivity extends BaseActivity implements OnFacilityEventClicked {
    private RecyclerView mRecyclerView;
    private RecyclerViewFacilitiesAdapter mRecyclerViewFacilitiesAdapter;
    private ArrayList<Facilities> facilitiesArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_facilities_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.DIALY_SERVICES);
        setData();
        setRecyclerViewData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData() {
        facilitiesArrayList = new ArrayList<>();

        Facilities facilities1 = new Facilities();
        facilities1.setFacilityName(Constants.ELECTRICIAN.toUpperCase());
        facilitiesArrayList.add(facilities1);

        Facilities facilities4 = new Facilities();
        facilities4.setFacilityName(Constants.PLUMBER.toUpperCase());
        facilitiesArrayList.add(facilities4);

        Facilities facilities2 = new Facilities();
        facilities2.setFacilityName(Constants.PHYSICIAN.toUpperCase());
        facilitiesArrayList.add(facilities2);


        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_fac_expand);

    }

    private void setRecyclerViewData() {
        mRecyclerViewFacilitiesAdapter = new RecyclerViewFacilitiesAdapter(facilitiesArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewFacilitiesAdapter.setOnFacilityEventClicked(this);
        mRecyclerView.setAdapter(mRecyclerViewFacilitiesAdapter);
    }

    @Override
    public void onFacilityClicked(String facilityName) {
       // NavigationUtils.navigateToFacilityType(getApplicationContext(), facilityName);
    }
}
