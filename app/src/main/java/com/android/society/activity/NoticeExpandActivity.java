/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.society.Model.Event;
import com.android.society.R;

/**
 * Created by 201101101 on 12/1/2017.
 */

public class NoticeExpandActivity extends BaseActivity {

    private Event event;
    private TextView textComplaintNo;
    private TextView textStatus;
    private TextView textType;
    private TextView textDesc;
    private TextView textSubject;
    private TextView textDate;
    private TextView textTime;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_detail);
        if(getIntent() != null && getIntent().getExtras() != null){
            event = getIntent().getParcelableExtra("event");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if(!TextUtils.isEmpty(event.getTitle())){
            getSupportActionBar().setTitle(event.getTitle());
        }else{
            getSupportActionBar().setTitle("Notice");
        }


        initViews();
        setData();
    }

    private void setData(){
        textComplaintNo.setText(event.getId());
        textType.setText(event.getType());
        textDesc.setText(event.getDescription());
        textSubject.setText(event.getTitle());
        TextView textView = (TextView)findViewById(R.id.text_complaint_number_label);
        textView.setText("Notice No.");
        TextView textViewDesc = (TextView)findViewById(R.id.text_desc_label);
        textViewDesc.setText("Notice Description");
        if(TextUtils.isEmpty(event.getType())){
            textType.setVisibility(View.GONE);
            findViewById(R.id.text_type_label).setVisibility(View.GONE);
        }
        if(TextUtils.isEmpty(event.getDate())){
            textDate.setVisibility(View.GONE);
            findViewById(R.id.text_date_label).setVisibility(View.GONE);
        }else{
            textDate.setText(event.getDate());
            textDate.setVisibility(View.VISIBLE);
            findViewById(R.id.text_date_label).setVisibility(View.VISIBLE);
        }

        if(TextUtils.isEmpty(event.getTime())){
            textTime.setVisibility(View.GONE);
            findViewById(R.id.text_time_label).setVisibility(View.GONE);
        }else{
            textTime.setText(event.getTime());
            textTime.setVisibility(View.VISIBLE);
            findViewById(R.id.text_time_label).setVisibility(View.VISIBLE);
        }
    }

    private void initViews(){
        textComplaintNo = (TextView)findViewById(R.id.text_complaint_number);
        textStatus = (TextView)findViewById(R.id.text_status);
        textType = (TextView)findViewById(R.id.text_type);
        textDesc = (TextView)findViewById(R.id.text_desc);
        textSubject = (TextView)findViewById(R.id.text_subject);
        textDate = (TextView)findViewById(R.id.text_date);
        textTime = (TextView)findViewById(R.id.text_time);
        mLinearLayout = (LinearLayout)findViewById(R.id.linear_continue);
        mLinearLayout.setVisibility(View.GONE);
        textStatus.setVisibility(View.GONE);
        findViewById(R.id.text_status_label).setVisibility(View.GONE);
        findViewById(R.id.card_comments).setVisibility(View.GONE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}


