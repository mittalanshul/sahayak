package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Child;
import com.android.society.R;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

public class ChildSafetyActivity extends BaseActivity implements View.OnClickListener {

    private TextView textDeny;
    private TextView textApprove;
    private String data;
    private TextView textChildLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_safety);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        initViews();
        initClickListener();

        if(getIntent() != null && getIntent().getExtras() != null){
           data = getIntent().getStringExtra(Constants.NOTIFICATION_DATA);
           if(!TextUtils.isEmpty(data)){
               Log.v("anshul","the data is " + data);
               setData();
           }
        }


    }

    private void setData(){
        textChildLabel.setText("Hi Resident , your child " + data.toUpperCase() + " wants to exit out from the society");
    }

    private void initViews(){
        textDeny = (TextView)findViewById(R.id.btn_deny);
        textApprove = (TextView)findViewById(R.id.btn_approve);
        textChildLabel = (TextView)findViewById(R.id.child_alert_label);
    }

    private void initClickListener(){
        textDeny.setOnClickListener(this);
        textApprove.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case R.id.btn_deny:
               triggerCallForExitAction("no",data);
               break;
           case R.id.btn_approve:
               triggerCallForExitAction("yes",data);
               break;
       }
    }

    private void triggerCallForExitAction(final String action , String childName){
        showProgressDialog("Please wait .. Sending your response",false);
        String url = Constants.BASE_URL + Constants.CHILD_EXIT_ACTION+"?answer="+action+"&name="+childName;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        removeProgressDialog();
                        BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                        if(baseModel.isStatus()){
                            String approval = "Approval";
                            if(action.equalsIgnoreCase("no")){
                                approval = "Denial";
                            }
                            Utils.displayToast(ChildSafetyActivity.this,"Guard is notified for the " +approval);
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        removeProgressDialog();
                        Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Utils.getHeaders(getApplicationContext());
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



        };
        queue.add(postRequest);
    }
}
