/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Complaint;
import com.android.society.R;
import com.android.society.callbacks.CommentAddedCallback;
import com.android.society.dialog.AddCommentsDialog;
import com.android.society.dialog.AlertNotifiedDialog;
import com.android.society.fragments.EnterCommentFragment;
import com.android.society.fragments.EnterOTPFragment;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class ComplaintDetailActivity extends BaseActivity implements CommentAddedCallback {

    private Complaint complaint;
    private TextView textComplaintNo;
    private TextView textStatus;
    private TextView textType;
    private TextView textDesc;
    private TextView textSubject;
    private LinearLayout mLinearLayout;
    private TextView mTextCompStatus;
    private CardView mCardComments;
    private TextView mTextComment;
    private String commentAdded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_detail);
        if(getIntent() != null && getIntent().getExtras() != null){
            complaint = getIntent().getParcelableExtra("complaint");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if(!TextUtils.isEmpty(complaint.getSubject())){
            getSupportActionBar().setTitle("Complaint No. " +complaint.getId());
        }else{
            getSupportActionBar().setTitle("Complaint");
        }

        initViews();
        setData();
    }

    private void setData(){
        textComplaintNo.setText(complaint.getId());
        textStatus.setText(complaint.getStatus());
        textType.setText(complaint.getType());
        textDesc.setText(complaint.getDesc());
        textSubject.setText(complaint.getSubject());
        if(Utils.getLoggedInMode(getApplicationContext()).equalsIgnoreCase(Constants.MODE_RESIDENT)){
            if(complaint.getStatus().equalsIgnoreCase("Solved")){
                mTextCompStatus.setText("REOPEN");
            }
        }
        findViewById(R.id.text_date_label).setVisibility(View.GONE);
        findViewById(R.id.text_date).setVisibility(View.GONE);
        findViewById(R.id.text_time_label).setVisibility(View.GONE);
        findViewById(R.id.text_time).setVisibility(View.GONE);
        if(!TextUtils.isEmpty(complaint.getComments())){
            mCardComments.setVisibility(View.VISIBLE);
            mTextComment.setText(complaint.getComments());
        }else{
            mCardComments.setVisibility(View.GONE);
        }

        if(complaint.getStatus().equalsIgnoreCase(Constants.OPEN) &&
                PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE,"")
                        .equalsIgnoreCase(Constants.MODE_RESIDENT)){
            mLinearLayout.setVisibility(View.GONE);
        }else{
            mLinearLayout.setVisibility(View.VISIBLE);
        }

    }

    private void initViews(){
        textComplaintNo = (TextView)findViewById(R.id.text_complaint_number);
        textStatus = (TextView)findViewById(R.id.text_status);
        textType = (TextView)findViewById(R.id.text_type);
        textDesc = (TextView)findViewById(R.id.text_desc);
        textSubject = (TextView)findViewById(R.id.text_subject);
        mTextComment = (TextView)findViewById(R.id.text_comments);
        mLinearLayout = (LinearLayout)findViewById(R.id.linear_continue);
        mCardComments = (CardView)findViewById(R.id.card_comments);
        mTextCompStatus = (TextView)findViewById(R.id.btn_continue);
        mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.getLoggedInMode(getApplicationContext()).equalsIgnoreCase(Constants.MODE_RWA)){
                    if(complaint.getStatus().equalsIgnoreCase(Constants.OPEN)){
                        if(TextUtils.isEmpty(commentAdded)){
                            showCommentsFragment();
                        }else{
                            triggerCallToUpdateComplaintStatus(Constants.SOLVED,commentAdded);
                        }
                    }
                } else if(Utils.getLoggedInMode(getApplicationContext()).equalsIgnoreCase(Constants.MODE_RESIDENT)){
                    if(complaint.getStatus().equalsIgnoreCase(Constants.SOLVED)){
                        if(TextUtils.isEmpty(commentAdded)){
                            showCommentsFragment();
                        }else{
                            triggerCallToUpdateComplaintStatus(Constants.OPEN,commentAdded);
                        }
                    }
                }
            }
        });
    }

    private void showCommentsFragment() {
        if(Utils.isInternetAvailable(this)){
            EnterCommentFragment enterCommentFragment = new EnterCommentFragment();
            enterCommentFragment.setRetainInstance(true);
            enterCommentFragment.setCommentAddedCallback(this);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                    R.anim.exit_out_bottom).add(R.id.rel_complaint, enterCommentFragment, "Comment")
                    .addToBackStack("Comment").commitAllowingStateLoss();
        } else {
            Utils.displayToast(this,getString(R.string.internet_error));
        }
    }

    private void triggerCallToUpdateComplaintStatus(String status,String comments){
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id",complaint.getId());
            jsonBody.put("status", status);
            jsonBody.put("comment", comments);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait ... updating status",false);
            String url = "";
            if(Utils.getLoggedInMode(getApplicationContext()).equalsIgnoreCase(Constants.MODE_RWA)){
                url = Constants.BASE_URL + Constants.GET_ALL_COMPLAINTS ;
            }else if(Utils.getLoggedInMode(getApplicationContext()).equalsIgnoreCase(Constants.MODE_RESIDENT)){
                url = Constants.BASE_URL + Constants.UPDATE_ALL_RESIDENT_COMPLAINTS ;
            }

            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Intent intent = new Intent();
                                setResult(Activity.RESULT_OK , intent);
                                finish();
                            }
                            Utils.displayToast(ComplaintDetailActivity.this,baseModel.getMessage());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }
            };
            queue.add(postRequest);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCommentAdded(String comment) {
        if(!TextUtils.isEmpty(comment)){
          commentAdded = comment;
            if(Utils.getLoggedInMode(getApplicationContext()).equalsIgnoreCase(Constants.MODE_RWA)){
                if(complaint.getStatus().equalsIgnoreCase(Constants.OPEN)){
                    if(TextUtils.isEmpty(commentAdded)){
                        showCommentsFragment();
                    }else{
                        triggerCallToUpdateComplaintStatus(Constants.SOLVED,commentAdded);
                    }
                }
            } else if(Utils.getLoggedInMode(getApplicationContext()).equalsIgnoreCase(Constants.MODE_RESIDENT)){
                if(complaint.getStatus().equalsIgnoreCase(Constants.SOLVED)){
                    if(TextUtils.isEmpty(commentAdded)){
                        showCommentsFragment();
                    }else{
                        triggerCallToUpdateComplaintStatus(Constants.OPEN,commentAdded);
                    }
                }
            }
        }else{
            Utils.displayToast(this,"Please add comment to update complaint");
        }
    }
}
