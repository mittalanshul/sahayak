/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Complaint;
import com.android.society.Model.Event;
import com.android.society.R;
import com.android.society.callbacks.OnComplaintAdded;
import com.android.society.parser.AllComplaintsParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class CreateComplaintActivity extends BaseActivity implements View.OnClickListener {
    private Button createComplaint;
    private EditText title;
    private EditText desc;
    private MaterialBetterSpinner materialDesignSpinnerEventType;
    String[] COMPLAINT_TYPE_LIST = {"Seepage","Tenant","Security","Maintenance","Personal","Other"};
    private String type;

    public OnComplaintAdded getOnComplaintAddedCall() {
        return onComplaintAddedCall;
    }

    public void setOnComplaintAddedCall(OnComplaintAdded onComplaintAddedCall) {
        this.onComplaintAddedCall = onComplaintAddedCall;
    }

    private OnComplaintAdded onComplaintAddedCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_complaint);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("CREATE COMPLAINT");

        title = (EditText) findViewById(R.id.text_event_title);
        desc = (EditText) findViewById(R.id.edt_desc);
        createComplaint = (Button) findViewById(R.id.create_complaint);
        createComplaint.setOnClickListener(this);

        materialDesignSpinnerEventType = (MaterialBetterSpinner) findViewById(R.id.spinner_event_type);

        ArrayAdapter<String> arrayAdapterName = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, COMPLAINT_TYPE_LIST);
        materialDesignSpinnerEventType.setAdapter(arrayAdapterName);
        arrayAdapterName.notifyDataSetChanged();
        materialDesignSpinnerEventType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                type = adapterView.getItemAtPosition(i).toString();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void triggerCallToCreateComplaint(){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Creating Complaint",false);
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("subject",title.getText().toString().trim());
                jsonBody.put("desc",desc.getText().toString().trim());
                jsonBody.put("type",type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String mRequestBody = jsonBody.toString();
            Log.v("anshul" ,"type send while creating complaint is " + mRequestBody);
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL
                    + Constants.CREATE_COMPLAINT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Complaint complaint = AllComplaintsParser.getComplaint(response);
                                if(complaint != null){
                                    Log.v("anshul","onResponse" + response);
                                    Intent intent = new Intent();
                                    intent.putExtra("complaint",complaint);
                                    setResult(RESULT_OK,intent);
                                    finish();
                                }
                            }
                            Utils.displayToast(getApplicationContext(),baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_complaint:
                if (isDataValid()) {
                    triggerCallToCreateComplaint();
                    /*Complaint complaint = new Complaint();
                    complaint.setName(title.getText().toString().trim());
                    complaint.setDescription(desc.getText().toString().trim());
                    Intent intent = new Intent();
                    intent.putExtra("complaint", complaint);
                    setResult(RESULT_OK,intent);
                    finish();*/
                }
                break;
        }
    }

    private boolean isDataValid() {
        if (TextUtils.isEmpty(title.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter title", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(type)) {
            Toast.makeText(getApplicationContext(), "Please select complaint type", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(desc.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter description", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
