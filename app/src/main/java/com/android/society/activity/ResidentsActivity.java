/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.Resident;
import com.android.society.R;
import com.android.society.adapter.RecyclerViewFacilitiesAdapter;
import com.android.society.adapter.ResidentTowersAdapter;
import com.android.society.callbacks.OnResidentAdded;
import com.android.society.callbacks.OnResidentTowerSelected;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.Utils;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReferenceArray;

public class ResidentsActivity extends BaseActivity implements View.OnClickListener,OnResidentTowerSelected {
    private RecyclerView mRecyclerView;
    private FloatingActionButton floatingActionButton;
    private ResidentTowersAdapter residentTowersAdapter;

    ArrayList<Resident> residentsArrayList;
    AllSocietyData allSocietyData ;
    private String towerNameSelected;
    private boolean isFromChildSafety;
    private boolean isFromPanic;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("all_data",allSocietyData);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_residents);
        if(savedInstanceState != null){
            allSocietyData = savedInstanceState.getParcelable("all_data");
        }else{
            if(getIntent() != null && getIntent().getExtras() != null){
                allSocietyData = getIntent().getParcelableExtra("all_data");
                isFromChildSafety = getIntent().getBooleanExtra("child_safety",false);
                isFromPanic = getIntent().getBooleanExtra("panic",false);
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.RESIDENTS);

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view_resident);
        floatingActionButton = (FloatingActionButton)findViewById(R.id.fab_resident);
        floatingActionButton.setOnClickListener(this);
        floatingActionButton.setVisibility(View.GONE);
        residentsArrayList = new ArrayList<>();
        showEmptyView();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void showEmptyView(){
        if(allSocietyData != null && allSocietyData.getTowerSet() != null && allSocietyData.getTowerSet().size()>0){
            findViewById(R.id.empty_view).setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            setRecyclerViewData();
        } else {
            findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == 42){
            if(data != null){
                String name = data.getStringExtra("neighbour_name");
                if(!TextUtils.isEmpty(name)){
                    Intent intent = new Intent();
                    intent.putExtra("neighbour_name",name);
                    setResult(Activity.RESULT_OK , intent);
                    finish();
                }
            }

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fab_event:
                NavigationUtils.navigateToAddEvent(this);
                break;
        }
    }

    private void setRecyclerViewData(){
        residentTowersAdapter = new ResidentTowersAdapter(allSocietyData.getTowerSet());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        residentTowersAdapter.setOnResidentTowerSelected(this);
        mRecyclerView.setAdapter(residentTowersAdapter);
    }

    @Override
    public void onResidentTowerSelected(String towerName) {
        towerNameSelected = towerName;
        NavigationUtils.navigateToResidentFlatsSection(this,towerNameSelected,allSocietyData,isFromChildSafety,isFromPanic);
    }
}
