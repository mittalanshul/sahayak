/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Event;
import com.android.society.R;
import com.android.society.adapter.RecyclerViewEventsAdapter;
import com.android.society.adapter.RecyclerViewFacilitiesAdapter;
import com.android.society.callbacks.onEventClicked;
import com.android.society.parser.BaseModelParser;
import com.android.society.parser.EventListParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

public class EventDetailActivity extends BaseActivity implements View.OnClickListener,onEventClicked {

    private RecyclerView mRecyclerView;
    private FloatingActionButton floatingActionButton;
    private RecyclerViewEventsAdapter mRecyclerViewEventsAdapter;
    private ArrayList<Event> eventArrayList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.EVENTS);

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view_event);
        floatingActionButton = (FloatingActionButton)findViewById(R.id.fab_event);
        floatingActionButton.setOnClickListener(this);
        if(PreferenceManager.getInstance(getApplicationContext()).getString(PreferenceManager.IS_LOGGED_IN_MODE)
                .equalsIgnoreCase(Constants.MODE_RWA)){
            floatingActionButton.setVisibility(View.VISIBLE);
        }else{
            floatingActionButton.setVisibility(View.GONE);
        }

        CreateEventActivity createEventActivity = new CreateEventActivity();
        createEventActivity.setOnEventClickedCall(this);
        triggerCallToGetEvent();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showEmptyView(){
        if(eventArrayList != null && eventArrayList.size() >0){
            findViewById(R.id.empty_view).setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            setRecyclerViewData();
        } else {
            findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    public void triggerCallToGetEvent(){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Getting All Event",false);
            String url;
            if(PreferenceManager.getInstance(getApplicationContext()).getString(PreferenceManager.IS_LOGGED_IN_MODE)
                    .equalsIgnoreCase(Constants.MODE_RESIDENT)){
                url = Constants.BASE_URL + Constants.CREATE_EVENT_RESIDENT;
            }else{
                url = Constants.BASE_URL + Constants.CREATE_EVENT;
            }
            StringRequest postRequest = new StringRequest(Request.Method.GET,url ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Log.v("anshul","onResponse" + response);
                                eventArrayList = EventListParser.getAllEvents(response);
                                showEmptyView();
                            }
                            Utils.displayToast(EventDetailActivity.this,baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                            showEmptyView();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    public void triggerCallToRemoveEvent(final Event event){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Deleting event",false);
            String url = Constants.BASE_URL + Constants.CREATE_EVENT+"?id="+event.getId();
            Log.v("anshul","url is " + url);
            StringRequest postRequest = new StringRequest(Request.Method.DELETE,url ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Log.v("anshul","onResponse" + response);
                                eventArrayList.remove(event);
                                showEmptyView();
                            }
                            Utils.displayToast(EventDetailActivity.this,baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                            showEmptyView();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    private void setRecyclerViewData(){
        mRecyclerViewEventsAdapter = new RecyclerViewEventsAdapter(eventArrayList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewEventsAdapter.setOnEventClickedCall(this);
        mRecyclerView.setAdapter(mRecyclerViewEventsAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fab_event:
                NavigationUtils.navigateToAddEvent(EventDetailActivity.this);
                break;
        }
    }

    @Override
    public void onEventAdded(Event event) {
        NavigationUtils.navigateToEvent(this,event);
    }

    @Override
    public void onEventDelete(Event event) {
        if(eventArrayList != null && eventArrayList.size()>0){
            triggerCallToRemoveEvent(event);

        }
    }
}
