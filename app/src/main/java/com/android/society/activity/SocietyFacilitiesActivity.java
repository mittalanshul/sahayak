/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Facilities;
import com.android.society.R;
import com.android.society.adapter.RecyclerViewFacilitiesAdapter;
import com.android.society.callbacks.OnFacilityEventClicked;
import com.android.society.dialog.AlertNotifiedDialog;
import com.android.society.parser.AllSocietyDataParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.receiver.SensorInteractionListener;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.ShakeSensor;
import com.android.society.utils.Utils;
import com.android.society.widget.AutoFitRecyclerView;
import com.android.society.widget.SpanSizeLokkup;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

import io.realm.internal.Util;

public class SocietyFacilitiesActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnFacilityEventClicked, SensorInteractionListener {

    private AutoFitRecyclerView mRecyclerView;
    private RecyclerViewFacilitiesAdapter mRecyclerViewFacilitiesAdapter;
    private ArrayList<Facilities> facilitiesArrayList;
    private TextView mTextHeaderTitle;
    private TextView mTextHeaderTitleSub;
    private TextView mTextHeaderEmail;
    private ImageView imageView;
    AllSocietyData allSocietyData;
    private SensorManager sensorManager;
    private ShakeSensor shakeSensor;
    private long lastUpdate;
    private FloatingActionButton floatingActionButton;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_society_facilities);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (AutoFitRecyclerView) findViewById(R.id.recycler_facilities);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        String mode = PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE);
        if (mode.equalsIgnoreCase(Constants.MODE_GUARD)) {
            getSupportActionBar().setTitle("Welcome Gate Guard");
        } else if (mode.equalsIgnoreCase(Constants.MODE_RESIDENT)) {
            getSupportActionBar().setTitle("Welcome " + PreferenceManager.getInstance(this).getString(PreferenceManager.USER_NAME));
        } else if (mode.equalsIgnoreCase(Constants.MODE_RWA)) {
            getSupportActionBar().setTitle(PreferenceManager.getInstance(this).getString(PreferenceManager.USER_NAME) + " RWA");
        }


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        mTextHeaderTitle = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_nav_title);
        mTextHeaderTitleSub = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_nav_sub);
        mImageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
        mTextHeaderTitleSub.setText("Welcome " + PreferenceManager.getInstance(getApplicationContext()).
                getString(PreferenceManager.USER_NAME, Constants.GUARDS));

        if (imageView != null && !TextUtils.isEmpty(PreferenceManager.getInstance(this).getString(PreferenceManager.LOGO))) {
            String url = Constants.MEDIA_BASE_URL + PreferenceManager.getInstance(this).getString(PreferenceManager.LOGO);
            Picasso.with(this)
                    .load(url)
                    .placeholder(android.R.drawable.sym_def_app_icon)
                    .error(android.R.drawable.sym_def_app_icon)
                    .into(imageView);
        }

        mTextHeaderEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_nav_name);
        if (PreferenceManager.getInstance(SocietyFacilitiesActivity.this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RWA)) {
            mTextHeaderEmail.setText(PreferenceManager.getInstance(getApplicationContext()).
                    getString(PreferenceManager.USER_EMAIL));
        } else {
            mTextHeaderEmail.setText(PreferenceManager.getInstance(getApplicationContext()).
                    getString(PreferenceManager.USER_FLAT));
        }

        floatingActionButton = findViewById(R.id.fab_panic);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUIOnActivity();
            }
        });


        navigationView.setNavigationItemSelectedListener(this);
        setData();
        setRecyclerViewData();
        setShakeListener();

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    }

    private void showAlertDialogNotified() {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Guard Alerted");
            alertDialogBuilder
                    .setMessage("Guard has been notified , help will reach you shortly")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
    }

    private void setShakeListener() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lastUpdate = System.currentTimeMillis();
        shakeSensor = new ShakeSensor(lastUpdate);
        shakeSensor.setSensorInteractionListener(this);
    }


    private void setData() {
        facilitiesArrayList = new ArrayList<>();

        if (PreferenceManager.getInstance(SocietyFacilitiesActivity.this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_GUARD)) {
            //floatingActionButton.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            Facilities facilities6 = new Facilities();
            facilities6.setIconID(R.drawable.attendance);
            facilities6.setFacilityName(Constants.VISITOR_ENTRY.toUpperCase());
            facilitiesArrayList.add(facilities6);

            Facilities facilities9 = new Facilities();
            facilities9.setIconID(R.drawable.attendance);
            facilities9.setFacilityName(Constants.VISITOR_REGISTER.toUpperCase());
            facilitiesArrayList.add(facilities9);

            Facilities facilities8 = new Facilities();
            facilities8.setIconID(R.drawable.attendance);
            facilities8.setFacilityName(Constants.ATTENDENCE.toUpperCase());
            facilitiesArrayList.add(facilities8);

            Facilities facilities14 = new Facilities();
            facilities14.setIconID(R.drawable.attendance);
            facilities14.setFacilityName(Constants.STAFF_ATTENDENCE.toUpperCase());
            facilitiesArrayList.add(facilities14);

            Facilities facilities10 = new Facilities();
            facilities10.setIconID(R.drawable.attendance);
            facilities10.setFacilityName(Constants.STAFF_REGISTER.toUpperCase());
            facilitiesArrayList.add(facilities10);

            Facilities facilities11 = new Facilities();
            facilities11.setIconID(R.drawable.entry);
            facilities11.setFacilityName(Constants.RESIDENT_ENTRY.toUpperCase());
            facilitiesArrayList.add(facilities11);

            Facilities facilities12 = new Facilities();
            facilities12.setIconID(R.drawable.child_safety);
            facilities12.setFacilityName(Constants.CHILD_EXIT_SAFETY.toUpperCase());
            facilitiesArrayList.add(facilities12);

            Facilities facilities13 = new Facilities();
            facilities13.setFacilityName(Constants.EXPECTED_VISITOR.toUpperCase());
            facilitiesArrayList.add(facilities13);

            String url = Constants.BASE_URL + Constants.GET_ALL_GUARD_DATA;
            configureAppData(url);
        } else if (PreferenceManager.getInstance(SocietyFacilitiesActivity.this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RWA)) {
            floatingActionButton.setVisibility(View.GONE);
            Facilities facilities = new Facilities();
            facilities.setIconID(R.drawable.news);
            facilities.setFacilityName(Constants.EVENTS.toUpperCase());
            facilitiesArrayList.add(facilities);

            Facilities facilities22 = new Facilities();
            facilities.setIconID(R.drawable.news);
            facilities22.setFacilityName(Constants.NOTICES.toUpperCase());
            facilitiesArrayList.add(facilities22);

            Facilities facilities1 = new Facilities();
            facilities1.setIconID(R.drawable.services);
            facilities1.setFacilityName(Constants.FACILITIES.toUpperCase());
            facilitiesArrayList.add(facilities1);

            Facilities facilities4 = new Facilities();
            facilities.setIconID(R.drawable.news);
            facilities4.setFacilityName(Constants.COMPLAINTS.toUpperCase());
            facilitiesArrayList.add(facilities4);

            Facilities facilities2 = new Facilities();
            facilities2.setIconID(R.drawable.residents);
            facilities2.setFacilityName(Constants.RESIDENTS.toUpperCase());
            facilitiesArrayList.add(facilities2);

            Facilities facilities5 = new Facilities();
            facilities5.setFacilityName(Constants.RWA_MEMBERS.toUpperCase());
            facilitiesArrayList.add(facilities5);


            String url = Constants.BASE_URL + Constants.GET_ALL_RESIDENT_DATA;
            configureAppData(url);
        } else if (PreferenceManager.getInstance(SocietyFacilitiesActivity.this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RESIDENT)) {
            mRecyclerView.setVisibility(View.VISIBLE);
            floatingActionButton.setVisibility(View.VISIBLE);

            Facilities facilities = new Facilities();
            facilities.setIconID(R.drawable.news);
            facilities.setFacilityName(Constants.EVENTS.toUpperCase());
            facilitiesArrayList.add(facilities);

            Facilities facilities4 = new Facilities();
            facilities4.setIconID(R.drawable.news);
            facilities4.setFacilityName(Constants.COMPLAINTS.toUpperCase());
            facilitiesArrayList.add(facilities4);

            Facilities facilities7 = new Facilities();
            facilities7.setIconID(R.drawable.news);
            facilities7.setFacilityName(Constants.DIALY_SERVICES.toUpperCase());
            facilitiesArrayList.add(facilities7);

            Facilities facilities9 = new Facilities();
            facilities9.setIconID(R.drawable.profile);
            facilities9.setFacilityName(Constants.RESIDENT_PROFILE.toUpperCase());
            facilitiesArrayList.add(facilities9);

            Facilities facilities10 = new Facilities();
            facilities10.setIconID(R.drawable.services);
            facilities10.setFacilityName(Constants.ADD_SERVICES.toUpperCase());
            facilitiesArrayList.add(facilities10);

            Facilities facilities11 = new Facilities();
            facilities11.setIconID(R.drawable.history);
            facilities11.setFacilityName(Constants.VISITOR_HISTORY.toUpperCase());
            facilitiesArrayList.add(facilities11);

            Facilities facilities12 = new Facilities();
            facilities12.setIconID(R.drawable.emergency_1);
            facilities12.setFacilityName(Constants.ADD_EMERGENCY_CONTACTS.toUpperCase());
            facilitiesArrayList.add(facilities12);

            Facilities facilities13 = new Facilities();
            facilities13.setIconID(R.drawable.emergency_1);
            facilities13.setFacilityName(Constants.EXPECTED_VISITOR.toUpperCase());
            facilitiesArrayList.add(facilities13);

            Facilities facilities14 = new Facilities();
            facilities14.setIconID(R.drawable.residents);
            facilities14.setFacilityName(Constants.RESIDENTS.toUpperCase());
            facilitiesArrayList.add(facilities14);

            Facilities facilities15 = new Facilities();
            facilities15.setIconID(R.drawable.dues);
            facilities15.setFacilityName(Constants.MAINTAINENCE_DUES.toUpperCase());
            facilitiesArrayList.add(facilities15);

            String url = Constants.BASE_URL + Constants.GET_RESIDENT_DIRECTORY;
            Log.v("anshul", "url to be called " + url);
            configureResidentDirectory(url);
        }

    }

    private void setRecyclerViewData() {
        mRecyclerViewFacilitiesAdapter = new RecyclerViewFacilitiesAdapter(facilitiesArrayList);
        mRecyclerViewFacilitiesAdapter.setOnFacilityEventClicked(this);
        if (mRecyclerView.getLayoutManager() instanceof GridLayoutManager) {
            int spanCount = 2;

            mRecyclerView.setExternalSpanCount(spanCount);
            ((GridLayoutManager) mRecyclerView.getLayoutManager())
                    .setSpanCount(2);
            ((GridLayoutManager) mRecyclerView.getLayoutManager()).setSpanSizeLookup(new SpanSizeLokkup());
        }
        mRecyclerView.setAdapter(mRecyclerViewFacilitiesAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_logout) {
            NavigationUtils.navigateToLogout(this);
            NavigationUtils.navigateToModeLogin(this);
            finish();
            Utils.displayToast(getApplicationContext(), "You have been logged out successfully");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFacilityClicked(String facilityName) {
        if (facilityName.equalsIgnoreCase(Constants.EVENTS)) {
            NavigationUtils.navigateToEventDetails(this);
        } else if (facilityName.equalsIgnoreCase(Constants.FACILITIES)) {
            NavigationUtils.navigateToFacilitiesDetails(this, allSocietyData);
        } else if (facilityName.equalsIgnoreCase(Constants.COMPLAINTS)) {
            NavigationUtils.navigateToComplaintDetails(this);
        } else if (facilityName.equalsIgnoreCase(Constants.RESIDENTS)) {
            NavigationUtils.navigateToResidentSection(this, allSocietyData, false, false);
        } else if (facilityName.equalsIgnoreCase(Constants.CHILD_EXIT_SAFETY)) {
            NavigationUtils.navigateToResidentSection(this, allSocietyData, true, false);
        } else if (facilityName.equalsIgnoreCase(Constants.RWA_MEMBERS)) {

        } else if (facilityName.equalsIgnoreCase(Constants.VISITOR_ENTRY)) {
            NavigationUtils.navigateToVisitorEntry(this, allSocietyData, false);
        } else if (facilityName.equalsIgnoreCase(Constants.EXPECTED_VISITOR)) {
            NavigationUtils.navigateToExpectedVisitor(this);
        } else if (facilityName.equalsIgnoreCase(Constants.DIALY_SERVICES)) {
            NavigationUtils.navigateToDialyServices(this);
        } else if (facilityName.equalsIgnoreCase(Constants.ATTENDENCE)) {
            NavigationUtils.navigateToAttendence(this, allSocietyData);
        } else if (facilityName.equalsIgnoreCase(Constants.STAFF_ATTENDENCE)) {
            NavigationUtils.navigateToStaffAttendence(this, allSocietyData);
        } else if (facilityName.equalsIgnoreCase(Constants.RESIDENT_PROFILE)) {
            NavigationUtils.navigateToResidentProfile(this, null);
        } else if (facilityName.equalsIgnoreCase(Constants.VISITOR_REGISTER)) {
            NavigationUtils.navigateToVisitorEntryRegister(this);
        } else if (facilityName.equalsIgnoreCase(Constants.STAFF_REGISTER)) {
            NavigationUtils.navigateToStaffEntryRegister(this);
        } else if (facilityName.equalsIgnoreCase(Constants.RESIDENT_ENTRY)) {
            NavigationUtils.navigateToResidentEntry(this, allSocietyData);
        } else if (facilityName.equalsIgnoreCase(Constants.ADD_SERVICES)) {
            NavigationUtils.navigateToAddServices(this);
        } else if (facilityName.equalsIgnoreCase(Constants.NOTICES)) {
            NavigationUtils.navigateToNoticeDetails(this);
        } else if (facilityName.equalsIgnoreCase(Constants.VISITOR_HISTORY)) {
            NavigationUtils.navigateToResidentVisitorHistory(this);
        } else if (facilityName.equalsIgnoreCase(Constants.ADD_EMERGENCY_CONTACTS)) {
            NavigationUtils.navigateToAddEmergencyContacts(this, allSocietyData);
        } else if (facilityName.equalsIgnoreCase(Constants.MAINTAINENCE_DUES)) {
            NavigationUtils.navigateToMaintainenceDuesHistory(this);
        }
    }

    private void configureResidentDirectory(String url) {
        if (Utils.isInternetAvailable(getApplicationContext())) {
            showProgressDialog("Please wait while we are configuring the directory", false);
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            Log.v("anshul", "configureResidentDirectory response " + response);
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if (baseModel.isStatus()) {
                                allSocietyData = AllSocietyDataParser.parseAllSocietyData(response);
                                PreferenceManager.getInstance(SocietyFacilitiesActivity.this).
                                        writeToPrefs(PreferenceManager.RESIDENT_DIRECTORY, response);
                            }
                            Utils.displayToast(SocietyFacilitiesActivity.this, baseModel.getMessage());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Utils.displayToast(getApplicationContext(), getString(R.string.error_server));
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

            };
            queue.add(postRequest);
        } else {
            Utils.displayToast(getApplicationContext(), getString(R.string.internet_error));
        }
    }

    private void configureAppData(String url) {
        if (Utils.isInternetAvailable(getApplicationContext())) {
            showProgressDialog("Please wait while we are configuring the app", false);
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            Log.v("anshul", "the all data response " + response);
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if (baseModel.isStatus()) {
                                mRecyclerView.setVisibility(View.VISIBLE);
                                allSocietyData = AllSocietyDataParser.parseAllSocietyData(response);
                            } else {
                                mRecyclerView.setVisibility(View.GONE);
                            }
                            Utils.displayToast(SocietyFacilitiesActivity.this, baseModel.getMessage());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Utils.displayToast(getApplicationContext(), getString(R.string.error_server));
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

            };
            queue.add(postRequest);
        } else {
            Utils.displayToast(getApplicationContext(), getString(R.string.internet_error));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSOSSms();
                }
                return;
            }
        }
    }

    private void checkForSendSmsPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.SEND_SMS)) {
                Snackbar.make(findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .SEND_SMS},
                                        Constants.REQUEST_PERMISSIONS);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .SEND_SMS},
                        Constants.REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want
            sendSOSSms();
        }
    }

    @Override
    public void updateUIOnActivity() {
        triggerAlert();
        checkForSendSmsPermission();
    }

    private void triggerAlert() {
        if (Utils.isInternetAvailable(getApplicationContext())) {

            String url = Constants.BASE_URL + (Utils.getLoggedInMode(this).equalsIgnoreCase(Constants.MODE_RESIDENT) ? Constants.RESIDENT_ALERT : Constants.ENTRY_ALERT);
            Log.v("anshul", "the url alert " + url);
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("anshul", "the all data response " + response);
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if (baseModel.isStatus()) {
                                showAlertDialogNotified();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Utils.displayToast(getApplicationContext(), getString(R.string.error_server));
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

            };
            queue.add(postRequest);
        }
    }

    private void sendSOSSms() {
        String emergencyNumbers = PreferenceManager.getInstance(this).getString(PreferenceManager.EMERGENCY_NUMBERS
                , "");
        if (!TextUtils.isEmpty(emergencyNumbers)) {
            if (emergencyNumbers.contains(",")) {
                String[] numSet = emergencyNumbers.split(",");
                if (numSet != null && numSet.length > 0) {
                    for (String num : numSet) {
                        Log.v("anshul", "the num is " + num);
                        if (!TextUtils.isEmpty(num)) {
                            sendSms(num);
                        }

                    }
                }
            } else {
                sendSms(emergencyNumbers);
            }
        } else {
            Utils.displayToast(SocietyFacilitiesActivity.this, "Please add SOS contacts");
            // NavigationUtils.navigateToAddEmergencyContacts(this);
        }


    }

    private void sendSms(String number) {
       /* String messageToSend = "I need some help. It's an emergency";
        SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null, null);*/
    }

    @Override
    protected void onStop() {
        if (PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE, "")
                .equalsIgnoreCase(Constants.MODE_RESIDENT)) {
            unregisterShakeListener();
        }
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        if (PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE)
                .equalsIgnoreCase(Constants.MODE_RESIDENT)) {
            registerShakeListener();
            registerLocationListener();
        }

        super.onResume();
    }

    private void registerLocationListener() {
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                // makeUseOfNewLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    @Override
    public void updateLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public void unregisterShakeListener() {
        if (sensorManager != null && sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
            ((SensorManager) getSystemService(Context.SENSOR_SERVICE)).unregisterListener(shakeSensor);
    }

    public void registerShakeListener() {
        SensorManager sensorManager;
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null && sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
            sensorManager.registerListener(shakeSensor, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    sensorManager.SENSOR_DELAY_NORMAL);
    }
}
