package com.android.society.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.core.deps.guava.collect.Iterables;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Contact;
import com.android.society.Model.Resident;
import com.android.society.Model.Visitor;
import com.android.society.Model.VisitorDetails;
import com.android.society.R;
import com.android.society.dialog.ExpectedVisitorDialog;
import com.android.society.request.VolleyMultipartRequest;
import com.android.society.utils.Keys;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.society.widget.CircularImageView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

public class ResidentVisitorEntryActivity extends BaseActivity implements View.OnClickListener{

    ArrayList<String> SPINNERLIST = new ArrayList<>();
    ArrayList<String> SPINNERLIST_FLAT = new ArrayList<>();

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_SIGNATURE_CAPTURE = 2;
    static final int REQUEST_GALLERY_CAPTURE = 3;

    private Button mButtonCreateVistor;
    private TextView mTextVistorName;
    private TextView mTextVistorVehicle;
    private TextView mTextVistorNumber;
    private String towerName ;
    private AllSocietyData allSocietyData ;
    private String flatName;
    private MaterialBetterSpinner materialDesignSpinner1;
    private MaterialBetterSpinner materialDesignSpinner;
    private MaterialBetterSpinner materialDesignSpinnerPurpose;
    private MaterialBetterSpinner materialDesignSpinnerVehicleType;
    private Resident resident;
    private String purpose;
    private boolean isFromExpected;
    private ImageView imgContact;
    private VisitorDetails visitorDetails;

    private Button mBtnDate;
    private EditText edtDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String expectedVisitorId = "";
    private CircularImageView circularImageView;
    private Bitmap visitorImageBitmap;

    private EditText etTower,etFlat;
    private String vehicleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resident_visitor_entry);
        takePhotoFromCamera();
        if(savedInstanceState != null){
            towerName = savedInstanceState.getString("tower");
            isFromExpected = savedInstanceState.getBoolean("is_from_expected");
            allSocietyData = savedInstanceState.getParcelable("all_data");
            visitorDetails = savedInstanceState.getParcelable("visitor_details");
        }else{
            if(getIntent() != null && getIntent().getExtras() != null){
                towerName = getIntent().getStringExtra("tower");
                allSocietyData = getIntent().getParcelableExtra("all_data");
                isFromExpected = getIntent().getBooleanExtra("is_from_expected",false);
                visitorDetails = getIntent().getParcelableExtra("visitor_details");
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.VISITOR_ENTRY);

        initViews();
        if(isFromExpected && PreferenceManager.getInstance(ResidentVisitorEntryActivity.this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RESIDENT)){
            imgContact.setVisibility(View.VISIBLE);
            materialDesignSpinner1.setVisibility(View.GONE);
            materialDesignSpinner.setVisibility(View.GONE);
        }else if(isFromExpected && PreferenceManager.getInstance(ResidentVisitorEntryActivity.this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_GUARD)){
            imgContact.setVisibility(View.GONE);
            findViewById(R.id.root_date).setVisibility(View.GONE);
            if(visitorDetails != null){
                setVisitorPreFilledData(visitorDetails);
                materialDesignSpinner.setEnabled(false);
                materialDesignSpinner1.setEnabled(false);
                materialDesignSpinner.setClickable(false);
                materialDesignSpinner1.setClickable(false);
                materialDesignSpinner.setFocusable(false);
                materialDesignSpinner1.setFocusable(false);
            }

        } else{
            imgContact.setVisibility(View.GONE);
            mBtnDate.setVisibility(View.GONE);
            findViewById(R.id.root_date).setVisibility(View.GONE);
            if(allSocietyData != null && allSocietyData.getTowerSet() != null && allSocietyData.getTowerSet().size() >0){
                for(int i =0;i < allSocietyData.getTowerSet().size();i++){
                    String towerName = Iterables.get(allSocietyData.getTowerSet(),i);
                    if(!TextUtils.isEmpty(towerName)){
                        SPINNERLIST.add(towerName);
                    }
                }
                materialDesignSpinner1.setVisibility(View.GONE);

                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
                materialDesignSpinner.setAdapter(arrayAdapter);
                arrayAdapter.notifyDataSetChanged();

                materialDesignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                        towerName = adapterView.getItemAtPosition(pos).toString();
                        materialDesignSpinner1.setVisibility(View.VISIBLE);
                        flatName = "";
                        SPINNERLIST_FLAT = new ArrayList<String>();
                        if(allSocietyData != null && allSocietyData.getFlatSet() != null && allSocietyData.getFlatSet().size()>0){
                            for(int i =0;i < allSocietyData.getFlatSet().size();i++){
                                String flatName = Iterables.get(allSocietyData.getFlatSet(),i);
                                if(flatName.contains(towerName)){
                                    flatName = flatName.replace(towerName,"").trim();
                                    SPINNERLIST_FLAT.add(flatName);
                                }
                            }
                            ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(ResidentVisitorEntryActivity.this,
                                    android.R.layout.simple_dropdown_item_1line, SPINNERLIST_FLAT);
                            materialDesignSpinner1.setAdapter(arrayAdapter1);
                            materialDesignSpinner1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    flatName = adapterView.getItemAtPosition(i).toString();
                                    resident = allSocietyData.getResidentHashMap().get(towerName + " " + flatName);
                                }
                            });
                        }

                    }
                });
            }

        }

        ArrayAdapter<String> purposeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line,getResources().getStringArray(R.array.purpose));
        materialDesignSpinnerPurpose.setAdapter(purposeAdapter);
        purposeAdapter.notifyDataSetChanged();
        materialDesignSpinnerPurpose.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                purpose = adapterView.getItemAtPosition(i).toString();
            }
        });

        ArrayAdapter<String> vehicleTypeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line,getResources().getStringArray(R.array.vehicle_type));
        materialDesignSpinnerVehicleType.setAdapter(vehicleTypeAdapter);
        vehicleTypeAdapter.notifyDataSetChanged();
        materialDesignSpinnerVehicleType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                vehicleType = adapterView.getItemAtPosition(i).toString();
            }
        });


    }

    private void setVisitorPreFilledData(VisitorDetails visitorDetails){
        mTextVistorName.setText(visitorDetails.getVisitorName());
        mTextVistorNumber.setText(visitorDetails.getVisitorNumber());
        if(!TextUtils.isEmpty(visitorDetails.getVehicleName())){
            mTextVistorVehicle.setText(visitorDetails.getVehicleName());
        }
        if(!TextUtils.isEmpty(visitorDetails.getFlatVisited())){
            String[] str = visitorDetails.getFlatVisited().split("-");
            if(str.length == 2){
                towerName = str[0];
                flatName = str[1];
                materialDesignSpinner.setText(str[0]);
                materialDesignSpinner1.setText(str[1]);
            }
        }
        if(!TextUtils.isEmpty(visitorDetails.getPurpose())){
            purpose = visitorDetails.getPurpose();
            materialDesignSpinnerPurpose.setText(visitorDetails.getPurpose());
        }

        if(!TextUtils.isEmpty(visitorDetails.getVehicleName())){
            materialDesignSpinnerVehicleType.setText(visitorDetails.getVehicleName());
        }

        if(!TextUtils.isEmpty(visitorDetails.getExpectedVisitorId())){
            expectedVisitorId = visitorDetails.getExpectedVisitorId();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews(){
        materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.spinner_tower);
        materialDesignSpinner1 = (MaterialBetterSpinner)
                findViewById(R.id.spinner_flat);
        materialDesignSpinnerPurpose = (MaterialBetterSpinner)
                findViewById(R.id.spinner_purpose);
        materialDesignSpinnerVehicleType = findViewById(R.id.spinner_type);
        mButtonCreateVistor = (Button)findViewById(R.id.create_visitor);
        mButtonCreateVistor.setOnClickListener(this);
        mTextVistorName = (TextView)findViewById(R.id.text_visitor_name);
        mTextVistorVehicle = (TextView)findViewById(R.id.text_vehicle_name);
        mTextVistorNumber = (TextView)findViewById(R.id.text_mobile);
        imgContact = (ImageView)findViewById(R.id.img_contact);
        circularImageView = (CircularImageView)findViewById(R.id.visitor_pic);
        edtDate = (EditText) findViewById(R.id.in_date);
        mBtnDate = (Button) findViewById(R.id.btn_date);
        imgContact.setOnClickListener(this);
        mBtnDate.setOnClickListener(this);
        circularImageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.visitor_pic:
                showPictureDialog();
                break;
            case R.id.create_visitor:

                if(isVisitorEntryValid() || isProgressDialogShowing()){
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(visitorImageBitmap, 150, 150, false);
                    resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    String imageEncodedData = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String vehicle = TextUtils.isEmpty(mTextVistorVehicle.getText().toString()) ? "N/A" : mTextVistorVehicle.getText().toString();
                    if(isFromExpected && PreferenceManager.getInstance(ResidentVisitorEntryActivity.this).
                            getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RESIDENT)){
                        makeExpectedVisitorEntryOnServer();
                    }else{
                        Log.v("anshul","bitmap imageEncodedData " + imageEncodedData);
                        makeVisitorEntry(vehicle,imageEncodedData);
                        makeVisitorEntryOnServer();
                    }
                }
                break;

            case R.id.img_contact:
                checkForReadPermission();
                break;

            case R.id.btn_date:
                showDatePickerDialog();
                break;
        }

    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void takePhotoFromCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkIfAlreadyhavePermission()) {
                dispatchTakePictureIntent();
            } else {
                ActivityCompat.requestPermissions(this, new
                        String[]{Manifest.permission.CAMERA}, 1);
            }
        } else {
            dispatchTakePictureIntent();
        }

    }

    private void choosePhotoFromGallary(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkIfAlreadyhaveGalleryPermission()) {
                dispatchTakeGalleryIntent();
            } else {
                ActivityCompat.requestPermissions(this, new
                        String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
            }
        } else {
            dispatchTakeGalleryIntent();
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkIfAlreadyhaveGalleryPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;

            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakeGalleryIntent();
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;
            case Constants.REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openContacts();
                }
                return;
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void dispatchTakeGalleryIntent() {
        Intent takePictureIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);;
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_GALLERY_CAPTURE);
        }
    }

    private void showDatePickerDialog(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        edtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 4 && data != null) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                    String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    String num = "";
                    String name = "";
                    if (Integer.valueOf(hasNumber) == 1) {
                        Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (numbers.moveToNext()) {
                            num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            name = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            Contact contact = new Contact();
                            contact.setName(name);
                            contact.setNumber(num);
                            if(num.startsWith("+91")){
                                num = num.replace("+91","");
                                num = num.trim();
                            }
                            mTextVistorName.setText(name);
                            mTextVistorNumber.setText(num);
                            break;
                        }
                    }
                }
            }
        }else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if(extras != null){
                visitorImageBitmap = (Bitmap) extras.get("data");
                if(visitorImageBitmap != null){
                    try {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        visitorImageBitmap = Bitmap.createScaledBitmap(visitorImageBitmap, 150, 150, false);
                        visitorImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        stream.flush();
                        stream.close();
                        circularImageView.setImageBitmap(visitorImageBitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK && null != data) {

            Uri URI = data.getData();
            String[] FILE = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(URI,
                    FILE, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(FILE[0]);
            String ImageDecode = cursor.getString(columnIndex);
            cursor.close();
            visitorImageBitmap = BitmapFactory.decodeFile(ImageDecode);
            if(visitorImageBitmap != null){
                try {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    visitorImageBitmap = Bitmap.createScaledBitmap(visitorImageBitmap, 150, 150, false);
                    visitorImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    stream.flush();
                    stream.close();
                    circularImageView.setImageBitmap(visitorImageBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkForReadPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_CONTACTS)) {
                Snackbar.make(findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_CONTACTS},
                                        Constants.REQUEST_PERMISSIONS);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_CONTACTS},
                        Constants.REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want
            openContacts();
        }
    }

    private void openContacts(){
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, 4);

    }

    private void showExpectedVistorConfrmedDialog(String jsonBody){
        FragmentManager fm = getSupportFragmentManager();
        ExpectedVisitorDialog expectedVisitorDialog = new ExpectedVisitorDialog ();
        expectedVisitorDialog.setJsonBody(jsonBody);
        expectedVisitorDialog.setCancelable(true);
        expectedVisitorDialog.show(fm, "Sample Fragment");

    }

    private void makeExpectedVisitorEntryOnServer(){
        if(Utils.isInternetAvailable(this)){
            try {
                String url = Constants.BASE_URL + Constants.CREATE_RESIDENT_EXPECTED_VISITOR_ENTRY_URL ;
                showProgressDialog("Registering", false);
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(this, Request.Method.POST, url, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        removeProgressDialog();
                        Utils.displayToast(ResidentVisitorEntryActivity.this,"Something is wrong, Try again");
                        error.printStackTrace();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(Keys.NAME,mTextVistorName.getText().toString().trim());
                        params.put(Keys.PHONE, mTextVistorNumber.getText().toString().trim());
                        params.put(Keys.REASON, purpose);
                        params.put(Keys.VEHICLE, mTextVistorVehicle.getText().toString().trim());
                        params.put(Keys.TYPE, vehicleType);
                        params.put(Keys.WHEN,edtDate.getText().toString().trim());
                        params.put(Keys.IDENTIFICATION, PreferenceManager.
                                getInstance(ResidentVisitorEntryActivity.this).getString(PreferenceManager.ID));
                        params.put(Keys.ID, expectedVisitorId);
                        return params;
                    }

                    @Override
                    protected Map<String, DataPart> getByteData() {
                        Map<String, DataPart> params = new HashMap<>();
                        // file name could found file base or direct access from real path
                        // for now just get bitmap data from ImageView
                        params.put(Keys.IMAGE, new DataPart(Keys.IMAGE, Utils.getFileDataFromDrawable(getBaseContext(), new BitmapDrawable(visitorImageBitmap)), "image/jpeg"));

                        Log.d("RESPONSE","Shit-"+ new DataPart(Keys.IMAGE, Utils.getFileDataFromDrawable(getBaseContext(), new BitmapDrawable(visitorImageBitmap)), "image/jpeg").toString());
                        return params;
                    }

                    @Override
                    public void handleReponse(BaseModel response) {
                        removeProgressDialog();
                        Utils.displayToast(ResidentVisitorEntryActivity.this,response.getMessage());
                        showExpectedVistorConfrmedDialog(getParams().toString());

                    }
                };
                queue.add(multipartRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void makeVisitorEntryOnServer(){
        String vehicle = TextUtils.isEmpty(mTextVistorVehicle.getText().toString()) ? "N/A" : mTextVistorVehicle.getText().toString();
        if(Utils.isInternetAvailable(this)){
            String time = Utils.getCurrentDate()+ " " +Utils.getCurrentTime();
            try {
                time = URLEncoder.encode(time,"utf-8");
                String url = Constants.BASE_URL + Constants.CREATE_VISITOR_ENTRY_URL ;
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(this,Request.Method.POST, url, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(Keys.NAME,mTextVistorName.getText().toString().trim());
                        params.put(Keys.PHONE, mTextVistorNumber.getText().toString().trim());
                        params.put(Keys.REASON, purpose);
                        params.put(Keys.VEHICLE, mTextVistorVehicle.getText().toString().trim());
                        params.put(Keys.TYPE, vehicleType);
                        params.put(Keys.ADDRESS1,towerName);
                        params.put(Keys.ADDRESS2,flatName.trim());
                        params.put(Keys.WHEN,edtDate.getText().toString().trim());
                        params.put(Keys.EXPECTED_VISITOR_ID, expectedVisitorId);
                        return params;
                    }

                    @Override
                    protected Map<String, DataPart> getByteData() {
                        Map<String, DataPart> params = new HashMap<>();
                        // file name could found file base or direct access from real path
                        // for now just get bitmap data from ImageView
                        params.put(Keys.IMAGE, new DataPart(Keys.IMAGE, Utils.getFileDataFromDrawable(getBaseContext(), new BitmapDrawable(visitorImageBitmap)), "image/jpeg"));

                        return params;
                    }

                    @Override
                    public void handleReponse(BaseModel response) {
                    }
                };
                queue.add(multipartRequest);








            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    }

    private void makeVisitorEntry(String vehicle,String imageEncodedData){

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Visitor visitor = realm.createObject(Visitor.class);
        visitor.setVisitorName(mTextVistorName.getText().toString());
        visitor.setVehicleName(vehicle);
        visitor.setFlatVisited(towerName + " - " + flatName);
        visitor.setPurpose(purpose);
        visitor.setExited(false);
        visitor.setVisitorNumber(mTextVistorNumber.getText().toString());
        visitor.setDate(Utils.getCurrentDate());
        visitor.setTime(Utils.getCurrentTime());
        visitor.setVisitorLocalImage(imageEncodedData);
        realm.commitTransaction();
        Log.v("anshul","bitmap entry made ");
        finish();
    }

    @Override
    protected void onDestroy() {
        Realm.getDefaultInstance().close();
        super.onDestroy();
    }

    private boolean isVisitorEntryValid(){
        if (TextUtils.isEmpty(mTextVistorName.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter visitor name", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(mTextVistorNumber.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter visitor number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isFromExpected && TextUtils.isEmpty(towerName)) {
            Toast.makeText(getApplicationContext(), "Please select Tower", Toast.LENGTH_SHORT).show();
            return false;
        }else if (!isFromExpected && TextUtils.isEmpty(flatName)) {
            Toast.makeText(getApplicationContext(), "Please select Flat", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(purpose)) {
            Toast.makeText(getApplicationContext(), "Please select Purpose for visit", Toast.LENGTH_SHORT).show();
            return false;
        }else if (isFromExpected && TextUtils.isEmpty(edtDate.getText().toString().trim()) &&
                Utils.getLoggedInMode(this).equalsIgnoreCase(Constants.MODE_RESIDENT)) {
            Toast.makeText(getApplicationContext(), "Please enter date for visit", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}


