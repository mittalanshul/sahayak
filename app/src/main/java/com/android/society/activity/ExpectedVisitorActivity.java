package com.android.society.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.society.BuildConfig;
import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;
import com.android.society.Model.VisitorDetails;
import com.android.society.R;
import com.android.society.adapter.VisitorExpectedListAdapter;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;
import com.android.society.dialog.VisitorInfoDialog;
import com.android.society.parser.BaseModelParser;
import com.android.society.parser.VisitorDetailParser;
import com.android.society.parser.VisitorDetailsParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.integration.android.IntentIntegrator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

public class ExpectedVisitorActivity extends BaseActivity implements OnVisitorInteractionCallbacks{
    private RecyclerView mRecyclerView;
    private VisitorExpectedListAdapter visitorExpectedListAdapter;
    private ArrayList<Visitor> visitors;
    private ImageView imageView;

    private BarcodeDetector detector;
    private Uri imageUri;
    private ImageView mImgBarcode;
    private static final int REQUEST_WRITE_PERMISSION = 20;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String SAVED_INSTANCE_URI = "uri";
    private static final String SAVED_INSTANCE_RESULT = "result";
    private static final String LOG_TAG = "Barcode Scanner API";
    private static final int PHOTO_REQUEST = 10;
    private RelativeLayout mRelBarcode;
    private EditText mEditBarcode;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            imageUri = Uri.parse(savedInstanceState.getString(SAVED_INSTANCE_URI));
            //scanResults.setText(savedInstanceState.getString(SAVED_INSTANCE_RESULT));
        }
        setContentView(R.layout.activity_expected_visitor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.EXPECTED_VISITOR);

        initViews();
        setListeners();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavigationUtils.navigateToVisitorEntry(ExpectedVisitorActivity.this,null,true);
            }
        });

       if(PreferenceManager.getInstance(this).getString
               (PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RESIDENT)){
           mRelBarcode.setVisibility(View.GONE);
           setTextWatcher();
           fab.setVisibility(View.VISIBLE);
       }else{
           mRelBarcode.setVisibility(View.VISIBLE);
           setUpDetector();
           setTextWatcher();
           fab.setVisibility(View.GONE);
       }

       if(PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE,"")
               .equalsIgnoreCase(Constants.MODE_RESIDENT)){
           triggerCallToGetVisitorData();
       }else{
          triggerCallToGetGuardVisitorData();
       }
    }

    private void setUpDetector(){
        detector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();
        if (!detector.isOperational()) {
            Log.v("anshul" ,"could not set up ");
            return;
        }
    }

    private void setListeners(){
        mImgBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkIfAlreadyhavePermission()) {
                        new IntentIntegrator(ExpectedVisitorActivity.this).initiateScan();
                    } else {
                        ActivityCompat.requestPermissions(ExpectedVisitorActivity.this, new
                                String[]{Manifest.permission.CAMERA}, 1);
                    }
                } else {
                    new IntentIntegrator(ExpectedVisitorActivity.this).initiateScan();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTextWatcher(){
        mEditBarcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               if(!TextUtils.isEmpty(s.toString()) && visitors != null && visitors.size()>0){
                  ArrayList<Visitor> tempList = null;
                  tempList = getFilteredList(s.toString());
                  if(tempList != null && tempList.size()>0){
                      if(visitorExpectedListAdapter != null){
                          visitorExpectedListAdapter.setVisitorsList(tempList);
                          visitorExpectedListAdapter.notifyDataSetChanged();
                      }
                  }
               }else{
                   if(visitorExpectedListAdapter != null){
                       visitorExpectedListAdapter.setVisitorsList(visitors);
                       visitorExpectedListAdapter.notifyDataSetChanged();
                   }
               }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private ArrayList<Visitor> getFilteredList(String query){
        ArrayList<Visitor> tempList = new ArrayList<>();
        if(visitors != null && visitors.size() > 0){
            for(Visitor visitor : visitors){
                if((!TextUtils.isEmpty(visitor.getVisitorName()) && visitor.getVisitorName().contains(query)) ||
                        (!TextUtils.isEmpty(visitor.getVisitorNumber()) && visitor.getVisitorNumber().contains(query)) ||
                (!TextUtils.isEmpty(visitor.getFlatVisited()) && visitor.getFlatVisited().contains(query))
                        || (!TextUtils.isEmpty(visitor.getPurpose()) && visitor.getPurpose().contains(query) )||
                        (!TextUtils.isEmpty(visitor.getVehicleName()) && visitor.getVehicleName().contains(query))){
                    tempList.add(visitor);
                }
            }
        }
        return  tempList;
    }

    private void initViews(){
           mRecyclerView = (RecyclerView)findViewById(R.id.visitor_recyclerview);
           imageView = (ImageView)findViewById(R.id.img_qr_code);
           mImgBarcode = (ImageView)findViewById(R.id.img_barcode);
          mRelBarcode = (RelativeLayout)findViewById(R.id.rel_barcode);
          mEditBarcode = (EditText)findViewById(R.id.edt_visitor_name);
        }

        private void setRecyclerViewData() {
            visitorExpectedListAdapter = new VisitorExpectedListAdapter(visitors,this);
            visitorExpectedListAdapter.setVisitorsList(visitors);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            visitorExpectedListAdapter.setOnVisitorInteractionCallbacks(this);
            mRecyclerView.setAdapter(visitorExpectedListAdapter);
        }

    private void triggerCallToGetGuardVisitorData(){
        showProgressDialog("Please wait .. Fetching your Expected Visitors",false);
        String url = Constants.BASE_URL + Constants.GUARD_EXPECTED_VISITOR_ENTRY_URL;
        Log.v("anshul","the triggerCallToGetGuardVisitorData " + url);
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        removeProgressDialog();
                        BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                        Log.v("anshul","the response is triggerCallToGetGuardVisitorData " + response);
                        Utils.displayToast(ExpectedVisitorActivity.this,baseModel.getMessage());
                        if(baseModel.isStatus()){
                            visitors = VisitorDetailParser.getVisitorDetails(response);
                            if(visitors != null && visitors.size() >0){
                                setRecyclerViewData();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        removeProgressDialog();
                        Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Utils.getHeaders(getApplicationContext());
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



        };
        queue.add(postRequest);
    }



        private void triggerCallToGetVisitorData(){
            showProgressDialog("Please wait .. Fetching your Expected Visitors",false);
            String url = Constants.BASE_URL + Constants.CREATE_RESIDENT_EXPECTED_VISITOR_ENTRY_URL;
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            Log.v("anshul","the response is triggerCallToGetVisitorData " + response);
                            Utils.displayToast(ExpectedVisitorActivity.this,baseModel.getMessage());
                            if(baseModel.isStatus()){
                                visitors = VisitorDetailParser.getVisitorDetails(response);
                                if(visitors != null && visitors.size() >0){
                                    setRecyclerViewData();
                                }
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }



            };
            queue.add(postRequest);
        }


    @Override
    public void onVisitorExited(Visitor visitor) {
            if(visitor != null && !TextUtils.isEmpty(visitor.getId())){
               triggerCallToRemoveVisitor(visitor);
            }
    }

    private void triggerCallToRemoveVisitor(final Visitor visitor){
            showProgressDialog("Please wait .. Removing your Expected Vistors",false);
            String url = Constants.BASE_URL + Constants.CREATE_RESIDENT_EXPECTED_VISITOR_ENTRY_URL + "?id="+visitor.getId();
            Log.v("anshul","triggerCallToRemoveVisitor --->>>> " + url);
            StringRequest postRequest = new StringRequest(Request.Method.DELETE, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            Log.v("anshul","the response is triggerCallToGetVisitorData " + response);
                            Utils.displayToast(ExpectedVisitorActivity.this,baseModel.getMessage());
                            if(baseModel.isStatus()){
                                visitors.remove(visitor);
                                visitorExpectedListAdapter.setVisitorsList(visitors);
                                visitorExpectedListAdapter.notifyDataSetChanged();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }



            };
            queue.add(postRequest);
        }


    @Override
    public void onVisitorClicked(Visitor visitor) {
        if(PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_GUARD)){
            VisitorDetails visitorDetails = VisitorDetailsParser.getVisitorDetails(visitor);
            if(visitorDetails != null){
                NavigationUtils.navigateToVisitorEntryFromBarcode(this,visitorDetails,true);
            }
        }else {
            FragmentManager fm = getSupportFragmentManager();
            VisitorInfoDialog visitorInfoDialog = new VisitorInfoDialog ();
            visitorInfoDialog.setVisitorData(visitor);
            visitorInfoDialog.setCancelable(true);
            visitorInfoDialog.show(fm, "Sample Fragment");
        }

    }

    @Override
    public void onResidentClicked(ResidentIn residentIn) {

    }

    @Override
    public void onResidentCallMake(ResidentIn residentIn) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if(data != null){
                String code = data.getStringExtra("SCAN_RESULT");
                Log.v("anshul", code);
                    if(!TextUtils.isEmpty(code)){
                        Log.v("anshul","finally results " + code);
                        VisitorDetails visitorDetails = VisitorDetailsParser.getVisitorDetails(code);
                        Log.v("anshul","finally visitorDetails " + visitorDetails);
                        if(visitorDetails != null){
                            Log.v("anshul","finally navigation ");
                            NavigationUtils.navigateToVisitorEntryFromBarcode(this,visitorDetails,true);
                        }

                    }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new IntentIntegrator(ExpectedVisitorActivity.this).initiateScan();
                } else {
                    Toast.makeText(ExpectedVisitorActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }



    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "picture.jpg");
        imageUri = FileProvider.getUriForFile(ExpectedVisitorActivity.this,
                BuildConfig.APPLICATION_ID + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PHOTO_REQUEST);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (imageUri != null) {
            outState.putString(SAVED_INSTANCE_URI, imageUri.toString());
           // outState.putString(SAVED_INSTANCE_RESULT, scanResults.getText().toString());
        }
        super.onSaveInstanceState(outState);
    }

    private void launchMediaScanIntent() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private Bitmap decodeBitmapUri(Context ctx, Uri uri) throws FileNotFoundException {
        int targetW = 600;
        int targetH = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeStream(ctx.getContentResolver()
                .openInputStream(uri), null, bmOptions);
    }
}

