/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.Staff;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.adapter.SelectDomesticHelpAdapter;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import io.realm.Realm;

public class AttendenceActivity extends BaseActivity implements View.OnClickListener {

    private String type;

    private MaterialBetterSpinner materialDesignSpinnerType;
    String[] TYPE_LIST = {Constants.MAID, Constants.CAR_CLEANER};

    private Button mButtonCreateAttendence;
    private AllSocietyData allSocietyData;

    private EditText etIdNumber;
    private RecyclerView recyclerView;
    private SelectDomesticHelpAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence);
        if (savedInstanceState != null) {
            allSocietyData = savedInstanceState.getParcelable("all_data");
        } else {
            if (getIntent() != null && getIntent().getExtras() != null) {
                allSocietyData = getIntent().getParcelableExtra("all_data");
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.ATTENDENCE);

        materialDesignSpinnerType = findViewById(R.id.spinner_type);
        recyclerView = findViewById(R.id.rc);
        etIdNumber = findViewById(R.id.et_id_number);
        mButtonCreateAttendence = (Button) findViewById(R.id.create_attendence);
        mButtonCreateAttendence.setOnClickListener(this);


        adapter = new SelectDomesticHelpAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        materialDesignSpinnerType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                type = adapterView.getItemAtPosition(i).toString();
                ArrayList<StaffDetails> sourceList = type.equalsIgnoreCase(Constants.MAID) ? allSocietyData.getMaidArrayList() : allSocietyData.getCarCleanerList();
                if (sourceList != null) {
                    ArrayList<StaffDetails> serviceList = (ArrayList<StaffDetails>) sourceList.clone();
                    Collections.copy(serviceList, sourceList);
                    adapter.setStaffDetailsArrayList(serviceList);
                    adapter.setSelectedStaff(null);
                    adapter.notifyDataSetChanged();
                }

            }
        });
      /*  materialDesignSpinnerType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });*/
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, TYPE_LIST);
        materialDesignSpinnerType.setAdapter(arrayAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_attendence:
                String id = getAndCheckStaffId();
                if (id != null) {
                    makeStaffEntry(id);
                }
                break;
        }
    }

    private void makeAttendenceEntryOnServer(String id) {
        if (Utils.isInternetAvailable(this)) {
            String url = Constants.BASE_URL + Constants.CREATE_STAFF_ENTRY_URL;
            url = url + "?id=" + id + "&entry=" + Utils.getCurrentDate() + Utils.getCurrentTime();
            showProgressDialog("Marking Attendance", false);
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            finish();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

            };
            queue.add(postRequest);
        }
    }


    private void makeStaffEntry(String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        /*Staff staffExists = realm.where(Staff.class)
                .equalTo("date", Utils.getCurrentDate())
                .findFirst();
        if(staffExists == null){*/
        Staff staff = realm.createObject(Staff.class);
        staff.setStaffName(adapter.getSelectedStaff().getStaffName());
        staff.setStaffId(adapter.getSelectedStaff().getStaffId());
        staff.setStaffType(type);
        staff.setStaffNumber(adapter.getSelectedStaff().getStaffNumber());
        staff.setExited(false);
        staff.setDate(Utils.getCurrentDate());
        staff.setTime(Utils.getCurrentTime());
        realm.commitTransaction();

        makeAttendenceEntryOnServer(id);
        Utils.displayToast(this, "ATTENDANCE MARKED");

    }


    private String getAndCheckStaffId() {
        if (!TextUtils.isEmpty(etIdNumber.getText())) {
            return etIdNumber.getText().toString();
        }

        if (TextUtils.isEmpty(type)) {
            Toast.makeText(getApplicationContext(), "Please select Type", Toast.LENGTH_SHORT).show();
            return null;
        }
        if (adapter.getSelectedStaff() == null) {
            Toast.makeText(getApplicationContext(), "SELECT STAFF", Toast.LENGTH_SHORT).show();
            return null;
        }
        return adapter.getSelectedStaff().getStaffId();
    }
}
