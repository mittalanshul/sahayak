/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Config;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ad4screen.sdk.activities.A4SActivity;
import com.android.society.Constants.Constants;
import com.android.society.R;
import com.android.society.receiver.NotificationBroadcastReciever;
import com.android.society.utils.Utils;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import java.util.Map;
import java.util.logging.Logger;

public class BaseActivity extends AppCompatActivity {
    public RequestQueue queue;
    public ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        queue = Volley.newRequestQueue(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("anshul","onMessageReceived : called onResume receiver");
    }

    /**
     * Utility function for displaying progress dialog
     *
     * @param bodyText    message to be shown
     * @param cancellable set true if you want to hide this dialog on back press
     */

    public void showProgressDialog(String bodyText, boolean cancellable) {
        try {
            if (Utils.isInternetAvailable(this)) {
                if (mProgressDialog == null) {
                    mProgressDialog = new ProgressDialog(BaseActivity.this);
                    mProgressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.custom_spinner_for_progressbar));
                    mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    mProgressDialog.setCancelable(cancellable);
                    mProgressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                            return (keyCode == KeyEvent.KEYCODE_CAMERA || keyCode == KeyEvent.KEYCODE_SEARCH);
                        }
                    });
                }

                mProgressDialog.setMessage(bodyText);

                if (!mProgressDialog.isShowing()) {
                    mProgressDialog.show();
                }
            }
        } catch (Exception e) {

        }
    }

    public boolean isProgressDialogShowing() {
        if (mProgressDialog != null) {
            return mProgressDialog.isShowing();
        }
        return false;
    }

    /**
     * Utility function to remove progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
        }
    }

    public void removeProgressDialog(View rootView) {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}
