/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.society.Constants.Constants;
import com.android.society.R;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.Utils;

public class KnowYourSocietyLogin extends AppCompatActivity implements View.OnClickListener {

    private Button enterAsRWAAdmin ;
    private Button enterAsResident ;
    private Button enterAsGateGuard ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Utils.isUserLoggedIn(this)){
            NavigationUtils.navigateToSocietyFacilities(this);
            finish();
        }

        setContentView(R.layout.activity_know_your_society_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Welcome");


        enterAsRWAAdmin = (Button)findViewById(R.id.btn_enter_as_rwa);
        enterAsResident = (Button)findViewById(R.id.btn_enter_as_resident);
        enterAsGateGuard = (Button)findViewById(R.id.btn_enter_as_guard);

        enterAsRWAAdmin.setOnClickListener(this);
        enterAsResident.setOnClickListener(this);
        enterAsGateGuard.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_enter_as_rwa:
                NavigationUtils.navigateToLogin(KnowYourSocietyLogin.this, Constants.MODE_RWA);
                break;

            case R.id.btn_enter_as_resident:
                NavigationUtils.navigateToLogin(KnowYourSocietyLogin.this, Constants.MODE_RESIDENT);
                break;

            case R.id.btn_enter_as_guard:
                NavigationUtils.navigateToLogin(KnowYourSocietyLogin.this, Constants.MODE_GUARD);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1&& resultCode == RESULT_OK){
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
