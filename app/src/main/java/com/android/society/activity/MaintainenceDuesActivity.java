package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.MaintainenceDues;
import com.android.society.R;
import com.android.society.parser.AllResidentMaintainenceParser;
import com.android.society.parser.AllResidentMaintainenceParserByID;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.w3c.dom.Text;

import java.util.Map;

public class MaintainenceDuesActivity extends BaseActivity implements View.OnClickListener{

    private TextView mTxtMaintainenceDues;
    private TextView mTxtPenalty;
    private TextView mTxtPreviousDues;
    private TextView mTxtTotal;

    private MaintainenceDues maintainenceDues;

    private LinearLayout mLinearPay;
    private TextView mTextPay;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintainence_dues);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Maintainence Dues");
        initViews();
        setOnClickListeners();

        if(getIntent() != null && getIntent().getExtras() != null){
            maintainenceDues = getIntent().getParcelableExtra("main_id");
            if(maintainenceDues !=  null){
                setData();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void triggerCalltoGetAllMaintainanceById(String id){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Fetching Maintainance Details",false);
            String url = Constants.BASE_URL + Constants.RESIDENT_ALL_MAINTAINENCE_BY_ID + id;
            StringRequest postRequest = new StringRequest(Request.Method.GET,url ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Log.v("anshul","onResponse" + response);
                                maintainenceDues = AllResidentMaintainenceParserByID.getMaintainenceDuesById(response);
                                if(maintainenceDues != null){
                                    setData();
                                }
                            }
                            Utils.displayToast(MaintainenceDuesActivity.this,baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    private void setOnClickListeners(){
        mLinearPay.setOnClickListener(this);
        mTextPay.setOnClickListener(this);
    }

    private void initViews(){
        mTxtMaintainenceDues = (TextView)findViewById(R.id.txt_maintainence);
        mTxtPenalty = (TextView)findViewById(R.id.txt_penalty);
        mTxtPreviousDues = (TextView)findViewById(R.id.txt_previous_dues);
        mTxtTotal = (TextView)findViewById(R.id.txt_total);
        mLinearPay = (LinearLayout)findViewById(R.id.linear_click);
        mTextPay = (TextView)findViewById(R.id.btn_continue);
    }

    private void setData(){
        if(maintainenceDues != null){
            mTxtMaintainenceDues.setText(getString(R.string.rupee_symbol) + maintainenceDues.getCurrentMaintainence());
            mTxtPenalty.setText(getString(R.string.rupee_symbol) + maintainenceDues.getPenalty());
            mTxtPreviousDues.setText(getString(R.string.rupee_symbol) + maintainenceDues.getPreviousDues());
            mTxtTotal.setText(getString(R.string.rupee_symbol) + maintainenceDues.getTotal());
            if(maintainenceDues.getStatus().equalsIgnoreCase("Pending")){
                mTextPay.setVisibility(View.VISIBLE);
                mTextPay.setText("PAY  " + getString(R.string.rupee_symbol) + maintainenceDues.getTotal());
            }else{
                mTextPay.setVisibility(View.GONE);
            }


        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.linear_click:
            case R.id.btn_continue:
                triggerCallForPayment();
                break;
        }
    }

    public void triggerCallForPayment(){

    }


}
