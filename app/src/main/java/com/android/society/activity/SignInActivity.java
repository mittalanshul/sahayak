/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.society.R;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;


public class SignInActivity extends BaseActivity implements View.OnClickListener {


    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    // UI references.
    private EditText mMobileView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mButtonSendOTP;
    private TextView mTextLogin;

    private EditText mTextSocietyName;
    private EditText mTextRegisterNo;
    private EditText mTextEmail;
    private EditText mTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        // Set up the login form.
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("SIGN IN");

        mMobileView = (EditText) findViewById(R.id.mobile);
        mLoginFormView = findViewById(R.id.mobile_login_form);
        mProgressView = findViewById(R.id.login_progress);

        mTextSocietyName = (EditText) findViewById(R.id.text_society_name);
        mTextRegisterNo = (EditText) findViewById(R.id.rwa_register_no);
        mTextEmail = (EditText) findViewById(R.id.email);
        mTextPassword = (EditText) findViewById(R.id.password);


        mButtonSendOTP = (Button) findViewById(R.id.send_otp);
        mButtonSendOTP.setOnClickListener(this);
        mTextLogin = (TextView)findViewById(R.id.text_login);
        mTextLogin.setOnClickListener(this);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_login:
                //NavigationUtils.navigateToLogin(getApplicationContext());
                finish();
                break;

            case R.id.send_otp:
                if(isDataEntryValid()){
                    PreferenceManager mPreferenceManager = PreferenceManager.getInstance(getApplicationContext());
                    mPreferenceManager.writeToPrefs(PreferenceManager.USER_EMAIL,mTextEmail.getText().toString().trim());
                    mPreferenceManager.writeToPrefs(PreferenceManager.USER_NAME,mTextSocietyName.getText().toString().trim());
                    mPreferenceManager.writeToPrefs(PreferenceManager.USER_REGISTER_NUMBER,mTextRegisterNo.getText().toString().trim());
                    mPreferenceManager.writeToPrefs(PreferenceManager.USER_NUMBER,mMobileView.getText().toString().trim());
                    mPreferenceManager.writeToPrefs(PreferenceManager.IS_LOGGED_IN,true);
                    NavigationUtils.navigateToSocietyFacilities(this);
                    finish();
                }
                break;
        }
    }

    private boolean isDataEntryValid(){
        if(TextUtils.isEmpty(mTextSocietyName.getText().toString().trim())){
            Utils.displayToast(this,"Please enter society name");
            return  false;
        } else if(TextUtils.isEmpty(mTextRegisterNo.getText().toString().trim())){
            Utils.displayToast(this,"Please enter registeration number");
            return  false;
        } else if(TextUtils.isEmpty(mTextEmail.getText().toString().trim())){
            Utils.displayToast(this,"Please enter email id");
            return  false;
        } else if(TextUtils.isEmpty(mMobileView.getText().toString().trim())){
            Utils.displayToast(this,"Please enter number");
            return  false;
        }else if(TextUtils.isEmpty(mTextPassword.getText().toString().trim())){
            Utils.displayToast(this,"Please enter password");
            return  false;
        }
        return  true;
    }

    private boolean isNumberValid(String mobile) {
        if (TextUtils.isEmpty(mobile)) {
            boolean cancel = false;
            View focusView = null;
            mMobileView.setError(getString(R.string.error_invalid_password));
            focusView = mMobileView;
            cancel = true;


            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            }
            return false;
        }
        return true;
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

        }

        @Override
        protected void onCancelled() {

        }
    }
}


