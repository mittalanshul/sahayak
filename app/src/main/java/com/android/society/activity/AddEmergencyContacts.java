package com.android.society.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.Child;
import com.android.society.Model.Contact;
import com.android.society.R;
import com.android.society.adapter.EmergencyContactsRecyclerView;
import com.android.society.callbacks.OnNeighbourAddedCallback;
import com.android.society.callbacks.SmsListener;
import com.android.society.receiver.SMSReceiver;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;

import java.util.ArrayList;

public class AddEmergencyContacts extends BaseActivity implements  View.OnClickListener , OnNeighbourAddedCallback {
    private RecyclerView mRecyclerView;
    private EmergencyContactsRecyclerView emergencyContactsRecyclerView;
    private ArrayList<Contact> contactArrayList;
    private TextView mTextNeigh1;
    private TextView mTextNeigh2;
    private View viewClicked;
    private AllSocietyData allSocietyData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergency_contacts_activity);
        if(savedInstanceState != null){
            allSocietyData = savedInstanceState.getParcelable("all_data");
        }else{
            if(getIntent() != null && getIntent().getExtras() != null){
                allSocietyData = getIntent().getParcelableExtra("all_data");
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Emergency Contacts");
        initViews();
        setUpListeners();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkForReadPermission();
            }
        });

        contactArrayList = findPreviousConfigContacts();
        if(contactArrayList != null && contactArrayList.size() >0){
            setRecyclerviewData();
        }
        setUpNeighbours();

    }

    private void setUpNeighbours(){
        String neighbour1 = PreferenceManager.getInstance(this).getString("neigh_1","");
        if(!TextUtils.isEmpty(neighbour1)){
            mTextNeigh1.setText("NEIGHBOUR 1 : " + neighbour1);
        }
        String neighbour2 = PreferenceManager.getInstance(this).getString("neigh_2","");
        if(!TextUtils.isEmpty(neighbour2)){
            mTextNeigh2.setText("NEIGHBOUR 2 : " + neighbour2);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpListeners(){
        mTextNeigh1.setOnClickListener(this);
        mTextNeigh2.setOnClickListener(this);
    }

    private void setRecyclerviewData(){
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        emergencyContactsRecyclerView = new EmergencyContactsRecyclerView(contactArrayList,this);
        mRecyclerView.setAdapter(emergencyContactsRecyclerView);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)findViewById(R.id.contact_recyclerview);
        mTextNeigh1 = (TextView)findViewById(R.id.text_neigh_1);
        mTextNeigh2 = (TextView)findViewById(R.id.text_neigh_2);
        contactArrayList = new ArrayList<>();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 4 && data != null) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                    String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    String num = "";
                    String name = "";
                    if (Integer.valueOf(hasNumber) == 1) {
                        Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (numbers.moveToNext()) {
                            num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            name = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            Contact contact = new Contact();
                            contact.setName(name);
                            contact.setNumber(num);
                            if (contactArrayList == null) {
                                contactArrayList = new ArrayList<>();
                            }
                            String emergencyNumbers = PreferenceManager.getInstance(this).getString(PreferenceManager.EMERGENCY_NUMBERS
                                    ,"");
                            String emergencyNames = PreferenceManager.getInstance(this).getString(PreferenceManager.EMERGENCY_NAMES
                                    ,"");
                            if(TextUtils.isEmpty(emergencyNames)){
                                emergencyNames = name;
                            }else{
                                emergencyNames = emergencyNames + "," +name;
                            }
                            if(TextUtils.isEmpty(emergencyNumbers)){
                                emergencyNumbers = num;
                            }else{
                                emergencyNumbers = emergencyNumbers + "," +num;
                            }


                            PreferenceManager.getInstance(this).writeToPrefs(PreferenceManager.EMERGENCY_NUMBERS,emergencyNumbers);
                            PreferenceManager.getInstance(this).writeToPrefs(PreferenceManager.EMERGENCY_NAMES,emergencyNames);
                                contactArrayList.add(contact);
                                setRecyclerviewData();
                                break;
                        }
                    }
                }
            }
        } else if(resultCode == Activity.RESULT_OK && requestCode == 43){
            String residentName = data.getStringExtra("neighbour_name");
            onNeighbourAdded(residentName);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private ArrayList<Contact> findPreviousConfigContacts(){
        ArrayList<Contact> contactArrayList = new ArrayList<>();
        String emergencyNumbers = PreferenceManager.getInstance(this).getString(PreferenceManager.EMERGENCY_NUMBERS
                ,"");
        String emergencyNames = PreferenceManager.getInstance(this).getString(PreferenceManager.EMERGENCY_NAMES
                ,"");
        if(!TextUtils.isEmpty(emergencyNumbers) && !TextUtils.isEmpty(emergencyNames)){
            if(emergencyNumbers.contains(",") && emergencyNames.contains(",")){
                String[] numSet = emergencyNumbers.split(",");
                String[] namesSet = emergencyNames.split(",");
                if(numSet != null && numSet.length > 1 && namesSet != null && namesSet.length >1){
                    Log.v("anshul","emergencyNumbers is " + emergencyNumbers);
                    Log.v("anshul","emergencyNames is " + emergencyNames);
                    for(int i =0 ; i < numSet.length ; i++){
                        Contact contact = new Contact();
                        contact.setName(namesSet[i]);
                        contact.setNumber(numSet[i]);
                        contactArrayList.add(contact);
                    }
                }
            } else{
                Contact contact = new Contact();
                contact.setName(emergencyNames);
                contact.setNumber(emergencyNumbers);
                contactArrayList.add(contact);
            }
        }
        return contactArrayList;
    }

    private void checkForReadPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_CONTACTS)) {
                Snackbar.make(findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_CONTACTS},
                                        Constants.REQUEST_PERMISSIONS);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_CONTACTS},
                        Constants.REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want
            openContacts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   openContacts();
                }
                return;
            }
        }
    }

    private void openContacts(){
        if(contactArrayList != null && contactArrayList.size() < Constants.MAX_EMERGENCY_CONACTS){
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, 4);
        }else{
            Utils.displayToast(this,"Only " + Constants.MAX_EMERGENCY_CONACTS + " can be added . Please remove one contact to add another");
        }

    }

    @Override
    public void onClick(View v) {
        viewClicked = v;
        PreferenceManager.getInstance(this).writeToPrefs("n_id",viewClicked.getId());
        NavigationUtils.navigateToResidentSection(this, allSocietyData,false , true);
    }

    @Override
    protected void onDestroy() {
        PreferenceManager.getInstance(this).remove("n_id");
        super.onDestroy();

    }

    @Override
    public void onNeighbourAdded(String flatName) {
     if(!TextUtils.isEmpty(flatName)){
         if(viewClicked.getId() == R.id.text_neigh_1){
             ((TextView)viewClicked).setText("NEIGHBOUR 1 : " + flatName);
             PreferenceManager.getInstance(this).writeToPrefs("neigh_1",flatName);
         }else if(viewClicked.getId() == R.id.text_neigh_2){
             ((TextView)viewClicked).setText("NEIGHBOUR 2 : " + flatName);
             PreferenceManager.getInstance(this).writeToPrefs("neigh_2",flatName);
         }
     }
    }
}
