package com.android.society.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.VisitorDetails;
import com.android.society.R;
import com.android.society.parser.BaseModelParser;
import com.android.society.parser.VisitorDetailsParser;
import com.android.society.utils.Utils;
import com.android.society.widget.CircularImageView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.common.StringUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.logging.Logger;

public class VisitorDetailForm extends BaseActivity implements View.OnClickListener{

    private CircularImageView visitorImage;
    private TextView textVisitorName;
    private TextView textVisitorNumber;
    private TextView textVisitorPurpose;
    private VisitorDetails visitorDetails;
    private String data ;
    private TextView textDeny;
    private TextView textApprove;
    private String type;
    private boolean isActionTakenByUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_detail_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.VISITOR_DETAIL);

        initViews();
        initClickListener();
        if(getIntent() != null && getIntent().getExtras() != null){
            data = getIntent().getStringExtra(Constants.NOTIFICATION_DATA);
            type = getIntent().getStringExtra(Constants.NOTIFICATION_TYPE);
            Log.v("anshul","the data on visitor is " + data + type);
            if(data != null){
                visitorDetails = VisitorDetailsParser.getVisitorDetails(data);
                if(visitorDetails != null){
                     setData();
                }
            }
            if(type.equalsIgnoreCase(Constants.AD_VISITOR_ACTION)){
                textDeny.setVisibility(View.GONE);
                textApprove.setVisibility(View.GONE);
            }
        }

        try{
            textApprove.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(!isActionTakenByUser){
                        onClick(textApprove);
                    }
                }
            },8000);
        }catch (Exception e){

        }
    }

    private void initClickListener(){
        textDeny.setOnClickListener(this);
        textApprove.setOnClickListener(this);
    }

    private void setData(){
        textVisitorName.setText(visitorDetails.getVisitorName());
        textVisitorNumber.setText(visitorDetails.getVisitorNumber());
        textVisitorPurpose.setText(visitorDetails.getPurpose());
        if(!TextUtils.isEmpty(visitorDetails.getImageURL())){
            String url = Constants.MEDIA_BASE_URL + visitorDetails.getImageURL();
            Picasso.with(this).load(url).placeholder(R.drawable.user_pic_def).error(R.drawable.user_pic_def)
                    .into(visitorImage);
        }

    }

    private void initViews(){
        visitorImage = (CircularImageView)findViewById(R.id.visitor_pic);
        textVisitorName = (TextView) findViewById(R.id.text_name);
        textVisitorNumber = (TextView)findViewById(R.id.text_number);
        textVisitorPurpose = (TextView)findViewById(R.id.text_purpose);
        textDeny = (TextView)findViewById(R.id.btn_deny);
        textApprove = (TextView)findViewById(R.id.btn_approve);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        isActionTakenByUser = true;
        switch (v.getId()){
            case R.id.btn_deny:
                triggerCallForExitAction("no");
                break;
            case R.id.btn_approve:
                triggerCallForExitAction("yes");
                break;
        }
    }
    private void triggerCallForExitAction(final String action){
        showProgressDialog("Please wait .. Sending your response",false);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("answer",action);
            jsonBody.put("token",visitorDetails.getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();
        String url = Constants.BASE_URL + Constants.RESIDENT_VISITOR_ALLOW;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        removeProgressDialog();
                        BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                        if(baseModel.isStatus()){
                            String approval = "Approval";
                            if(action.equalsIgnoreCase("no")){
                                approval = "Denial";
                            }
                            Utils.displayToast(VisitorDetailForm.this,"Guard is notified for the " + approval);
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        removeProgressDialog();
                        Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Utils.getHeaders(getApplicationContext());
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(postRequest);
    }
}
