package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Child;
import com.android.society.Model.ChildExitAction;
import com.android.society.R;
import com.android.society.parser.BaseModelParser;
import com.android.society.parser.ChildExitStatusParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

public class ChildExitActionActivity extends BaseActivity implements View.OnClickListener{

    private TextView textApprove;
    private String data;
    private TextView textChildLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_exit_action);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        initViews();
        initClickListener();

        if(getIntent() != null && getIntent().getExtras() != null){
            data = getIntent().getStringExtra(Constants.NOTIFICATION_DATA);
            if(!TextUtils.isEmpty(data)){
                ChildExitAction childExitAction = ChildExitStatusParser.parseExitActionData(data);
                if(childExitAction != null){
                    setData(childExitAction);
                }
            }
        }


    }

    private void setData(ChildExitAction childExitAction){
        if(childExitAction.getAnswer().equalsIgnoreCase("yes")){
            textChildLabel.setText("Hi Guard ," + childExitAction.getAddress1() + " has allowed child " + childExitAction.getName()
            + " to exit from the society");
            textApprove.setBackground(getDrawable(R.drawable.cta_button));
        }else{
            textChildLabel.setText("Hi Guard ," + childExitAction.getAddress1() + " has denied child " + childExitAction.getName()
                    + " to exit from the society");
            textApprove.setBackground(getDrawable(R.drawable.cta_red));
        }

    }

    private void initViews(){
        textApprove = (TextView)findViewById(R.id.btn_approve);
        textChildLabel = (TextView)findViewById(R.id.child_alert_label);
    }

    private void initClickListener(){
        textApprove.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_approve:
                finish();
               // triggerCallForExitAction("yes",data);
                break;
        }
    }

    private void triggerCallForExitAction(final String action , String childName){
        showProgressDialog("Please wait .. Sending your response",false);
        String url = Constants.BASE_URL + Constants.CHILD_EXIT_ACTION+"?answer="+action+"&name="+childName;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        removeProgressDialog();
                        BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                        if(baseModel.isStatus()){
                            String approval = "Approval";
                            if(action.equalsIgnoreCase("no")){
                                approval = "Denial";
                            }
                            Utils.displayToast(ChildExitActionActivity.this,"Guard is notified for the " +approval);
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        removeProgressDialog();
                        Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Utils.getHeaders(getApplicationContext());
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



        };
        queue.add(postRequest);
    }
}

