/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.ServiceDetail;
import com.android.society.Model.StaffDetails;
import com.android.society.Model.User;
import com.android.society.R;
import com.android.society.callbacks.OnServiceAdded;
import com.android.society.dialog.AddServiceDialog;
import com.android.society.dialog.ResidentInfoDialog;
import com.android.society.parser.AuthorityLoginParser;
import com.android.society.parser.ServiceDetailParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

public class AddServicesActivity extends BaseActivity implements OnServiceAdded ,View.OnClickListener{
    private ServiceDetail serviceDetail;
    private Button mButtonAddService;
    private Button mButtonCarCleaner;
    private TextView textMaidTitle;
    private TextView textCarCleanerTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_services);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.ADD_SERVICES);
        initViews();
        setonClickListeners();
        configureServices();



    }

    private void setonClickListeners(){
        mButtonAddService.setOnClickListener(this);
        mButtonCarCleaner.setOnClickListener(this);
    }

    private void initViews(){
        mButtonAddService = (Button)findViewById(R.id.add_service);
        mButtonCarCleaner = (Button)findViewById(R.id.add_service_cleaner);
        textMaidTitle = (TextView) findViewById(R.id.text_maid_title);
        textCarCleanerTitle = (TextView) findViewById(R.id.text_cleaner_title);
    }

    private void addService(final StaffDetails staffDetails){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Adding " + staffDetails.getStaffType(),false);
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("isAdd",true);
                jsonBody.put("id",staffDetails.getStaffId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String mRequestBody = jsonBody.toString();
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL
                    + Constants.GET_ALL_RESIDENT_SERVICE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            Log.v("anshul","onResponse" + response);
                            Utils.displayToast(getApplicationContext(),staffDetails.getStaffType() +"Added successfully");
                            if(staffDetails.getStaffType().equalsIgnoreCase(Constants.MAID)){
                                refreshMaidViews(staffDetails);
                            }else if(staffDetails.getStaffType().equalsIgnoreCase(Constants.CAR_CLEANER)){
                                refreshCarCleanerViews(staffDetails);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshMaidViews(StaffDetails staffDetails){
        textMaidTitle.setVisibility(View.GONE);
        findViewById(R.id.card_details_maid).setVisibility(View.VISIBLE);
        TextView textViewName = (TextView)findViewById(R.id.text_staff_name_maid);
        TextView textViewNumber = (TextView)findViewById(R.id.text_staff_number_maid);
        TextView textViewPurpose = (TextView)findViewById(R.id.label);
        textViewName.setText(staffDetails.getStaffName());
        textViewNumber.setText(staffDetails.getStaffNumber());
        textViewPurpose.setText(staffDetails.getStaffType().toUpperCase());
        mButtonAddService.setText(getString(R.string.change_maid));
    }

    private void refreshCarCleanerViews(StaffDetails staffDetails){
        textCarCleanerTitle.setVisibility(View.GONE);
        findViewById(R.id.card_details).setVisibility(View.VISIBLE);
        TextView textViewName = (TextView)findViewById(R.id.text_staff_name);
        TextView textViewNumber = (TextView)findViewById(R.id.text_staff_number);
        TextView textViewPurpose = (TextView)findViewById(R.id.label_cleaner);
        textViewName.setText(staffDetails.getStaffName());
        textViewNumber.setText(staffDetails.getStaffNumber());
        textViewPurpose.setText(staffDetails.getStaffType().toUpperCase());
        mButtonCarCleaner.setText(getString(R.string.change_cleaner));
    }

    private void configureServices(){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait.. configuring services",false);
            StringRequest postRequest = new StringRequest(Request.Method.GET, Constants.BASE_URL + Constants.GET_ALL_RESIDENT_SERVICE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            Log.v("anshul","onResponse" + response);
                            //serviceDetail = ServiceDetailParser.getAllServiceDetail(response);
                            if(serviceDetail.getMaidDetails() != null){
                                refreshMaidViews(serviceDetail.getMaidDetails());
                            }else{
                                findViewById(R.id.card_details_maid).setVisibility(View.GONE);
                            }

                            if(serviceDetail.getCleanerDetails() != null){
                                refreshCarCleanerViews(serviceDetail.getCleanerDetails());
                            }else{
                                findViewById(R.id.card_details).setVisibility(View.GONE);
                            }
                            Log.v("anshul","serviceDetail " +serviceDetail);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }



            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    @Override
    public void onServiceAdded(StaffDetails staffDetails) {
         if(staffDetails != null){
             Log.v("anshul","get staff " + staffDetails.getStaffName());
             addService(staffDetails);
         }
    }

    @Override
    public void onClick(View view) {
       switch (view.getId()){
           case R.id.add_service:
               if(serviceDetail != null &&
                       serviceDetail.getMaidArrayList() != null && serviceDetail.getMaidArrayList().size() >0){
                   showAddServiceDialog(serviceDetail.getMaidArrayList());
               }else{
                   Utils.displayToast(getApplicationContext(),"No Maid found to add");
               }
               break;
           case R.id.add_service_cleaner:
               if(serviceDetail != null &&
                       serviceDetail.getCarCleanerArrayList() != null && serviceDetail.getCarCleanerArrayList().size() >0){
                   showAddServiceDialog(serviceDetail.getCarCleanerArrayList());
               }else{
                   Utils.displayToast(getApplicationContext(),"No Car Cleaner found to add");
               }
               break;
       }
    }

    private void showAddServiceDialog(ArrayList<StaffDetails> staffDetailsArrayList){
        FragmentManager fm = getSupportFragmentManager();
        AddServiceDialog addServiceDialog = new AddServiceDialog ();
        addServiceDialog.setOnServiceAdded(this);
        addServiceDialog.setStaffDetailsArrayList(staffDetailsArrayList);
        addServiceDialog.setCancelable(true);
        addServiceDialog.show(fm, "Sample Fragment");
    }
}
