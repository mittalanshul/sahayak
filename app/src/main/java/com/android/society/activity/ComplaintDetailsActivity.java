/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Complaint;
import com.android.society.Model.Event;
import com.android.society.R;
import com.android.society.adapter.ComplaintEntryPagerAdapter;
import com.android.society.adapter.RecyclerComplaintsAdapter;
import com.android.society.adapter.RecyclerViewEventsAdapter;
import com.android.society.adapter.VisitorEntryPagerAdapter;
import com.android.society.callbacks.OnComplaintAdded;
import com.android.society.callbacks.onEventClicked;
import com.android.society.fragments.OpenComplaintsFragment;
import com.android.society.fragments.ResidentInFragment;
import com.android.society.fragments.ResolvedComplaintsFragment;
import com.android.society.fragments.VisitorInFragment;
import com.android.society.fragments.VisitorOutFragment;
import com.android.society.parser.AllComplaintsParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

public class ComplaintDetailsActivity extends BaseActivity implements View.OnClickListener,OnComplaintAdded {

    private RecyclerView mRecyclerView;
    private FloatingActionButton floatingActionButton;
    private RecyclerComplaintsAdapter mRecyclerViewComplaintsAdapter;
    private ArrayList<Complaint> complaintArrayList;
    ViewPager pager;
    private SearchView mSearchView;
    private TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.COMPLAINTS);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_complaint);
        ComplaintEntryPagerAdapter adapter = new ComplaintEntryPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
        createTabViews();

        if(PreferenceManager.getInstance(getApplicationContext()).getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RESIDENT)){
            floatingActionButton.setVisibility(View.VISIBLE);
            floatingActionButton.setOnClickListener(this);
            CreateComplaintActivity createEventActivity = new CreateComplaintActivity();
            createEventActivity.setOnComplaintAddedCall(this);
        }else{
            floatingActionButton.setVisibility(View.GONE);
            floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_complaint);
            floatingActionButton.setOnClickListener(this);
            CreateComplaintActivity createEventActivity = new CreateComplaintActivity();
            createEventActivity.setOnComplaintAddedCall(this);
        }

       /* mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_complaint);
        showEmptyView();*/
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager)this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (mSearchView != null) {
            mSearchView.setQueryHint("Search Complaint");
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            EditText etSearch= ((EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
            try {
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(etSearch, R.drawable.cursor);// set textCursorDrawable to null
            } catch (Exception e) {
                e.printStackTrace();
            }
            etSearch.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String query) {
                    if(!TextUtils.isEmpty(query)){
                        int index = pager.getCurrentItem();
                        ComplaintEntryPagerAdapter adapter = ((ComplaintEntryPagerAdapter)pager.getAdapter());
                        if(index == 0){
                            OpenComplaintsFragment fragment = (OpenComplaintsFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.refreshVisitorsDataOnQuery(query);
                            }
                        }else if(index == 1){
                            ResolvedComplaintsFragment fragment = (ResolvedComplaintsFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.refreshVisitorsDataOnQuery(query);
                            }
                        }
                    }else{
                        int index = pager.getCurrentItem();
                        ComplaintEntryPagerAdapter adapter = ((ComplaintEntryPagerAdapter)pager.getAdapter());
                        if(index == 0){
                            OpenComplaintsFragment fragment = (OpenComplaintsFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.loadDefaultData();
                            }
                        }else if(index == 1){
                            ResolvedComplaintsFragment fragment = (ResolvedComplaintsFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.loadDefaultData();
                            }
                        }
                    }
                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }
    public void triggerCallToDeleteComplaint(final Complaint complaint) {
        if (Utils.isInternetAvailable(getApplicationContext())) {
            showProgressDialog("Please wait .. Deleting Complaint", false);
            StringRequest postRequest = new StringRequest(Request.Method.DELETE, Constants.BASE_URL
                    + Constants.CREATE_COMPLAINT+"?id="+complaint.getId(),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                int index = pager.getCurrentItem();
                                ComplaintEntryPagerAdapter adapter = ((ComplaintEntryPagerAdapter)pager.getAdapter());
                                if(index == 0){
                                    OpenComplaintsFragment fragment = (OpenComplaintsFragment) adapter.getFragment(index);
                                    if(fragment != null){
                                        fragment.refreshComplaints(complaint);
                                    }
                                }else if(index == 1){
                                    ResolvedComplaintsFragment fragment = (ResolvedComplaintsFragment) adapter.getFragment(index);
                                    if(fragment != null){
                                        fragment.refreshComplaints(complaint);
                                    }
                                }
                            }
                            Utils.displayToast(getApplicationContext(), baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul", "onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };
            queue.add(postRequest);
        } else {
            Utils.displayToast(getApplicationContext(), "Please check your internet connection");
        }
    }

    private void createTabViews() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabOne.setText("OPEN");
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabTwo.setText("RESOLVED");
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showEmptyView() {
        if (complaintArrayList != null && complaintArrayList.size() > 0) {
            findViewById(R.id.empty_view).setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            setRecyclerViewData();
        } else {
            findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    private void setRecyclerViewData() {
        mRecyclerViewComplaintsAdapter = new RecyclerComplaintsAdapter(complaintArrayList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewComplaintsAdapter.setOnComplaintAddedCall(this);
        mRecyclerView.setAdapter(mRecyclerViewComplaintsAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
            Complaint complaint = data.getParcelableExtra("complaint");
            if (complaint != null) {
                int index = pager.getCurrentItem();
                ComplaintEntryPagerAdapter adapter = ((ComplaintEntryPagerAdapter)pager.getAdapter());
                if(index == 0){
                    OpenComplaintsFragment fragment = (OpenComplaintsFragment) adapter.getFragment(index);
                    if(fragment != null){
                        Log.v("anshul","omComplaintAdded level 1");
                        fragment.omComplaintAdded(complaint);
                    }
                }
            }
        } else if (requestCode == 1234 && resultCode == Activity.RESULT_OK) {
                int index = pager.getCurrentItem();
                ComplaintEntryPagerAdapter adapter = ((ComplaintEntryPagerAdapter)pager.getAdapter());
                if(index == 0){
                    OpenComplaintsFragment fragment = (OpenComplaintsFragment) adapter.getFragment(index);
                    if(fragment != null){
                        Log.v("anshul","omComplaintAdded level 1");
                        fragment.refreshStatus();
                    }
                }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_complaint:
                NavigationUtils.navigateToAddComplaint(ComplaintDetailsActivity.this);
                break;
        }
    }

    @Override
    public void omComplaintAdded(Complaint complaint) {
        if(complaintArrayList == null){
            complaintArrayList = new ArrayList<>();
        }
        complaintArrayList.add(0,complaint);
        showEmptyView();
    }

    @Override
    public void omComplaintDelete(Complaint complaint) {
        if(complaintArrayList != null && complaintArrayList.size()>0){
            complaintArrayList.remove(complaint);
            showEmptyView();
        }
    }

    @Override
    public void omComplaintClicked(Complaint complaint) {

    }
}
