/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.R;
import com.android.society.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class ParkingEntryActvity extends BaseActivity implements View.OnClickListener{

    String[] SPINNERLIST = {"A1","AB1","AB2","AB3","B1","B2","B3","B4","C1","C2","C3"};
    String[] SPINNERLIST_FLAT = {"001","002","003","004"};

    private Button mButtonCreateParking;
    private TextView mTextVehicleName;
    private String towerName ;
    private String flatName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_entry_actvity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mButtonCreateParking = (Button)findViewById(R.id.create_parking);
        mButtonCreateParking.setOnClickListener(this);
        mTextVehicleName = (TextView)findViewById(R.id.text_car_name);


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.spinner_tower);
        materialDesignSpinner.setAdapter(arrayAdapter);
        materialDesignSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                towerName = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST_FLAT);
        MaterialBetterSpinner materialDesignSpinner1 = (MaterialBetterSpinner)
                findViewById(R.id.spinner_flat);
        materialDesignSpinner1.setAdapter(arrayAdapter1);
        materialDesignSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                flatName = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.create_parking:

                break;
        }
    }

    private boolean isParkingEntryValid(){
        if (TextUtils.isEmpty(mTextVehicleName.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter vehicle name", Toast.LENGTH_SHORT).show();
            return false;
        }/*else if (TextUtils.isEmpty(towerName)) {
            Toast.makeText(getApplicationContext(), "Please select Tower", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(flatName)) {
            Toast.makeText(getApplicationContext(), "Please select Flat", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;
    }
}

