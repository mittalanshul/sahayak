package com.android.society.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.adapter.DomesticHelpAdapter;
import com.android.society.callbacks.MakePhoneCallListener;
import com.android.society.callbacks.OnServiceAdded;
import com.android.society.fragments.AddNewDomesticFragment;
import com.android.society.parser.ServiceDetailParser;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

public class DomesticServicesUsed extends BaseActivity implements View.OnClickListener, OnServiceAdded, MakePhoneCallListener {

    private Button mButtonAddService;
    private ArrayList<StaffDetails> staffDetailsArrayList;
    private RecyclerView mRecyclerView;
    private DomesticHelpAdapter domesticHelpAdapter;
    private String domesticNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domestic_services_used);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.ADD_SERVICES);
        initViews();
        setonClickListeners();
        configureServices();
    }

    private void configureServices() {
        if (Utils.isInternetAvailable(getApplicationContext())) {
            showProgressDialog("Please wait.. getting your services", false);
            StringRequest postRequest = new StringRequest(Request.Method.GET, Constants.BASE_URL + Constants.GET_ALL_RESIDENT_SERVICE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            Log.v("anshul", "onResponse" + response);
                            staffDetailsArrayList = ServiceDetailParser.getAllServiceDetail(response);
                            if (staffDetailsArrayList != null && staffDetailsArrayList.size() > 0) {
                                setRecyclerViewData();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul", "onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }


            };
            queue.add(postRequest);
        } else {
            Utils.displayToast(getApplicationContext(), "Please check your internet connection");
        }
    }

    private void setonClickListeners() {
        mButtonAddService.setOnClickListener(this);
    }

    private void initViews() {
        mButtonAddService = (Button) findViewById(R.id.add_service);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_domestic);
    }

    private void setRecyclerViewData() {
        domesticHelpAdapter = new DomesticHelpAdapter(staffDetailsArrayList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        domesticHelpAdapter.setMakePhoneCallListener(this);
        mRecyclerView.setAdapter(domesticHelpAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_service:
                showAddServiceDialog();
                break;
        }
    }

    private void showAddServiceDialog() {

        AddNewDomesticFragment addNewDomesticFragment = new AddNewDomesticFragment();
        addNewDomesticFragment.setRetainInstance(true);
        addNewDomesticFragment.setOnServiceAdded(this);
        addNewDomesticFragment.setStaffDetailsArrayList(staffDetailsArrayList);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                R.anim.exit_out_bottom).add(R.id.frame_domestic, addNewDomesticFragment, "domestic")
                .addToBackStack("domestic").commitAllowingStateLoss();
    }

    @Override
    public void onServiceAdded(StaffDetails staffDetails) {
        if (staffDetails != null) {
            boolean isStaffAlreadyAdded = false;
            for (StaffDetails staffDetails1 : staffDetailsArrayList) {
                if (staffDetails1.getStaffId().equalsIgnoreCase(staffDetails.getStaffId())) {
                    isStaffAlreadyAdded = true;
                }
            }
            if (!isStaffAlreadyAdded) {
                addService(staffDetails);
            } else {
                Utils.displayToast(this, "You have already added this service");
            }

        }
    }

    private void addService(final StaffDetails staffDetails) {
        if (Utils.isInternetAvailable(getApplicationContext())) {
            showProgressDialog("Please wait .. Adding " + staffDetails.getStaffType(), false);
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("isAdd", true);
                jsonBody.put("id", staffDetails.getStaffId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String mRequestBody = jsonBody.toString();
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL
                    + Constants.GET_ALL_RESIDENT_SERVICE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            Utils.displayToast(getApplicationContext(), staffDetails.getStaffType() + "Added successfully");
                            staffDetailsArrayList.add(staffDetails);
                            domesticHelpAdapter.setStaffDetailsArrayList(staffDetailsArrayList);
                            domesticHelpAdapter.notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul", "onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

            };
            queue.add(postRequest);
        } else {
            Utils.displayToast(getApplicationContext(), "Please check your internet connection");
        }
    }

    @Override
    public void onPhoneCallMake(String phoneNumber) {
        domesticNumber = phoneNumber;
        if(isPermissionGranted()){
            makeCall(domesticNumber);
        }
    }

    private void makeCall(String number){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        startActivity(callIntent);
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            makeCall(domesticNumber);
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    makeCall(domesticNumber);
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
