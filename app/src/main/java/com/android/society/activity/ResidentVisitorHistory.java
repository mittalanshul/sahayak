package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.adapter.ResidentVisitorHistoryAdapter;
import com.android.society.adapter.VisitorEntryListAdapter;
import com.android.society.callbacks.OnVisitorInteractionCallbacks;
import com.android.society.dialog.VisitorInfoDialog;
import com.android.society.parser.BaseModelParser;
import com.android.society.parser.ResidentProfileParser;
import com.android.society.parser.VisitorDetailParser;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.Map;

public class ResidentVisitorHistory extends BaseActivity implements OnVisitorInteractionCallbacks{
    private RecyclerView mRecyclerView;
    private ResidentVisitorHistoryAdapter residentVisitorHistoryAdapter;
    private ArrayList<Visitor> visitorArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resident_visitor_history);
        initViews();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("MY VISITORS");

        triggerCallToGetVisitors();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        residentVisitorHistoryAdapter = new ResidentVisitorHistoryAdapter(visitorArrayList,this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(ResidentVisitorHistory.this));
        mRecyclerView.setAdapter(residentVisitorHistoryAdapter);
        residentVisitorHistoryAdapter.setOnVisitorInteractionCallbacks(this);
    }


    private void triggerCallToGetVisitors(){
        String url = Constants.BASE_URL + Constants.RESIDENT_VISITOR_HISTORY;
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait..Fetching Visitors",false);
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                visitorArrayList = VisitorDetailParser.getVisitorDetails(response);
                                if(visitorArrayList != null && visitorArrayList.size() >0){
                                    setmRecyclerViewData();
                                }
                                Log.v("anshul","value is "+ visitorArrayList.size());
                            }
                            Utils.displayToast(ResidentVisitorHistory.this,baseModel.getMessage());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),getString(R.string.internet_error));
        }
    }

    @Override
    public void onVisitorExited(Visitor visitor) {

    }

    @Override
    public void onVisitorClicked(Visitor visitor) {
        FragmentManager fm = getSupportFragmentManager();
        VisitorInfoDialog visitorInfoDialog = new VisitorInfoDialog ();
        visitorInfoDialog.setVisitorData(visitor);
        visitorInfoDialog.setCancelable(true);
        visitorInfoDialog.show(fm, "Sample Fragment");
    }

    @Override
    public void onResidentClicked(ResidentIn residentIn) {

    }

    @Override
    public void onResidentCallMake(ResidentIn residentIn) {
        Log.v("anshul","onResidentCallMake");
    }
}
