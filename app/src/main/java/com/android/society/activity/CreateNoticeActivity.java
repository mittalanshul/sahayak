/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.R;
import com.android.society.callbacks.onEventClicked;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by 201101101 on 12/1/2017.
 */

public class CreateNoticeActivity extends BaseActivity implements View.OnClickListener {

    private EditText title;
    private EditText desc;
    private Button mCreateEvent;
    private String type;

    private MaterialBetterSpinner materialDesignSpinnerEventType;
    String[] EVENT_TYPE_LIST = {"General","Policies","Pest Control","Security","Maintainence","Other"};

    public onEventClicked getOnEventClickedCall() {
        return onEventClickedCall;
    }

    public void setOnEventClickedCall(onEventClicked onEventClickedCall) {
        this.onEventClickedCall = onEventClickedCall;
    }

    private onEventClicked onEventClickedCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create Notice");

        title = (EditText) findViewById(R.id.text_event_title);
        desc = (EditText) findViewById(R.id.edt_desc);
        mCreateEvent = (Button) findViewById(R.id.create_event);

        materialDesignSpinnerEventType = (MaterialBetterSpinner) findViewById(R.id.spinner_event_type);
        materialDesignSpinnerEventType.setHint("Select Notice Type");
        mCreateEvent.setText("CREATE NOTICE");
        mCreateEvent.setOnClickListener(this);

        findViewById(R.id.root_date).setVisibility(View.GONE);
        findViewById(R.id.root_time).setVisibility(View.GONE);

        ArrayAdapter<String> arrayAdapterName = new ArrayAdapter<String>(CreateNoticeActivity.this,
                android.R.layout.simple_dropdown_item_1line, EVENT_TYPE_LIST);
        materialDesignSpinnerEventType.setAdapter(arrayAdapterName);
        arrayAdapterName.notifyDataSetChanged();
        materialDesignSpinnerEventType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                type = adapterView.getItemAtPosition(i).toString();
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_event:
                if (isDataValid()) {
                    triggerCallToCreateEvent();

                }
                break;
        }
    }

    public void triggerCallToCreateEvent(){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Creating Event",false);
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("title",title.getText().toString().trim());
                jsonBody.put("desc",desc.getText().toString().trim());
                jsonBody.put("type",type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String mRequestBody = jsonBody.toString();
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL
                    + Constants.CREATE_NOTICE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Log.v("anshul","onResponse" + response);
                                finish();
                            }
                            Utils.displayToast(CreateNoticeActivity.this,baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    private boolean isDataValid() {
        if (TextUtils.isEmpty(title.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter title", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(type)) {
            Toast.makeText(getApplicationContext(), "Please select event type", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(desc.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter description", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}

