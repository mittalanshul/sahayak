/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Event;
import com.android.society.Model.StaffDetails;
import com.android.society.R;
import com.android.society.callbacks.onEventClicked;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Map;

public class CreateEventActivity extends BaseActivity implements View.OnClickListener {

    private EditText title;
    private EditText desc;
    private Button mCreateEvent;
    private String type;
    private MaterialBetterSpinner materialDesignSpinnerEventType;
    String[] EVENT_TYPE_LIST = {"General","Festival","Camp","Other"};

    public onEventClicked getOnEventClickedCall() {
        return onEventClickedCall;
    }

    public void setOnEventClickedCall(onEventClicked onEventClickedCall) {
        this.onEventClickedCall = onEventClickedCall;
    }

    private onEventClicked onEventClickedCall;
    private Button mBtnDate;
    private EditText edtDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button mBtnTime;
    private EditText edtTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create Event");

        title = (EditText) findViewById(R.id.text_event_title);
        desc = (EditText) findViewById(R.id.edt_desc);
        mCreateEvent = (Button) findViewById(R.id.create_event);
        mBtnDate = (Button) findViewById(R.id.btn_date);
        edtDate = (EditText) findViewById(R.id.in_date);
        mBtnTime = (Button) findViewById(R.id.btn_time);
        edtTime = (EditText) findViewById(R.id.in_time);
        materialDesignSpinnerEventType = (MaterialBetterSpinner) findViewById(R.id.spinner_event_type);
        mCreateEvent.setOnClickListener(this);
        mBtnDate.setOnClickListener(this);
        mBtnTime.setOnClickListener(this);

        ArrayAdapter<String> arrayAdapterName = new ArrayAdapter<String>(CreateEventActivity.this,
                android.R.layout.simple_dropdown_item_1line, EVENT_TYPE_LIST);
        materialDesignSpinnerEventType.setAdapter(arrayAdapterName);
        arrayAdapterName.notifyDataSetChanged();
        materialDesignSpinnerEventType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                type = adapterView.getItemAtPosition(i).toString();
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_event:
                if (isDataValid()) {
                    triggerCallToCreateEvent();

                }
                break;

            case R.id.btn_date:
                showDatePickerDialog();
                break;

            case R.id.btn_time:
                showTimePickerDialog();
                break;
        }
    }

    private void showTimePickerDialog(){
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String AM_PM ;
                        if(hourOfDay < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        edtTime.setText(hourOfDay + ":" + minute + AM_PM);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void showDatePickerDialog(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        edtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void triggerCallToCreateEvent(){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Creating Event",false);
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("title",title.getText().toString().trim());
                jsonBody.put("desc",desc.getText().toString().trim());
                jsonBody.put("type",type);
                jsonBody.put("date",edtDate.getText().toString().trim());
                jsonBody.put("time",edtTime.getText().toString().trim());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String mRequestBody = jsonBody.toString();
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL
                    + Constants.CREATE_EVENT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Log.v("anshul","onResponse" + response);
                                finish();
                            }
                            Utils.displayToast(CreateEventActivity.this,baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    private boolean isDataValid() {
        if (TextUtils.isEmpty(title.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter title", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(type)) {
            Toast.makeText(getApplicationContext(), "Please select event type", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(edtDate.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please set event date", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(edtTime.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please set event time", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(desc.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter event description", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
