/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.espresso.core.deps.guava.collect.Iterables;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Child;
import com.android.society.Model.Resident;
import com.android.society.R;
import com.android.society.adapter.ResidentTowersAdapter;
import com.android.society.callbacks.OnResidentTowerSelected;
import com.android.society.dialog.ChildNamesDialog;
import com.android.society.dialog.ResidentDirectoryInfoDialog;
import com.android.society.dialog.ResidentInfoDialog;
import com.android.society.parser.AllChildDetailsParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Logger;

public class ResidentFlatListActivity extends BaseActivity implements OnResidentTowerSelected{
    private String towerName;
    private ResidentTowersAdapter residentTowersAdapter;
    private RecyclerView mRecyclerView;
    private AllSocietyData allSocietyData ;
    private String flatSelected;
    private boolean isFromChildSafety;
    private boolean isFromPanic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resident_flat_list);

        if(savedInstanceState != null){
            towerName = savedInstanceState.getString("tower");
            allSocietyData = savedInstanceState.getParcelable("all_data");
        }else{
            if(getIntent() != null && getIntent().getExtras() != null){
                towerName = getIntent().getStringExtra("tower");
                allSocietyData = getIntent().getParcelableExtra("all_data");
                isFromChildSafety = getIntent().getBooleanExtra("child_safety",false);
                isFromPanic = getIntent().getBooleanExtra("panic",false);
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(towerName + " Flats");

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view_resident);
        showEmptyView();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showEmptyView(){
        if(!TextUtils.isEmpty(towerName)){
            findViewById(R.id.empty_view).setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            setRecyclerViewData();
        } else {
            findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }



    private void setRecyclerViewData(){
        TreeSet<String> flatSet = new TreeSet<>();
        for(int i =0;i < allSocietyData.getFlatSet().size();i++){
            String flatName = Iterables.get(allSocietyData.getFlatSet(),i);
            if(flatName.contains(towerName)){
                flatName = flatName.replace(towerName,"").trim();
                flatSet.add(flatName);
            }
        }
        residentTowersAdapter = new ResidentTowersAdapter(flatSet);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        residentTowersAdapter.setOnResidentTowerSelected(this);
        mRecyclerView.setAdapter(residentTowersAdapter);
    }

    @Override
    public void onResidentTowerSelected(String towerName) {
        flatSelected = towerName;
        Resident resident = allSocietyData.getResidentHashMap().get(this.towerName + " "  + flatSelected);
        if(resident != null && isFromPanic){
            try{
                JSONObject jsonObject = new JSONObject();
                int id = PreferenceManager.getInstance(this).getInt("n_id",-1);
                if(id != -1 && id == R.id.text_neigh_1){
                    jsonObject.put("emergency1",resident.getId());
                }else if(id != -1 && id == R.id.text_neigh_2){
                    jsonObject.put("emergency2",resident.getId());
                }
                triggerCallToUploadResidentData(jsonObject.toString());
            }catch (JSONException e){

            }
        }else if(resident != null){
            if(isFromChildSafety){
                triggerCallToGetAllChild(resident);
            }else if(Utils.getLoggedInMode(this).equalsIgnoreCase(Constants.MODE_GUARD)){
                NavigationUtils.navigateToResidentProfile(this,resident);
            }else  if(Utils.getLoggedInMode(this).equalsIgnoreCase(Constants.MODE_RESIDENT)){
                FragmentManager fm = getSupportFragmentManager();
                ResidentDirectoryInfoDialog residentInfoDialog = new ResidentDirectoryInfoDialog ();
                residentInfoDialog.setResidentInData(resident);
                residentInfoDialog.setCancelable(true);
                residentInfoDialog.show(fm, "Sample Fragment");
            }
        }
    }

    private void triggerCallToUploadResidentData(final String requestBody){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait..Fetching data",false);
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL + Constants.RESIDENT_UPDATE_PROFILE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Intent intent = new Intent();
                                intent.putExtra("neighbour_name",towerName + " "  + flatSelected);
                                setResult(Activity.RESULT_OK , intent);
                                finish();
                            }
                            Utils.displayToast(ResidentFlatListActivity.this, baseModel.getMessage());



                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    private void showChildDialog(ArrayList<Child> children,String residentId){
        FragmentManager fm = getSupportFragmentManager();
        ChildNamesDialog childNamesDialog = new ChildNamesDialog ();
        childNamesDialog.setChildArrayList(children);
        childNamesDialog.setResidentId(residentId);
        childNamesDialog.setCancelable(true);
        childNamesDialog.show(fm, "Sample Fragment");
    }

    private void triggerCallToGetAllChild(final Resident resident){
        showProgressDialog("Please wait .. Getting children attached",false);
        String url = Constants.BASE_URL + Constants.GET_ALL_CHILD+"?address1="+resident.getAddress1()
                +"&address2="+resident.getAddress2();
        Log.v("anshul","the url is "+ url);
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("anshul","the response is "+ response);
                        removeProgressDialog();
                        BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                        if(baseModel.isStatus()){
                            ArrayList<Child> childArrayList = AllChildDetailsParser.getAllChildDetails(response);
                            if(childArrayList != null && childArrayList.size()>0){
                                showChildDialog(childArrayList,resident.getId());
                            }else{
                                Utils.displayToast(ResidentFlatListActivity.this,"No Children found");
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        removeProgressDialog();
                        Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Utils.getHeaders(getApplicationContext());
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



        };
        queue.add(postRequest);
    }

}
