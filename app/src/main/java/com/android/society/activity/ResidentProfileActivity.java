/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.Resident;
import com.android.society.Model.User;
import com.android.society.R;
import com.android.society.callbacks.ResidentProfileCallbacks;
import com.android.society.fragments.EnterOTPFragment;
import com.android.society.fragments.ResidentProfileFragment;
import com.android.society.parser.AllSocietyDataParser;
import com.android.society.parser.AuthorityLoginParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.parser.ResidentDataParser;
import com.android.society.parser.ResidentProfileParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.PreferenceManager;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class ResidentProfileActivity extends BaseActivity implements View.OnClickListener,ResidentProfileCallbacks {

    private Button mResidentProfile;

    private Resident resident;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_GALLERY_CAPTURE = 3;
    private  Bitmap cameraImageBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resident_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Profile");

        initViews();
        setOnClickListener();

        if (PreferenceManager.getInstance(this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_RWA)) {
           // makeFieldsNonEditable();
            if(getIntent() != null && getIntent().getExtras() != null){
                resident = (Resident) getIntent().getSerializableExtra("resident");
                if(resident != null){
                    String url = Constants.BASE_URL + Constants.RESIDENT_PROFILE_URL + resident.getId();
                    triggerCallForResidentProfile(url);
                }
            }
        } else{
            String residentId = PreferenceManager.getInstance(getApplicationContext()).getString(PreferenceManager.IDENTIFICATION);
            String url = Constants.BASE_URL + Constants.ONLY_RESIDENT_URL;
            triggerCallForResidentProfile(url);
        }

        Utils.hideKeyboard(this);
    }

    private void showResidentProfileFragment(){
        ResidentProfileFragment residentProfileFragment = new ResidentProfileFragment();
        residentProfileFragment.setRetainInstance(true);
        residentProfileFragment.setmResidentProfileCallbacks(this);
       Bundle bundle = new Bundle();
        bundle.putSerializable("resident", resident);
        residentProfileFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                R.anim.exit_out_bottom).add(R.id.frame_login, residentProfileFragment, "rpofile")
                .addToBackStack("profile").commitAllowingStateLoss();
    }

    private void triggerCallForResidentProfile(String url){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait..Fetching Profile",false);
            Log.v("anshul","profile url is " + url);

            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            Log.v("anshul","triggerCallForResidentProfile " + response);
                            resident = ResidentProfileParser.residentParser(response,getApplicationContext());
                            if(resident != null){
                                Utils.saveResidentData(getApplicationContext(),resident);
                                //setResidentData(resident);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),getString(R.string.internet_error));
        }
    }



    private void initViews() {
        mResidentProfile = (Button) findViewById(R.id.btn_profile);
    }

    private void setOnClickListener() {
        mResidentProfile.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_profile:
                showResidentProfileFragment();
                break;
        }
    }

    private void triggerCallToUploadResidentData(final String requestBody){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait..updating your profile",false);
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL + Constants.RESIDENT_UPDATE_PROFILE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            Utils.displayToast(ResidentProfileActivity.this, baseModel.getMessage());
                            FragmentManager fm = getSupportFragmentManager();
                            for(int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
                                fm.popBackStack();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Log.v("anshul","onErrorResponse" + error.getLocalizedMessage());
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    private void triggerImageFromCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkIfAlreadyhavePermission()) {
                dispatchTakePictureIntent();
            } else {
                ActivityCompat.requestPermissions(ResidentProfileActivity.this, new
                        String[]{Manifest.permission.CAMERA}, 1);
            }
        } else {
            dispatchTakePictureIntent();
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkIfAlreadyhaveGalleryPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                } else {
                    Toast.makeText(ResidentProfileActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;

            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakeGalleryIntent();
                } else {
                    Toast.makeText(ResidentProfileActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void dispatchTakeGalleryIntent() {
        Intent takePictureIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);;
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_GALLERY_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if(extras != null){
                cameraImageBitmap = (Bitmap) extras.get("data");
                if(cameraImageBitmap != null){
                    try {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        cameraImageBitmap = Bitmap.createScaledBitmap(cameraImageBitmap, 150, 150, false);
                        cameraImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        stream.flush();
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK && null != data) {

            Uri URI = data.getData();
            String[] FILE = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(URI,
                    FILE, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(FILE[0]);
            String ImageDecode = cursor.getString(columnIndex);
            cursor.close();
            cameraImageBitmap = BitmapFactory.decodeFile(ImageDecode);
            if(cameraImageBitmap != null){
                try {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    cameraImageBitmap = Bitmap.createScaledBitmap(cameraImageBitmap, 150, 150, false);
                    cameraImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    stream.flush();
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void uploadResidentProfile(String requestbody) {
        triggerCallToUploadResidentData(requestbody);
    }
}
