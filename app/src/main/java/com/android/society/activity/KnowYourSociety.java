/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;

import com.ad4screen.sdk.A4SApplication;
import com.android.society.R;
import com.android.society.service.TestService;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import io.fabric.sdk.android.Fabric;
import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;

/**
 * Created by 201101101 on 10/18/2017.
 */

public class KnowYourSociety extends Application {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .migration(new RealmMigration() {
                    @Override
                    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
                        if(oldVersion == 0){
                            RealmObjectSchema visitorSchema = Realm.getDefaultInstance().getSchema().get("Visitor");

                            // Combine 'firstName' and 'lastName' in a new field called 'fullName'
                            visitorSchema.addField("id", String.class, FieldAttribute.REQUIRED)
                                    .transform(new RealmObjectSchema.Function() {
                                        @Override
                                        public void apply(DynamicRealmObject obj) {
                                        }
                                    });
                            oldVersion++;
                        }
                    }
                }).schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(config);
        sAnalytics = GoogleAnalytics.getInstance(this);
        getDefaultTracker();


    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }


    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }
}
