package com.android.society.activity;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import com.android.society.Constants.Constants;
import com.android.society.Model.BaseModel;
import com.android.society.Model.MaintainenceDues;
import com.android.society.R;
import com.android.society.adapter.AllMaintainanceAdapter;
import com.android.society.callbacks.MaintainanceCallbacks;
import com.android.society.parser.AllResidentMaintainenceParser;
import com.android.society.parser.BaseModelParser;
import com.android.society.utils.NavigationUtils;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.Map;

public class ResidentMaintainenceHistoryActivity extends BaseActivity implements MaintainanceCallbacks{

    private RecyclerView mRecyclerView;
    private AllMaintainanceAdapter allMaintainanceAdapter;
    private ArrayList<MaintainenceDues> maintainenceDuesArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resident_maintainence_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Maintainance History");
        initViews();
        triggerCalltoGetAllMaintainance();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void triggerCalltoGetAllMaintainance(){
        if(Utils.isInternetAvailable(getApplicationContext())){
            showProgressDialog("Please wait .. Fetching Maintainance History",false);
            String url = Constants.BASE_URL + Constants.RESIDENT_ALL_MAINTAINENCE;
            StringRequest postRequest = new StringRequest(Request.Method.GET,url ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if(baseModel.isStatus()){
                                Log.v("anshul","onResponse" + response);
                                maintainenceDuesArrayList = AllResidentMaintainenceParser.getMaintainenceDues(response);
                                if(maintainenceDuesArrayList != null && maintainenceDuesArrayList.size() >0){
                                    setmRecyclerViewData();
                                }
                            }
                            Utils.displayToast(ResidentMaintainenceHistoryActivity.this,baseModel.getMessage());

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

            };
            queue.add(postRequest);
        }else{
            Utils.displayToast(getApplicationContext(),"Please check your internet connection");
        }
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)findViewById(R.id.main_recyclerview);
    }

    private void setmRecyclerViewData(){
        allMaintainanceAdapter = new AllMaintainanceAdapter(maintainenceDuesArrayList,this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(allMaintainanceAdapter);
        allMaintainanceAdapter.setMaintainanceCallbacks(this);
    }

    @Override
    public void onMaintainanceClicked(String id) {
      if(!TextUtils.isEmpty(id)){
          NavigationUtils.navigateToMaintainenceDuesById(this,null,id);
      }
    }

    @Override
    public void onMaintainanceClicked(MaintainenceDues maintainenceDues) {
        if(maintainenceDues != null){
            NavigationUtils.navigateToMaintainence(this,maintainenceDues);
        }
    }
}
