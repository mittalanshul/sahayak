/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.core.deps.guava.collect.Iterables;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.society.Constants.Constants;
import com.android.society.Model.AllSocietyData;
import com.android.society.Model.Resident;
import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;
import com.android.society.R;
import com.android.society.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

import io.realm.Realm;

public class ResidentEntryActivity extends BaseActivity implements View.OnClickListener {

    ArrayList<String> SPINNERLIST = new ArrayList<>();
    ArrayList<String> SPINNERLIST_FLAT = new ArrayList<>();

    private Button mButtonCreateVistor;
    private TextView mTextVistorName;
    private TextView mTextVistorVehicle;
    private TextView mTextVistorNumber;
    private TextView mTextVistorFlat;
    private TextView mTextVistorParkingSlot;
    private String towerName ;
    private AllSocietyData allSocietyData ;
    private String flatName;
    private MaterialBetterSpinner materialDesignSpinner1;
    private MaterialBetterSpinner materialDesignSpinner;
    private MaterialBetterSpinner materialDesignSpinnerPurpose;
    private Resident resident;
    private String purpose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resident_entry);
        initViews();
        if(savedInstanceState != null){
            towerName = savedInstanceState.getString("tower");
            allSocietyData = savedInstanceState.getParcelable("all_data");
        }else{
            if(getIntent() != null && getIntent().getExtras() != null){
                towerName = getIntent().getStringExtra("tower");
                allSocietyData = getIntent().getParcelableExtra("all_data");
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.RESIDENT_ENTRY);

        mButtonCreateVistor = (Button)findViewById(R.id.create_visitor);
        mButtonCreateVistor.setOnClickListener(this);

        mTextVistorName = (TextView)findViewById(R.id.text_resident_name);
        mTextVistorVehicle = (TextView)findViewById(R.id.text_resident_vehicle);
        mTextVistorNumber = (TextView)findViewById(R.id.text_resident_number);
        mTextVistorFlat = (TextView)findViewById(R.id.text_resident_flat);
        mTextVistorParkingSlot = (TextView)findViewById(R.id.text_resident_parking_slot);

        if(allSocietyData != null && allSocietyData.getTowerSet().size() >0){
            for(int i =0;i < allSocietyData.getTowerSet().size();i++){
                String towerName = Iterables.get(allSocietyData.getTowerSet(),i);
                if(!TextUtils.isEmpty(towerName)){
                    SPINNERLIST.add(towerName);
                }
            }

            materialDesignSpinner1.setVisibility(View.GONE);

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
            materialDesignSpinner.setAdapter(arrayAdapter);
            arrayAdapter.notifyDataSetChanged();
            materialDesignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                    towerName = adapterView.getItemAtPosition(pos).toString();
                    materialDesignSpinner1.setVisibility(View.VISIBLE);
                    flatName = "";
                    SPINNERLIST_FLAT = new ArrayList<String>();
                    for(int i =0;i < allSocietyData.getFlatSet().size();i++){
                        String flatName = Iterables.get(allSocietyData.getFlatSet(),i);
                        if(flatName.contains(towerName)){
                            flatName = flatName.replace(towerName,"").trim();
                            SPINNERLIST_FLAT.add(flatName);
                        }
                    }
                    ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(ResidentEntryActivity.this,
                            android.R.layout.simple_dropdown_item_1line, SPINNERLIST_FLAT);
                    materialDesignSpinner1.setAdapter(arrayAdapter1);
                    materialDesignSpinner1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            flatName = adapterView.getItemAtPosition(i).toString();
                            resident = allSocietyData.getResidentHashMap().get(towerName + " " + flatName);
                            if(resident != null){
                                showCardDetails();
                            }

                        }
                    });
                }
            });

        }






    }

    private void showCardDetails(){
        findViewById(R.id.card_details).setVisibility(View.VISIBLE);
        mTextVistorName.setText(resident.getName());
        try {
            mTextVistorNumber.setVisibility(View.VISIBLE);
            String maskedNumber = Utils.maskString(resident.getPhone1(),0,
                    resident.getPhone1().length()-2,'*');
            mTextVistorNumber.setText(maskedNumber);
        } catch (Exception e) {
            mTextVistorNumber.setVisibility(View.GONE);
            e.printStackTrace();
        }
        mTextVistorVehicle.setText("HR 26 CZ 6479");
        mTextVistorFlat.setText(towerName + "-" + flatName);
       mTextVistorParkingSlot.setText("795");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews(){
        materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.spinner_tower);
        materialDesignSpinner1 = (MaterialBetterSpinner)
                findViewById(R.id.spinner_flat);
        materialDesignSpinnerPurpose = (MaterialBetterSpinner)
                findViewById(R.id.spinner_purpose);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.create_visitor:
                if(isVisitorEntryValid()){
                    makeResidentEntry();
                }
                break;
        }

    }


    private void makeResidentEntry(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        ResidentIn residentIn = realm.createObject(ResidentIn.class);
        residentIn.setResidentName(mTextVistorName.getText().toString());
        residentIn.setResidentNumber(resident.getPhone1());
        residentIn.setVehicleName("HR 26 CZ 6479");
        residentIn.setSlotName("795");
        residentIn.setFlatVisited(towerName + " - " + flatName);
        residentIn.setDate(Utils.getCurrentDate());
        residentIn.setTime(Utils.getCurrentTime());
        realm.commitTransaction();
        Utils.displayToast(getApplicationContext(),"Resident Entry made");
        finish();
    }

    @Override
    protected void onDestroy() {
        Realm.getDefaultInstance().close();
        super.onDestroy();
    }

    private boolean isVisitorEntryValid(){
        if (TextUtils.isEmpty(towerName)) {
            Toast.makeText(getApplicationContext(), "Please select Tower", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(flatName)) {
            Toast.makeText(getApplicationContext(), "Please select Flat", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}

