package com.android.society.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.society.Constants.Constants;
import com.android.society.Model.Alert;
import com.android.society.R;
import com.android.society.parser.AlertParser;

public class PanicAlertActivity extends AppCompatActivity {

    private String data;
    private TextView textFlat;
    private TextView textPhone;
    private TextView textIntercom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panic_alert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Panic Alert");
        toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_red));
        initViews();
        setStatusBarGradiant(this);

        Log.v("anshul","call panic oncreate");



        if(getIntent() != null && getIntent().getExtras() != null){
            data = getIntent().getStringExtra(Constants.NOTIFICATION_DATA);
            if(!TextUtils.isEmpty(data)){
                Alert alert = AlertParser.parseAlertData(data);
                if(alert != null){
                    setData(alert);
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.v("anshul","call panic onNewIntent");
        if(getIntent() != null && getIntent().getExtras() != null){
            data = getIntent().getStringExtra(Constants.NOTIFICATION_DATA);
            if(!TextUtils.isEmpty(data)){
                Alert alert = AlertParser.parseAlertData(data);
                if(alert != null){
                    setData(alert);
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_red);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews(){
        textFlat = (TextView)findViewById(R.id.text_flat);
        textPhone= (TextView)findViewById(R.id.text_number);
        textIntercom = (TextView)findViewById(R.id.text_intercom);
    }

    private void setData(Alert alert){
        textFlat.setText(alert.getFlatName());
        textPhone.setText(alert.getNumber());
        textIntercom.setText(alert.getIntercomNumber());
    }

}
