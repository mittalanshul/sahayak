package com.android.society.manager;

/**
 * Created by 201101101 on 10/22/2017.
 */

public class StaticDataManager {

    public static  StaticDataManager mStaticDataManager;
    private static final Object lock = new Object();

    public static StaticDataManager getInstance(){
        synchronized (lock) {
            if(mStaticDataManager == null){
                mStaticDataManager = getInstance();
            }
        }
        return mStaticDataManager;
    }
}
