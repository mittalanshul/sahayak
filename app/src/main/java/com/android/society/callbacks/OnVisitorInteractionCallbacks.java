/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.callbacks;

import com.android.society.Model.ResidentIn;
import com.android.society.Model.Visitor;

/**
 * Created by 201101101 on 10/30/2017.
 */

public interface OnVisitorInteractionCallbacks {

    public void onVisitorExited(Visitor visitor);
    public void onVisitorClicked(Visitor visitor);
    public void onResidentClicked(ResidentIn residentIn);
    public void onResidentCallMake(ResidentIn residentIn);
}
