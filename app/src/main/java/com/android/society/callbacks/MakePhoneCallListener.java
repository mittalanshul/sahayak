package com.android.society.callbacks;

/**
 * Created by anshul on 10/03/18.
 */

public interface MakePhoneCallListener {

    public void onPhoneCallMake(String phoneNumber);
}
