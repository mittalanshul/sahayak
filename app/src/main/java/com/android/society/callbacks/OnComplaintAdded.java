/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.callbacks;

import com.android.society.Model.Complaint;

/**
 * Created by 201101101 on 10/18/2017.
 */

public interface OnComplaintAdded {

    public void omComplaintAdded(Complaint complaint);
    public void omComplaintDelete(Complaint complaint);
    public void omComplaintClicked(Complaint complaint);
}
