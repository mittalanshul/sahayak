package com.android.society.callbacks;

/**
 * Created by anshul on 12/01/18.
 */

public interface CommentAddedCallback {

    public void onCommentAdded(String comment);
}
