package com.android.society.callbacks;

import com.android.society.Model.Child;

/**
 * Created by anshul on 28/12/17.
 */

public interface ChildSelectedCallback {

    public void onChildSelected(Child child);
    public void onChildDeselected();
}
