package com.android.society.callbacks;

/**
 * Created by anshul on 08/01/18.
 */

public interface OnNeighbourAddedCallback {

    public void onNeighbourAdded(String flatName);
}
