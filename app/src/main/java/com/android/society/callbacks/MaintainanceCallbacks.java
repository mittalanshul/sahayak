package com.android.society.callbacks;

import com.android.society.Model.MaintainenceDues;

/**
 * Created by anshul on 08/03/18.
 */

public interface MaintainanceCallbacks {

    public void onMaintainanceClicked(String id);
    public void onMaintainanceClicked(MaintainenceDues maintainenceDues);

}
