package com.android.society.callbacks;

/**
 * Created by anshul on 04/02/18.
 */

public interface ResidentProfileCallbacks {

    public void uploadResidentProfile(String requestbody);
}
