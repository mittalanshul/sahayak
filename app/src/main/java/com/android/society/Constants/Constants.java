/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package com.android.society.Constants;


/**
 * Created by 201101101 on 10/16/2017.
 */

public class Constants {

    public static final String EVENTS = "Events";
    public static final String NOTICES = "Notices";
    public static final String FACILITIES = "Facilities";
    public static final String COMPLAINTS = "Complaints";
    public static final String RESIDENTS = "Residents";
    public static final String RWA_MEMBERS = "RWA Members";
    public static final String MAID = "Maid";
    public static final String OFFICE = "Office";
    public static final String OTHER = "Service";
    public static final String CAR_CLEANER = "Car Cleaner";
    public static final String GUARDS = "Guard";
    public static final String ELECTRICIAN = "Electrician";
    public static final String PLUMBER = "Plumber";
    public static final String PHYSICIAN = "Physician";
    public static final String ATTENDENCE = "Attendance";
    public static final String STAFF_ATTENDENCE = "Staff Attendance";
    public static final String RESIDENT_PROFILE = "Profile";
    public static final String ADD_SERVICES = "Add Services";
    public static final String ADD_EMERGENCY_CONTACTS = "Emergency Contacts";
    public static final String MAINTAINENCE_DUES = "Maintainence Dues";

    public static final String VISITOR_ENTRY = "Create Visitor";
    public static final String RESIDENT_ENTRY = "Resident Entry";
    public static final String VISITOR_DETAIL = "Visitor Detail";
    public static final String DIALY_SERVICES = "Daily Services";
    public static final String VISITOR_REGISTER = "Visitor Register";
    public static final String STAFF_REGISTER = "Staff Register";
    public static final String VISITOR_HISTORY = "Visitor History";
    public static final String EXPECTED_VISITOR = "Expected Visitor";
    public static final String CHILD_EXIT_SAFETY = "Child Safety";

    public static final String TYPE = "type";

    public static final String MODE_RWA = "mode_rwa";
    public static final String MODE_RESIDENT = "mode_resident";
    public static final String MODE_GUARD = "mode_guard";
    public static final int REQUEST_PERMISSIONS = 1045;

    public static String BASE_URL ="http://www.edgeover.in";
    public static String AUTHORITY_CREATE ="/sys/create";
    public static String AUTHORITY_lOGIN ="/authority/user";
    public static String GUARD_lOGIN = "/entry/login";
    public static String GET_ALL_RESIDENT_DATA = "/authority?resident=true&staff=true&service=true";
    public static String GET_ALL_GUARD_DATA ="/entry/data?resident=true&staff=true&service=true";
    public static String LOGIN_RESIDENT = "/resident/user";
    public static String VERIFY_RESIDENT = "/resident/user/verify";
    public static String RESIDENT_PROFILE_URL = "/authority/resident?type=single&id=";
    public static String ONLY_RESIDENT_URL ="/resident/";
    public static String CREATE_VISITOR_ENTRY_URL = "/entry/visitor?";
    public static String CREATE_RESIDENT_EXPECTED_VISITOR_ENTRY_URL = "/resident/visitor";
    public static String GUARD_EXPECTED_VISITOR_ENTRY_URL = "/entry/expectedvisitor";
    public static String CREATE_STAFF_ENTRY_URL ="/entry/service";
    public static String GET_ALL_RESIDENT_SERVICE ="/resident/service";
    public static String EXIT_VISITOR ="/exit/visitor?";
    public static String CREATE_EVENT ="/authority/event";
    public static String CREATE_NOTICE = "/authority/notice";
    public static String GET_ALL_COMPLAINTS ="/authority/complaint";
    public static String GET_ALL_RESIDENT_COMPLAINTS ="/resident/complaint";
    public static String UPDATE_ALL_RESIDENT_COMPLAINTS ="/resident/complaint/set";
    public static String CREATE_COMPLAINT ="/resident/complaint";
    public static String CREATE_EVENT_RESIDENT ="/resident/event";
    public static String CREATE_NOTICE_RESIDENT = "/resident/notice";
    public static String GET_RESIDENT_DIRECTORY ="/resident/directory";
    public static String RESIDENT_VISITOR_HISTORY ="/resident/entry?type=visitor";
    public static String RESIDENT_ALERT ="/resident/alert?type=fire";
    public static String RESIDENT_ALL_MAINTAINENCE = "/resident/maintenance";
    public static String RESIDENT_ALL_MAINTAINENCE_BY_ID = "/resident/maintenance?maintenanceId=";
    public static final String ENTRY_ALERT="/entry/alert";
    public static final String RESIDENT_VISITOR_ALLOW = "/resident/visitor/allow";

    public static String CHILD_ENTRY_REQUEST = "/entry/child/request";
    public static String GET_ALL_CHILD = "/entry/child/data";
    public static final String CHILD_EXIT_ACTION = "/resident/child/exit";
    public static final String VISITOR_ACTION = "/resident/visitor/exit";
    public static String RESIDENT_UPDATE_PROFILE =  "/resident/update";

    public static String AUTHORIZATION_HEADER ="Authorization";
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    public static int LOGIN_REQUEST_CODE = 77;

    public static String AD_COMPLAINT = "r-complaint";
    public static String AD_ALERT = "g-alert";
    public static String AD_VISITOR = "r-visitor-action";
    public static String AD_CHILD_SAFETY = "r-child-safety";
    public static String AD_CHILD_ACTION = "g-child-action";
    public static String AD_VISITOR_ACTION = "r-visitor";
    public static String AD_GUARD_VISITOR_ACTION = "g-visitor-action";
    public static String AD_MAINTAINENCE= "r-maintainance";
    public static String AD_EVENT= "r-event";
    public static String AD_NOTICE= "r-notice";
    public static String AD_SERVICE= "r-service";
    public static String A_COMPLAINT= "a-complaint";
    public static String R_ALERT = "r-alert";

    public static final int MAX_EMERGENCY_CONACTS = 2;
    public static final String INTENT_ACTION_BROADCAST = "intent_action_broadcast";
    public static final String NOTIFICATION_TYPE = "notification_type";
    public static final String NOTIFICATION_MESSAGE = "notification_message";
    public static final String NOTIFICATION_DATA = "notification_data";
    public static final String SOLVED = "solved";
    public static final String REOPEN = "reopen";
    public static final String OPEN = "Pending";
    public static final String MEDIA_BASE_URL = "https://s3.ap-south-1.amazonaws.com/www.edgeover.in/";

}
